<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['domain' => 'admin.dentinefkgunair.com'], function () {
    Route::get('/', function () {
        return redirect('organizer');
    });
});

Route::get('error-page', function () {
    return view('error-page');
});

Route::get('/', 'Participant\AccountController@indexSignIn');
Route::get('register', 'Participant\AccountController@indexRegister');
Route::get('change-province', 'Participant\AccountController@actionChangeProvince');
Route::get('register/{event}', 'Participant\AccountController@indexRegister');
Route::post('register', 'Participant\AccountController@actionCheckCode');
Route::post('signin', 'Participant\AccountController@actionSignIn');
Route::post('signup', 'Participant\AccountController@actionSignUp');
Route::get('logout', 'AuthGlobalController@actionSignOut');
Route::get('invoice/get/{id}', 'Organizer\VoucherController@indexInvoice');
Route::get('admin/download-essay/file-zip', 'Organizer\ParticipantController@actionDownloadFileZip');
Route::get('admin/download-poster/file-zip', 'Organizer\ParticipantController@actionDownloadFileZip2');

Route::group(['middleware' => ['auth_participant']], function () {
    Route::get('welcome', function () {
        return view('participant/welcome');
    });

    Route::get('payment-test', 'Participant\AccountController@indexPaymentTest');

    Route::get('payment-confirmation', 'Participant\AccountController@indexPaymentConfirmation');
    Route::post('payment-confirmation', 'Participant\AccountController@actionPaymentConfirmation');
    Route::get('profile', 'Participant\AccountController@indexProfile');
    Route::post('profile', 'Participant\AccountController@actionSaveProfile');
    Route::get('change-password', 'Participant\AccountController@indexPassword');
    Route::post('change-password', 'Participant\AccountController@actionChangePassword');
    Route::get('certificate/{id}', 'Participant\EventController@indexCertificate');

    Route::get('certificate/next/{id}', 'Participant\EventController@indexCertificateNext');

    Route::post('event/essay/upload', 'Participant\EventController@actionEssayUpload');
    Route::get('event/{event}', 'Participant\EventController@indexEventRegist');
    Route::get('event/{event}/{participant_number}', 'Participant\EventController@indexEventRegist');
    Route::post('event/{event}/participate', 'Participant\EventController@actionParticipate');
    Route::post('event/{event}/regist', 'Participant\EventController@actionRegist');
    Route::get('event/{event}/card', 'Participant\EventController@indexMemberCard');

    Route::get('member/edit/{id}', 'Participant\EventController@indexManage');
    Route::post('member/save', 'Participant\EventController@actionSaveMember');

    Route::get('event/file-upload/delete/{id}', 'Participant\EventController@actionDeleteFileUpload');

    Route::get('test/rule', function () {
        return view('participant/test-rule');
    });
    Route::get('test', 'Participant\TestController@index');
    Route::get('test/question/{id}', 'Participant\TestController@indexDetail');
    Route::post('test/answer', 'Participant\TestController@actionSaveAnswer');
    Route::post('test/flags', 'Participant\TestController@actionFlags');
    Route::get('test/result', 'Participant\TestController@indexReview');
    Route::get('test/end', 'Participant\TestController@actionEndTest');
});

Route::group(array('prefix' => 'organizer'), function () {
    Route::get('/', 'Organizer\AccountController@indexSignIn');
    Route::post('signin', 'Organizer\AccountController@actionSignIn');
    Route::group(array('middleware' => ['auth_organizer']), function () {
        Route::get('welcome', function () {
            return view('organizer/welcome');
        });

        Route::get('profile', 'Organizer\AccountController@indexProfile');
        Route::post('profile', 'Organizer\AccountController@actionSaveProfile');
        Route::get('change-password', 'Organizer\AccountController@indexPassword');
        Route::post('change-password', 'Organizer\AccountController@actionChangePassword');

        Route::group(array('prefix' => 'voucher'), function () {
            Route::get('/', 'Organizer\VoucherController@indexList');
            Route::get('new', 'Organizer\VoucherController@indexNew');

            Route::post('table', 'Organizer\VoucherController@commonList');
            Route::post('new', 'Organizer\VoucherController@actionGenerate');
            Route::post('pay', 'Organizer\VoucherController@actionPay');
            Route::post('delete', 'Organizer\VoucherController@actionDelete');
        });

        Route::group(array('prefix' => 'question'), function () {
            Route::get('/', 'Organizer\QuestionController@indexList');
            Route::get('new', 'Organizer\QuestionController@indexNew');
            Route::get('manage/{id}', 'Organizer\QuestionController@indexManage');
            Route::get('test/{id}', 'Organizer\QuestionController@indexTest');

            Route::post('table', 'Organizer\QuestionController@commonList');
            Route::post('/', 'Organizer\QuestionController@actionSave');
            Route::post('delete', 'Organizer\QuestionController@actionDelete');

            Route::group(array('prefix' => 'category'), function () {
                Route::get('/', 'Organizer\QuestionCategoryController@indexList');
                Route::get('manage', 'Organizer\QuestionCategoryController@indexManage');
                Route::get('manage/{id}', 'Organizer\QuestionCategoryController@indexManage');

                Route::post('table', 'Organizer\QuestionCategoryController@commonList');
                Route::post('/', 'Organizer\QuestionCategoryController@actionSave');
                Route::post('delete', 'Organizer\QuestionCategoryController@actionDelete');
            });

            Route::group(array('prefix' => 'package'), function () {
                Route::get('/', 'Organizer\QuestionPackageController@indexList');
                Route::get('manage', 'Organizer\QuestionPackageController@indexManage');
                Route::get('manage/{id}', 'Organizer\QuestionPackageController@indexManage');

                Route::post('table', 'Organizer\QuestionPackageController@commonList');
                Route::post('/', 'Organizer\QuestionPackageController@actionSave');
                Route::post('delete', 'Organizer\QuestionPackageController@actionDelete');

                Route::get('detail/{id}', 'Organizer\QuestionPackageController@indexDetail');

                Route::post('detail/table/{id}/{tipe}', 'Organizer\QuestionPackageController@detailList');
                Route::post('detail/add', 'Organizer\QuestionPackageController@actionDetailAdd');
                Route::post('detail/delete', 'Organizer\QuestionPackageController@actionDetailDelete');
            });
        });

        Route::group(array('prefix' => 'test'), function () {
            Route::get('/', 'Organizer\TestController@indexList');
            Route::get('excel', 'Organizer\TestController@indexDownloadExcel');
            Route::get('detail/{id}', 'Organizer\TestController@indexDetail');
            Route::get('refresh', 'Organizer\TestController@actionRefresh');

            Route::post('table', 'Organizer\TestController@commonList');
        });

        Route::group(array('prefix' => 'fileupload'), function () {
            Route::get('/', 'Organizer\FileUploadController@indexList');

            Route::post('table', 'Organizer\FileUploadController@commonList');
            Route::post('delete', 'Organizer\FileUploadController@actionDelete');
        });

        Route::group(array('prefix' => 'participant'), function () {
            Route::get('/', 'Organizer\ParticipantController@indexList');
            Route::get('detail/{account}/{event}', 'Organizer\ParticipantController@indexDetail');
            Route::get('member/manage/{id}', 'Organizer\ParticipantController@indexManage');
            Route::get('reset-password/{account}', 'Organizer\ParticipantController@indexPassword');
            Route::get('account/excel', 'Organizer\ParticipantController@indexAccountDownloadExcel');
            Route::get('member/excel', 'Organizer\ParticipantController@indexMemberDownloadExcel');

            Route::post('table', 'Organizer\ParticipantController@commonList');
            Route::post('participate/list', 'Organizer\ParticipantController@participateList');
            Route::post('reset-password', 'Organizer\ParticipantController@actionResetPassword');
            Route::post('member/save', 'Organizer\ParticipantController@actionSaveMember');
            Route::post('delete', 'Organizer\ParticipantController@actionDelete');
        });

        Route::post('img/upload', 'Organizer\QuestionController@actionUploadImage');
    });

});
