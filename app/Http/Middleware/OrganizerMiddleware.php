<?php

namespace App\Http\Middleware;

use Closure;
// use Illuminate\Support\Facades\Auth;
use Auth;

class OrganizerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            if(empty(session('user'))){
                $account = Auth::user();
                session(['user' => json_encode($account)]);
            }
            
            $user = (object) json_decode(session('user'));
            if($user->type == 10){
                return $next($request);
            }else{
                Auth::logout();
                return redirect('error-page')->with('err', 'You do not have permission to view this page');
            }
        }else{
            Auth::logout();
            return redirect('error-page')->with('err', 'You are not logged in');
        }

    }
}
