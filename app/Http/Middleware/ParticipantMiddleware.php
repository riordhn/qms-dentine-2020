<?php

namespace App\Http\Middleware;

// use Illuminate\Support\Facades\Auth;

use Auth;
use Closure;
use Session;

class ParticipantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            $account = Auth::user();

            if ($account) {
                if ($account->type == 1) {
                    $token = Session::get('auth_token');
                    if (!empty($token)) {
                        if ($token == $account->auth_token) {
                            return $next($request);
                        } else {
                            Session::flush();
                            Auth::logout();
                            return redirect('error-page')->with('err', 'Your account is open in other device');
                        }
                    } else {
                        return $next($request);
                    }
                } else {
                    Session::flush();
                    Auth::logout();
                    return redirect('error-page')->with('err', 'You do not have permission to view this page');
                }
            } else {
                Session::flush();
                Auth::logout();
                return redirect('error-page')->with('err', 'You do not have permission to view this page');
            }
        } else {
            Session::flush();
            Auth::logout();
            return redirect('error-page')->with('err', 'You are not logged in');
        }

    }
}
