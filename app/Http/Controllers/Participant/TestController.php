<?php

namespace App\Http\Controllers\Participant;

use App\Libraries\TestTime;
use App\Models\EventTime;
use App\Models\Participant;
use App\Models\Question;
use App\Models\QuestionCategory;
use App\Models\QuestionOption;
use App\Models\QuestionPackage;
use App\Models\QuestionPackageDetail;
use App\Models\Test;
use App\Models\TestAnswer;
use App\Models\TestSession;
use App\Models\VoucherEvent;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Validator;

class TestController extends BaseController
{
    public function index(Request $request)
    {
        $account = Auth::user();
        if ($test = Test::where('account_id', $account->account_id)->first()) {
            $check_running = TestTime::checkRunning($test);
            if ($check_running->code == 500) {
                return view('participant/test-rule');
                // $message = $check_running->message;
                // return view('participant/blank-page', compact('message'));
            } else {
                if ($check_running->code == 200) {
                    $test_answer = TestAnswer::where(['account_id' => $account->account_id, 'test_id' => $test->test_id])->first();
                    return redirect('test/question/' . $test_answer->question_id);
                } else {
                    $message = $check_running->message;
                    return view('participant/blank-page', compact('message'));
                }
            }
        } else {
            if ($voucher_event = VoucherEvent::where(['voucher_id' => $account->voucher_id, 'event_id' => 1])->first()) {
                // if($account->voucher->regional_id == 20){

                // Not using test duration, but end time
                // $test_duration = 70; // Minutes
                // $end_time = Carbon::now('Asia/Jakarta')->addMinutes($test_duration);

                $start_time = Carbon::now('Asia/Jakarta');
                $test_session = TestSession::with('event_time')->where('account_id', $account->account_id)->first();

                $end_time = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->end_date);
                $real_end_time = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->end_date);

                if (strtotime($real_end_time) < strtotime($end_time)) {
                    $end_time = $real_end_time;
                }
                DB::transaction(function () use ($start_time, $end_time, $account, $test_session) {
                    $test = new Test;
                    $test->account_id = $account->account_id;
                    // $test->question_package_id = QuestionPackage::inRandomOrder()->first()->question_package_id;
                    $test->question_package_id = $test_session->question_package_id;
                    $test->start_at = $start_time;
                    $test->end_at = $end_time;
                    $test->save();

                    $question_package_details = QuestionPackageDetail::where('question_package_id', $test->question_package_id)->with('question')->get();
                    $question_package_details = $question_package_details->sortBy('question.question_category_id')->values();
                    $question_number = 1;
                    foreach ($question_package_details->chunk(40) as $chunk) {
                        $data = array();
                        foreach ($chunk as $question_package_detail) {
                            $question_package_detail = (object) $question_package_detail;
                            $test_answer = array(
                                'account_id' => $account->account_id,
                                'test_id' => $test->test_id,
                                'number' => $question_number,
                                'question_id' => $question_package_detail->question_id,
                                'created_at' => Carbon::now('Asia/Jakarta'),
                                'updated_at' => Carbon::now('Asia/Jakarta'),
                            );

                            $data[] = $test_answer;
                            $question_number++;
                        }

                        TestAnswer::insert($data);
                    }
                }, 3);

                $test_answer = TestAnswer::where(['account_id' => $account->account_id])->first();
                return redirect('test/question/' . $test_answer->question_id);
                // }
            }
        }

        $message = 'Tidak bisa mengerjakan soal';
        return view('participant/blank-page', compact('message'));
    }

    public function indexDetail(Request $request, $question_id = 0)
    {
        $account = Auth::user();
        $test_start = EventTime::find(8)->start_date;
        if ($test = Test::where('account_id', $account->account_id)->first()) {
            $check_running = TestTime::checkRunning($test);
            if ($check_running->code == 500) {
                $message = $check_running->message;
                return view('participant/blank-page', compact('message'));
            } else {
                if ($check_running->code == 200) {
                    if ($test_answer = TestAnswer::with('question')->where(['account_id' => $account->account_id, 'test_id' => $test->test_id, 'question_id' => $question_id])->first()) {
                        $now = Carbon::now('Asia/Jakarta');
                        $seconds_running = $now->diffInSeconds(Carbon::parse($test_start));

                        $question_categories = QuestionCategory::orderBy('question_category_id', 'asc')->get();
                        $active_category = null;
                        $seconds_start = 0;
                        foreach($question_categories as $question_category){
                            if(($seconds_start + 1) <= $seconds_running && $seconds_running <= ($seconds_start + $question_category->seconds_total)){
                                $active_category = $question_category;
                            }

                            $seconds_start += $question_category->seconds_total;

                            $question_category->minutes_total = $question_category->seconds_total / 60;
                        }

                        if($test_answer->question->question_category_id != $active_category->question_category_id){
                            return redirect('test/question/'. TestAnswer::whereHas('question', function($q) use ($active_category){
                                $q->where('question_category_id', $active_category->question_category_id);
                            })->orderBy('number', 'asc')->first()->question_id);
                        }

                        $test_answers = TestAnswer::with('question')->whereHas('question', function($q) use ($active_category){
                            $q->where('question_category_id', $active_category->question_category_id);
                        })->where(['account_id' => $account->account_id, 'test_id' => $test->test_id])->get();
                        $question = Question::find($test_answer->question_id);
                        $question_options = QuestionOption::where('question_id', $test_answer->question_id)->get();

                        $participant = Participant::where('account_id', $account->account_id)->first();

                        $total_seconds_end = 0;
                        foreach($question_categories as $question_category){
                            if(!empty($active_category) && $question_category->question_category_id <= $active_category->question_category_id){
                                $total_seconds_end += $question_category->seconds_total;
                            }
                        }
                        // $timeup_left = strtotime($test->end_at) - strtotime($now);
                        $timeup_left = strtotime(Carbon::parse($test_start)->addSeconds($total_seconds_end)) - strtotime($now);

                        // dd(Carbon::parse($test_start)->addSeconds($total_seconds_end));

                        return view('participant/question', compact('question', 'question_options', 'test_answer', 'test_answers', 'timeup_left', 'participant', 'question_categories', 'active_category'));
                    } else {
                        $message = 'Tidak bisa mengerjakan soal';
                        return view('participant/blank-page', compact('message'));
                    }
                } else {
                    $message = $check_running->message;
                    return view('participant/blank-page', compact('message'));
                }
            }
        } else {
            $message = 'Tidak bisa mengerjakan soal';
            return view('participant/blank-page', compact('message'));
        }
    }

    public function indexReview(Request $request, $question_id = 0)
    {
        $account = Auth::user();
        if ($test = Test::where('account_id', $account->account_id)->first()) {
            $check_running = TestTime::checkRunning($test);
            if ($check_running->code == 500) {
                $message = $check_running->message;
                return view('participant/blank-page', compact('message'));
            } else {
                if ($check_running->code == 200) {
                    $message = $check_running->message;
                    return view('participant/blank-page', compact('message'));
                } else {
                    $question_package = QuestionPackage::find($test->question_package_id);
                    $test_answers = TestAnswer::where(['account_id' => $account->account_id, 'test_id' => $test->test_id])->get();
                    return view('participant/test-result', compact('question_package', 'test_answers'));
                }
            }
        } else {
            $message = 'Belum mengerjakan test';
            return view('participant/blank-page', compact('message'));
        }
    }

    public function actionSaveAnswer(Request $request)
    {
        $input = (object) $request->input();

        $validator = Validator::make($request->all(), [
            'question' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $account = Auth::user();
        if ($test = Test::where('account_id', $account->account_id)->first()) {
            $check_running = TestTime::checkRunning($test);
            if ($check_running->code == 500) {
                $message = $check_running->message;
                return view('participant/blank-page', compact('message'));
            } else {
                if ($check_running->code == 200) {
                    if ($test_answer = TestAnswer::where(['account_id' => $account->account_id, 'test_id' => $test->test_id, 'question_id' => $input->question])->first()) {
                        if ($test_answer->question->question_type == 1) {
                            if (!empty($input->question_option)) {
                                if ($question_option = QuestionOption::where(['question_id' => $test_answer->question_id, 'question_option_id' => $input->question_option])->first()) {
                                    $question_category = QuestionCategory::join('questions', 'questions.question_category_id', '=', 'question_categories.question_category_id')->where('question_id', $test_answer->question_id)->first();
                                    $test_answer->question_option_id = $question_option->question_option_id;
                                    if ($question_option->correct == 1) {
                                        $test_answer->correct = 1;
                                        $test_answer->value = $question_category->true_value;
                                    } else {
                                        $test_answer->correct = 2;
                                        $test_answer->value = $question_category->false_value;
                                    }
                                    $test_answer->save();

                                    if ($next_test_answer = TestAnswer::where(['account_id' => $account->account_id, 'test_id' => $test->test_id, 'number' => ($test_answer->number + 1)])->first()) {
                                        return redirect('test/question/' . $next_test_answer->question_id);
                                    } else {
                                        return back();

                                    }
                                } else {
                                    $message = 'Tidak bisa memjawab soal';
                                    return view('participant/blank-page', compact('message'));
                                }
                            } else {
                                $question_category = QuestionCategory::join('questions', 'questions.question_category_id', '=', 'question_categories.question_category_id')->where('question_id', $test_answer->question_id)->first();
                                $test_answer->question_option_id = null;
                                $test_answer->correct = 0;
                                $test_answer->value = $question_category->null_value;
                                $test_answer->save();

                                return back();
                            }
                        } else {
                            $test_answer->content_text = $input->answer;
                            $test_answer->save();

                            if ($next_test_answer = TestAnswer::where(['account_id' => $account->account_id, 'test_id' => $test->test_id, 'number' => ($test_answer->number + 1)])->first()) {
                                return redirect('test/question/' . $next_test_answer->question_id);
                            } else {
                                return back();

                            }
                        }
                    } else {
                        $message = 'Tidak bisa memjawab soal';
                        return view('participant/blank-page', compact('message'));
                    }
                } else {
                    $message = $check_running->message;
                    return view('participant/blank-page', compact('message'));
                }
            }
        } else {
            $message = 'Tidak bisa menjawab soal';
            return view('participant/blank-page', compact('message'));
        }
    }

    public function actionEndTest(Request $request)
    {
        $input = (object) $request->input();

        $account = Auth::user();
        if ($test = Test::where('account_id', $account->account_id)->first()) {
            $check_running = TestTime::checkRunning($test);
            if ($check_running->code == 500) {
                $message = $check_running->message;
                return view('participant/blank-page', compact('message'));
            } else {
                if ($check_running->code == 200) {
                    $test->end_at = Carbon::now('Asia/Jakarta');
                    $test->save();
                    return back()->with('toast', 'Test Anda sudah selesai dikerjakan');
                } else {
                    $message = $check_running->message;
                    return view('participant/blank-page', compact('message'));
                }
            }
        } else {
            $message = 'Tidak bisa mengerjakan soal';
            return view('participant/blank-page', compact('message'));
        }
    }

    public function actionFlags(Request $request)
    {
        $account = Auth::user();
        if ($test = Test::where('account_id', $account->account_id)->first()) {
            $test->flags = $test->flags + 1;
            $test->save();
        } 

        return '...';
    }
}
