<?php

namespace App\Http\Controllers\Participant;

use App\Models\Account;
use App\Models\City;
use App\Models\Country;
use App\Models\Event;
use App\Models\Image;
use App\Models\Participant;
use App\Models\Province;
use App\Models\Rayon;
use App\Models\Voucher;
use App\Models\VoucherEvent;
use App\Services\Midtrans\CreateTransactionService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Session;
use Validator;

class AccountController extends BaseController
{
    public function indexSignIn(Request $request)
    {
        if (Auth::check()) {
            $account = Auth::user();

            if ($account->type == 1) {
                return redirect('welcome');
            } else if ($account->type == 10) {
                return redirect('error-page')->with('err', 'You do not have permission to view this page');
            } else {
                return redirect('error-page')->with('err', 'You do not have permission to view this page');
            }
        } else {
            return view('participant/signin');
        }
    }

    public function indexRegister(Request $request, $event_code = 0)
    {
        $input = (object) $request->input();
        // if(empty($input->code)){
        // return view('participant/check-code');
        // }else{
        // if($voucher = Voucher::where(['code' => $input->code, 'is_paid' => 10])->whereNull('account_id')->first()){
        if (!empty($event_code)) {
            $events = Event::where('code', $event_code)->get();
        } else {
            $events = Event::whereIn('event_id', [100])->get();
            // $events = Event::get();
        }
        $provinces = Province::orderBy('name')->get();
        $countries = Country::orderBy('name')->get();
        $rayons = Rayon::orderBy('name')->get();

        return view('participant/signup', compact('events', 'provinces', 'countries', 'rayons'));
        // }else{
        //     return view('participant/check-code')->with('toast', 'Kode voucher tidak valid, kamu bisa coba lagi.');
        // return view('participant/check-code')->with('toast', 'Your code is invalid, try again.');
        // }
        // }
    }

    public function indexProfile(Request $request)
    {
        $account = Auth::user();
        return view('participant/profile', compact('account'));
    }

    public function indexPaymentTest(Request $request)
    {
        $account = Auth::user();
        $voucher = Voucher::with('voucher_events')->where('voucher_id', $account->voucher_id)->first();
        $event = $voucher->voucher_events->first()->event;

        if($account->is_paid == 10){
            return abort(404);
        }else{
            if(!empty($voucher->payment_url)){
                return redirect($voucher->payment_url);
            }else{
                $order_id = rand();
                $order = (object) [
                    'order_id' => $order_id,
                    'gross_amount' => $event->registration_fee,
                ];

                $items = [
                    (object) [
                        'id' => 1,
                        'price' => $event->registration_fee,
                        'quantity' => 1,
                        'name' => $event->name,
                    ]
                ];

                $customer = (object) [
                    'first_name' => $account->name,
                    'email' => $account->email,
                    'phone' => $account->phone,
                ];
                
                try {
                    $midtrans = new CreateTransactionService($order, $items, $customer);
                    $midtrans_create_transaction = $midtrans->create();
                    $payment_url = $midtrans_create_transaction->redirect_url;

                    $voucher->payment_order_id = $order_id;
                    $voucher->payment_url = $payment_url;
                    $voucher->save();
                    
                    return redirect($voucher->payment_url);
                }
                catch (Exception $e) {
                  return $e->getMessage();
                }
            }
        }
    }

    public function indexPaymentConfirmation(Request $request)
    {
        $account = Auth::user();
        $voucher = Voucher::with('image', 'image_addon')->where('voucher_id', $account->voucher_id)->first();
        $voucher_event = VoucherEvent::where('voucher_id', $account->voucher_id)->orderBy('event_id', 'desc')->first();

        if ($voucher->is_paid == 10) {
            return redirect('welcome');
        }
        return view('participant/payment-confirmation', compact('account', 'voucher', 'voucher_event'));
    }

    public function indexPassword(Request $request)
    {
        return view('participant/password');
    }

    public function actionCheckCode(Request $request)
    {
        $input = (object) $request->input();
        if ($voucher = Voucher::where(['code' => $input->code, 'is_paid' => 10])->whereNull('account_id')->first()) {
            return redirect('register?code=' . $voucher->code);
        } else {
            return redirect('register')->with('toast', 'Your code is invalid, try again.');
        }
    }

    public function actionSignUp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'organization' => 'required',
            // 'province_id' => 'required',
            // 'city_id' => 'required',
            // 'address' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();

        if ($account = Account::where('email', $input->email)->first()) {
            return back()->with('toast', 'The email that you are using is already registered.');
        }

        DB::beginTransaction();

        try {
            $event = Event::find($input->event_id);
            $new_voucher = new Voucher;
            $new_voucher->save();

            $voucher_event = new VoucherEvent;
            $voucher_event->voucher_id = $new_voucher->voucher_id;
            $voucher_event->event_id = $event->event_id;
            $voucher_event->save();

            if(in_array($event->event_id, [6,7,8])){
                $voucher_event1 = new VoucherEvent;
                $voucher_event1->voucher_id = $new_voucher->voucher_id;
                $voucher_event1->event_id = 1;
                $voucher_event1->save();

                $voucher_event2 = new VoucherEvent;
                $voucher_event2->voucher_id = $new_voucher->voucher_id;
                $voucher_event2->event_id = 4;
                $voucher_event2->save();
            }

            $event->participate = $event->participate + 1;
            $event->save();

            $account = new Account;
            $account->voucher_id = $new_voucher->voucher_id;
            $account->name = strtoupper($input->name);
            $account->organization = strtoupper($input->organization);
            $account->email = strtolower($input->email);
            $account->phone = $input->phone;

            if($event->event_id == 2 || $event->event_id == 3){ // any event international
                if($input->is_other_country == 0){ // ID
                    $account->country_id = 77;
                    $account->province_id = $input->province_id;
                    $account->city_id = $input->city_id;
                }else{
                    $account->country_id = $input->country_id;
                }
            }else if($event->event_id == 1 || in_array($event->event_id, [6,7,8])){ // Dentine, any rayon, no international
                $account->country_id = 77;
                $account->province_id = $input->province_id;
                $account->city_id = $input->city_id;
                $account->rayon_id = $input->rayon_id;
            }else{ // 4 & 5, only domestic
                $account->country_id = 77;
                $account->province_id = $input->province_id;
                $account->city_id = $input->city_id;
            }

            $account->password = Hash::make($input->password);
            $account->save();

            $new_voucher->code = $this->generateCode($account, $event);
            $new_voucher->account_id = $account->account_id;
            $new_voucher->used_at = Carbon::now('Asia/Jakarta');
            $new_voucher->save();

            DB::commit();

            $to_name = $account->name;
            $to_email = $account->email;

            $data = array(
                'account' => $account,
                'event_name' => $event->name,
            );

            Mail::send('emails/regist-success', $data, function ($message) use ($to_name, $to_email, $event) {
                $message->to($to_email, $to_name)
                    ->subject($event->name.' - Registration Was Successful');
                $message->from(env('MAIL_FROM_ADDRESS', ''), env('MAIL_FROM_NAME', ''));
            });

            if (Auth::loginUsingId($account->account_id, true)) {
                $auth_token = uniqid() . '' . uniqid();
                $account->auth_token = $auth_token;
                $account->save();

                Session::put('auth_token', $auth_token);
                return redirect('welcome');
            } else {
                return back()->with('toast', 'Registration is successful, you can LOGIN with your email and password.');
            }
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('toast', (env('APP_DEBUG', 'true') == 'true') ? $e->getMessage() . ' (Line ' . $e->getLine() . ')' : 'Operation error');
        }
    }

    public function actionSignIn(Request $request)
    {
        $input = (object) $request->input();
        if ($account = Account::where(['email' => $input->email, 'type' => 1, 'status' => 1])->first()) {
            if (Hash::check($input->password, $account->password)) {
                $auth_token = uniqid() . '' . uniqid();
                $account->auth_token = $auth_token;
                $account->save();

                Session::put('auth_token', $auth_token);
                Auth::loginUsingId($account->account_id);
                return redirect('welcome');
            } else {
                return back()->with('toast', 'The password you entered is incorrect');
            }
        } else {
            return back()->with('toast', 'Your account was not found');
        }
    }

    public function actionPaymentConfirmation(Request $request)
    {
        $input = (object) $request->input();
        $account = Auth::user();
        $voucher = Voucher::find($account->voucher_id);

        if ($request->hasFile('file_tweet')) {
            $file = $request->file('file_tweet');
            $ext = $file->getClientOriginalExtension();
            $filename = $account->name . '-' . rand(100, 999) . "." . $ext;
            $file->move('uploads/tweet-bond', $filename);

            $image = new Image;
            $image->name = $filename;
            $image->type = $ext;
            $image->path = "tweet-bond/" . $filename;
            $image->save();

            $voucher->image_addon_id = $image->image_id;
            $voucher->save();

            return back()->with('toast', 'Please wait for max 1x24 hours for us to apply your discount');
        }

        if (!$request->hasFile('file')) {
            return back()->with('toast', 'Please upload your payment confirmation...');
        }

        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $filename = $account->name . '-' . rand(100, 999) . "." . $ext;
        $file->move('uploads/payment-confirmation', $filename);

        $image = new Image;
        $image->name = $filename;
        $image->type = $ext;
        $image->path = "payment-confirmation/" . $filename;
        $image->save();

        $voucher->image_id = $image->image_id;
        $voucher->referral = $input->referral;
        $voucher->save();

        return back()->with('toast', 'Please wait for max 2x24 hours for us to confirm your payment');
    }

    public function actionSignOut(Request $request)
    {
        Session::flush();
        Auth::logout();
        return redirect('/')->with('Sign out successfully');
    }

    public function actionChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'new_confirm_password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $account = Auth::user();
        $input = (object) $request->input();
        if ($input->new_password == $input->new_confirm_password) {
            if (Auth::once(['email' => $account->email, 'password' => $input->old_password])) {
                $account->password = Hash::make($input->new_password);
                $account->save();
                return back()->with('toast', 'Change password successfully');
            } else {
                return back()->with('toast', 'Your old password is incorrect');
            }
        } else {
            return back()->with('toast', 'Re-type your new password again');
        }
    }

    public function actionChangeProvince(Request $request)
    {
        $input = (object) $request->input();
        $validator = Validator::make($request->all(), [
            'province_id' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        return City::select('city_id', 'name')->where('province_id', $input->province_id)->orderBy('name')->get();
    }

    public function actionSaveProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();

        $account = Auth::user();
        $account->name = $input->name;
        $account->email = $input->email;
        $account->phone = $input->phone;
        $account->save();

        $account = $account->only('account_id', 'voucher_id', 'name', 'email', 'type', 'organization');
        return back()->with('toast', 'Your change saved successfully');
    }

    public function generateCode($account, $event)
    {
        if($event->event_id == 1){
            return $event->code . '/' . $account->rayon->code . '/' . str_pad($event->participate, 5, "0",STR_PAD_LEFT);
        }else{
            return $event->code . '/' . str_pad($event->participate, 5, "0",STR_PAD_LEFT);
        }
    }
}
