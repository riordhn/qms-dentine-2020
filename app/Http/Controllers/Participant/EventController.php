<?php

namespace App\Http\Controllers\Participant;

use App\Models\Event;
use App\Models\FileUpload;
use App\Models\Image;
use App\Models\Participant;
use App\Models\ParticipantMember;
use App\Models\Voucher;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use PDF;

class EventController extends BaseController
{
    public function indexEventRegist(Request $request, $event_id = 0, $participant_number = 0)
    {
        $account = Auth::user();
        $events = Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
        if ($event = $events->where('event_id', $event_id)->first()) {
            $participant = Participant::where(['event_id' => $event_id, 'account_id' => $account->account_id])->first();
            $participant_members = ['Leader', 'Member 1', 'Member 2', 'Member 3', 'Member 4', 'Member 5', 'Member 6', 'Member 7', 'Member 8', 'Member 9', 'Member 10', 'Member 11', 'Member 12', 'Member 13', 'Member 14', 'Member 15'];

            if($events->firstWhere('event_id', 6)){
                $event->person_in_event = 1;
            }else if($events->firstWhere('event_id', 7)){
                $event->person_in_event = 2;
            }else if($events->firstWhere('event_id', 8)){
                $event->person_in_event = 3;
            }

            if($participant){
                $participate_as = [$participant_members[ParticipantMember::where('participant_id', $participant->participant_id)->count()]];
            }else{
                $participate_as = $participant_members;
            }
            // switch ($event->event_id) {
            //     case 1:$participate_as = $participant_members;
            //         break;
            //     case 2:$participate_as = $participant_members;
            //         break;
            //     case 3:$participate_as = $participant_members;
            //         break;
            // }

            if ($participant_number > 0 && $participant_number <= $event->person_in_event) {
                // if ($participant_member = ParticipantMember::where('participant_id', $participant->participant_id)->first()) {
                //     return view('participant/event', compact('event', 'participant', 'account', 'participate_as'));
                // } else {
                    return view('participant/regist-member', compact('event', 'participant', 'account', 'participate_as', 'participant_number'));
                // }
            } else {
                return view('participant/event', compact('event', 'participant', 'account', 'participate_as'));
            }
        } else {
            return redirect('/');
        }
    }

    public function indexManage(Request $request, $id = 0)
    {
        $item = ParticipantMember::find($id);
        return view('participant/manage-participant-member', compact('item'));
    }

    public function indexMemberCard(Request $request, $event_id = 0)
    {
        $account = Auth::user();
        $events = Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
        if ($event = $events->where('event_id', $event_id)->first()) {
            if ($participant = Participant::where(['event_id' => $event_id, 'account_id' => $account->account_id])->first()) {
                $voucher = Voucher::find($account->voucher_id);
                $regional = $voucher->regional;
                $participant_members = ParticipantMember::where('participant_id', $participant->participant_id)->orderBy('participant_member_id', 'asc')->get();
                $html = view('member-card', compact('regional', 'voucher', 'participant_members'))->render();
                // return $html;
                $pdf = PDF::loadHTML($html)->setPaper('a4', 'portrait');
                return $pdf->stream('btp.pdf');
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function indexCertificate(Request $request, $participant_member_id = 0)
    {
        $account = Auth::user();
        if ($participant_member = ParticipantMember::with('participant')->where(['participant_member_id' => $participant_member_id])->first()) {
            $html = view('certificate', compact('participant_member'))->render();
            // return $html;
            $pdf = PDF::loadHTML($html)->setPaper('a4', 'landscape');
            return $pdf->stream('CERTIFICATE FOR ' . strtoupper($participant_member->name) . '.pdf');
        } else {
            return redirect('/');
        }
    }

    public function indexCertificateNext(Request $request, $participant_member_id = 0)
    {
        $account = Auth::user();
        if ($participant_member = ParticipantMember::with('participant', 'participant.account')->where(['participant_member_id' => $participant_member_id])->first()) {
            if($participant_member->participant->account->is_winner == 1){
                $html = view('certificate-winner', compact('participant_member'))->render();
                // return $html;
                $pdf = PDF::loadHTML($html)->setPaper('a4', 'landscape');
                return $pdf->stream('CERTIFICATE FOR ' . strtoupper($participant_member->name) . '.pdf');
            }
        } 
        
        return redirect('/');
    }

    public function actionParticipate(Request $request, $event_id = 0)
    {
        $account = Auth::user();
        $events = Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
        if ($event = $events->where('event_id', $event_id)->first()) {
            if ($participant = Participant::where(['event_id' => $event_id, 'account_id' => $account->account_id])->first()) {
                return redirect('/');
            } else {
                $participant = new Participant;
                $participant->event_id = $event->event_id;
                $participant->account_id = $account->account_id;
                $participant->name = 'TIM ' . strtoupper($event->name) . ' ' . strtoupper($account->name);
                $participant->save();

                return back()->with('toast', 'Please fill in your team complete data');
            }
        } else {
            return redirect('/');
        }
    }

    public function actionEssayUpload(Request $request)
    {
        $input = (object) $request->input();
        $account = Auth::user();
        $events = Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();

        if ($participant = Participant::where(['participant_id' => $input->participant, 'account_id' => $account->account_id])->first()) {
            if ($event = $events->where('event_id', $participant->event_id)->first()) {
                if ($participant->is_done == 1) {
                    if ($file_upload = FileUpload::where('participant_id', $participant->participant_id)->first()) {
                        $file_upload->delete();
                    }

                    $file = $request->file('file_1');
                    $ext = $file->getClientOriginalExtension();
                    $filename_1 = $event->code . '-' . $account->name . '1-' . rand(100, 999) . "." . $ext;
                    $file->move('uploads/' . $event->name, $filename_1);

                    if($request->hasFile('file_2')){
                        $file = $request->file('file_2');
                        $ext = $file->getClientOriginalExtension();
                        $filename_2 = $event->code . '-' . $account->name . '2-' . rand(100, 999) . "." . $ext;
                        $file->move('uploads/' . $event->name, $filename_2);
                    }

                    $file_upload = new FileUpload;
                    $file_upload->participant_id = $participant->participant_id;
                    $file_upload->filename_1 = $filename_1;
                    $file_upload->fileurl_1 = 'uploads/' . $event->name . '/' . $filename_1;
                    if($request->hasFile('file_2')){
                        $file_upload->filename_2 = $filename_2;
                        $file_upload->fileurl_2 = 'uploads/' . $event->name . '/' . $filename_2;
                    }
                    $file_upload->save();

                    $participant->is_done = 10;
                    $participant->save();

                    return back()->with('toast', 'Thank you for uploading your best results');
                } else {
                    return redirect('/');
                }
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function actionRegist(Request $request, $event_id = 0)
    {
        $input = (object) $request->input();
        $member_name = $input->name;
        $member_birthday = $input->birthday;
        $member_organization = $input->organization;
        $member_status = $input->status;
        $member_participate_as = $input->participate_as;
        $member_phone = $input->phone;
        // $member_faculty         = $input->faculty;
        $member_generation = $input->generation;
        $social_media = $input->social_media;
        $member_nim = $input->nim;
        $member_photos = $request->file('file');
        $member_profiles = $request->file('profile');

        $account = Auth::user();
        $events = Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
        if ($event = $events->where('event_id', $event_id)->first()) {
            if ($participant = Participant::where(['event_id' => $event_id, 'account_id' => $account->account_id])->first()) {

                // if ($check_member = ParticipantMember::where(['participant_id' => $participant->participant_id])->first()) {
                //     return back()->with('toast', 'Save your data successfully');
                // }
                $members = array();

                for ($i = 0; $i < sizeof($member_participate_as); $i++) {
                    if (empty($member_birthday[$i])) {
                        return back()->with('toast', 'Please fill in your date of birth');
                    }

                    if (empty($member_phone[$i])) {
                        return back()->with('toast', 'Please fill in your phone number');
                    }

                    if (isset($member_photos[$i])) {
                        $ext = $member_photos[$i]->getClientOriginalExtension();
                        $filename = $member_nim[$i] . '-' . rand(100, 999) . "." . $ext;
                        $member_photos[$i]->move('uploads/member-photos', $filename);

                        $image1 = new Image;
                        $image1->name = $filename;
                        $image1->type = $ext;
                        $image1->path = "member-photos/" . $filename;
                        $image1->save();
                    }
                    
                    if (isset($member_profiles[$i])) {
                        $ext = $member_profiles[$i]->getClientOriginalExtension();
                        $filename = $member_nim[$i] . '-' . rand(100, 999) . "." . $ext;
                        $member_profiles[$i]->move('uploads/member-photos', $filename);

                        $image2 = new Image;
                        $image2->name = $filename;
                        $image2->type = $ext;
                        $image2->path = "member-photos/" . $filename;
                        $image2->save();
                    }

                    $now = Carbon::now('Asia/Jakarta');
                    $member = array(
                        'participant_id' => $participant->participant_id,
                        'name' => strtoupper($member_name[$i]),
                        'nim' => $member_nim[$i],
                        'organization' => strtoupper($member_organization[$i]),
                        'organization_status' => $member_status[$i],
                        'faculty' => null,
                        'generation' => $member_generation[$i],
                        'participate_as' => $member_participate_as[$i],
                        'birthday' => $member_birthday[$i],
                        'phone' => $member_phone[$i],
                        'social_media' => $social_media[$i],
                        'photo' => $image1->path,
                        'profile' => $image2->path,
                        'created_at' => $now,
                        'updated_at' => $now,
                    );
                    $members[] = $member;
                }

                ParticipantMember::insert($members);
                return redirect('event/' . $event_id)->with('toast', 'Save your data successfully');
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function actionDeleteFileUpload(Request $request, $id)
    {
        if ($item = FileUpload::find($id)) {
            $participant = Participant::find($item->participant_id);
            $participant->is_done = 1;
            $participant->save();

            $item->delete();

            return back()->with('toast', 'Your file have deleted successfully...');
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Error',
            ]);
        }
    }

    public function actionSaveMember(Request $request)
    {
        $input = (object) $request->input();
        if ($item = ParticipantMember::find($input->id)) {
            if ($request->has('file')) {
                $ext = $request->file('file')->getClientOriginalExtension();
                $filename = rand(100, 999) . "." . $ext;
                $request->file('file')->move('uploads/member-photos', $filename);

                $image = new Image;
                $image->name = $filename;
                $image->type = $ext;
                $image->path = "member-photos/" . $filename;
                $image->save();

                $item->photo = $image->path;
            }

            if ($request->has('profile')) {
                $ext = $request->file('profile')->getClientOriginalExtension();
                $filename = rand(100, 999) . "." . $ext;
                $request->file('profile')->move('uploads/member-photos', $filename);

                $image = new Image;
                $image->name = $filename;
                $image->type = $ext;
                $image->path = "member-photos/" . $filename;
                $image->save();

                $item->profile = $image->path;
            }

            $item->name = strtoupper($input->name);
            $item->nim = $input->nim;
            $item->organization = strtoupper($input->organization);
            $item->generation = $input->generation;
            $item->birthday = $input->birthday;
            $item->phone = $input->phone;
            $item->social_media = $input->social_media;

            $item->save();

            return redirect('event/'. $item->participant->event_id)->with('toast', 'Save data successfully');
        } else {
            return back()->with('toast', 'Opps, failed to update data');
        }
    }

}
