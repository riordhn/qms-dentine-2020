<?php

namespace App\Http\Controllers\Participant;

use App\Models\Voucher;
use App\Services\Midtrans\Midtrans;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Midtrans\Transaction as MidtransTransaction;
use Midtrans\Notification as MidtransNotification;

class PaymentController extends BaseController
{
    public function checkPayment(Request $request, $order_id){
        $midtrans = new Midtrans();
        $status = MidtransTransaction::status($order_id);
        dd($status);
    }

    public function paymentNotification(){
        $midtrans = new Midtrans();
        $notif = new MidtransNotification();

        $transaction = $notif->transaction_status;
        $fraud = $notif->fraud_status;

        error_log("Order ID $notif->order_id: "."transaction status = $transaction, fraud staus = $fraud");

        if ($transaction == 'capture') {
            if ($fraud == 'challenge') {
                echo "Set payment status in merchant's database to 'challenge'";
            } else if ($fraud == 'accept') {
                $voucher = Voucher::where('payment_order_id', $notif->order_id)->first();
                $voucher->is_paid = 10;
                $voucher->save();
            }
        } else if ($transaction == 'cancel') {
            if ($fraud == 'challenge') {
                echo "Set payment status in merchant's database to 'failure'";
            } else if ($fraud == 'accept') {
                echo "Set payment status in merchant's database to 'failure'";
            }
        } else if ($transaction == 'deny') {
            echo "Set payment status in merchant's database to 'failure'";
        }
    }

}
