<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;

use Auth;
use Session;

class AuthGlobalController extends BaseController{
    public function actionSignOut(Request $request){
        Auth::logout();
        return redirect('/')->with('Sign out successfully');
    }
}