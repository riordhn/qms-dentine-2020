<?php

namespace App\Http\Controllers\Organizer;

use App\Models\Account;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;

class AccountController extends BaseController
{
    public function indexSignIn(Request $request)
    {
        if (Auth::check()) {
            if (empty(session('user'))) {
                $account = Auth::user();
                if ($account->type == 1) {
                    return redirect('welcome');
                }
                session(['user' => json_encode($account)]);
            }

            $account = (object) json_decode(session('user'));
            if ($account->type == 1) {
                return redirect('welcome');
            } else if ($account->type == 10) {
                return redirect('organizer/welcome');
            } else {
                return false;
            }
        } else {
            return view('organizer/signin');
        }
    }

    public function indexProfile(Request $request)
    {
        $account = Auth::user();
        return view('organizer/profile', compact('account'));
    }

    public function indexPassword(Request $request)
    {
        return view('organizer/password');
    }

    public function actionSignIn(Request $request)
    {
        $input = (object) $request->input();
        if ($account = Account::where(['email' => $input->email, 'type' => 10, 'status' => 1])->first()) {
            if (Hash::check($input->password, $account->password)) {
                Auth::loginUsingId($account->account_id);
                session(['user' => json_encode($account)]);
                return redirect('organizer/welcome');
            } else {
                return back()->with('toast', 'Invalid your password');
            }
        } else {
            return back()->with('toast', 'Sign in failed');
        }
    }

    public function actionSignOut(Request $request)
    {
        Auth::logout();
        Session::flush();
        return redirect('/')->with('Sign out successfully');
    }

    public function actionChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'new_confirm_password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $account = Auth::user();
        $input = (object) $request->input();
        if ($input->new_password == $input->new_confirm_password) {
            if (Auth::once(['email' => $account->email, 'password' => $input->old_password])) {
                $account->password = Hash::make($input->new_password);
                $account->save();
                return back()->with('toast', 'Change password successfully');
            } else {
                return back()->with('toast', 'Your old password is incorrect');
            }
        } else {
            return back()->with('toast', 'Re-type your new password again');
        }
    }

    public function actionSaveProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();

        $account = Auth::user();
        $account->name = $input->name;
        $account->email = $input->email;
        $account->phone = $input->phone;
        $account->save();

        $account = $account->only('account_id', 'name', 'email', 'type');
        session(['user' => json_encode($account)]);
        return back()->with('toast', 'Your change saved successfully');
    }
}
