<?php

namespace App\Http\Controllers\Organizer;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Yajra\Datatables\Datatables;
use App\Libraries\UploadHandler;
use App\Models\Account;
use App\Models\Question;
use App\Models\Image;
use App\Models\QuestionCategory;
use App\Models\QuestionOption;

use Auth;
use Carbon\Carbon;
use DB;
use PDF;
use Validator;

class QuestionController extends BaseController{
    public function indexList(Request $request){
        return view('organizer/question');
    }

    public function indexNew(Request $request, $account_id = 0, $event_id = 0){
        $question_categories = QuestionCategory::get();
        return view('organizer/question-new', compact('question_categories'));
    }

    public function indexManage(Request $request, $question_id = 0){
        $question_categories = QuestionCategory::get();
        if($item = Question::find($question_id)){
            $question_options = QuestionOption::where('question_id', $item->question_id)->orderBy('question_option_id')->get();
        }else{
            $question_options = null;
        }
        return view('organizer/question-manage', compact('item', 'question_categories', 'question_options'));
    }

    public function indexTest(Request $request, $question_id = 0){
        $question = Question::find($question_id);
        $question_options = QuestionOption::where('question_id', $question->question_id)->get();
        
        return view('organizer/question-detail', compact('question', 'question_options'));
    }

    public function commonList(Request $request){
        $list_data = Question::join('question_categories', 'question_categories.question_category_id', '=', 'questions.question_category_id');

        return Datatables::of($list_data)
                ->addColumn('type', function($item){
                    return $item->type_to_text();
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->question_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'question' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();
        if(!empty($input->question_id) && $question = Question::find($input->question_id)){
            $bom = '\xEF\xBB\xBF';
            
            $question->question_category_id = $input->category;
            $question->content = $input->question;
            $question->text = str_replace($bom, '', ucfirst(strtolower(stripslashes(strip_tags($input->question)))));
            $question->save();
            
            if($question->question_type == 1){
                $true_answer_id = 0;
                foreach($input->answer as $no_answer => $answer){
                    $question_option = QuestionOption::find($input->answer_id[$no_answer]);
                    $question_option->question_id = $question->question_id;
                    $question_option->content = $answer;
                    $question_option->text = str_replace($bom, '', ucfirst(strtolower(stripslashes(strip_tags($answer)))));
                    if($input->true_answer == $no_answer){
                        $question_option->correct = 1;
                        $true_answer_id = $question_option->question_option_id;
                    }else{
                        $question_option->correct = 0;
                    }
                    $question_option->save();
                }
                
                $question->true_answer_id = $true_answer_id;
            }
            $question->save();
            
            return back()->with('toast', 'Your changed save successfully');
        }else{
            $bom = '\xEF\xBB\xBF';
            
            $question = new Question;
            $question->question_category_id = $input->category;
            $question->content = $input->question;
            $question->text = str_replace($bom, '', ucfirst(strtolower(stripslashes(strip_tags($input->question)))));
            $question->save();
            
            $true_answer_id = 0;
            foreach($input->answer as $no_answer => $answer){
                $question_option = new QuestionOption;
                $question_option->question_id = $question->question_id;
                $question_option->content = $answer;
                $question_option->text = str_replace($bom, '', ucfirst(strtolower(stripslashes(strip_tags($answer)))));
                if($input->true_answer == $no_answer){
                    $question_option->correct = 1;
                }else{
                    $question_option->correct = 0;
                }
                $question_option->save();

                if($input->true_answer == $no_answer){
                    $true_answer_id = $question_option->question_option_id;
                }
            }
            
            $question->true_answer_id = $true_answer_id;
            $question->save();
            return redirect('organizer/question');
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($question = Question::find($input->question_id)){
            $question->delete();

            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully'
            ]);
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'Error'
            ]);
        }   
    }

    public function actionUploadImage(Request $request){
        $input = (object) $request->input();

        $tanggal = Carbon::today()->format('Y-m-d');
        $upload_handler = new UploadHandler;
        $upload_handler->setDateDir($tanggal);
        $info = $upload_handler->post();
        if(isset($info[0]->name) && (!isset($info[0]->error)))
        {
            $image = new Image;
            $name = substr($info[0]->name, 0 , strripos($info[0]->name, '.'));
            $path_parts = pathinfo($info[0]->name);
            $name = $path_parts['filename'];
            $type = $path_parts['extension'];
            $image->name = $name;
            $image->type = $type;
            $image->path = $tanggal."/".$name.".".$type;
            $image->save();
        }
        return url('uploads/img/origins').'/'.$image->path;
    }

}