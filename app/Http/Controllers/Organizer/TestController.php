<?php

namespace App\Http\Controllers\Organizer;

use App\Exports\ViewToXlsx;
use App\Models\Account;
use App\Models\Participant;
use App\Models\QuestionPackage;
use App\Models\Test;
use App\Models\TestAnswer;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Yajra\Datatables\Datatables;

class TestController extends BaseController
{
    public function indexList(Request $request)
    {
        return view('organizer/test');
    }

    public function indexDetail(Request $request, $account_id = 0)
    {
        if ($test = Test::where('account_id', $account_id)->first()) {
            $participant = Participant::where(['account_id' => $account_id, 'event_id' => 1])->first();
            $question_package = QuestionPackage::find($test->question_package_id);
            $test_answers = TestAnswer::with('question')->where(['account_id' => $account_id, 'test_id' => $test->test_id])->get();
            return view('organizer/test-detail', compact('question_package', 'test_answers', 'participant'));
        } else {
            $message = 'Belum mengerjakan test';
            return view('organizer/blank-page', compact('message'));
        }
    }

    public function indexDownloadExcel(Request $request, $account_id = 0)
    {
        $list_data = Account::selectRaw('accounts.account_id, accounts.email, accounts.name, accounts.phone, accounts.organization, accounts.address, tests.result, tests.null_answer, tests.false_answer, tests.true_answer, question_packages.title, tests.test_id, tests.flags')
            ->join('vouchers', 'vouchers.voucher_id', '=', 'accounts.voucher_id')
            ->join('voucher_events', 'voucher_events.voucher_id', '=', 'accounts.voucher_id')
            ->leftJoin('tests', 'tests.account_id', '=', 'accounts.account_id')
            ->leftJoin('question_packages', 'question_packages.question_package_id', '=', 'tests.question_package_id')
            ->where('voucher_events.event_id', 1)
            ->orderByDesc('tests.result')
            ->orderByDesc('accounts.name')->get();

        $filename = 'Hasil Test DENTINE 2024-2025';

        return Excel::download(new ViewToXlsx('prints.test-result-excel', ['list_data' => $list_data]), $filename . '.xlsx');
    }

    public function commonList(Request $request)
    {
        $list_data = Account::selectRaw('accounts.account_id, accounts.email, accounts.name, accounts.organization, tests.result, tests.test_id, tests.flags')
            ->join('vouchers', 'vouchers.voucher_id', '=', 'accounts.voucher_id')
            ->join('voucher_events', 'voucher_events.voucher_id', '=', 'accounts.voucher_id')
            ->leftJoin('tests', 'tests.account_id', '=', 'accounts.account_id')
            ->where('voucher_events.event_id', 1)
            ->orderByDesc('tests.result')
            ->orderByDesc('accounts.name');

        return Datatables::of($list_data)
            ->addColumn('status', function ($item) {
                if (!empty($item->test_id)) {
                    return 'Selesai mengerjakan';
                } else {
                    return 'Belum mengerjakan';
                }
            })
            ->addColumn('action', function ($item) {
                $data = array(
                    'id' => $item->account_id,
                );
                return $data;
            })
            ->make(true);
    }

    public function actionRefresh(Request $request)
    {
        $refresh = DB::update('UPDATE tests t
        INNER JOIN (SELECT test_answers.test_id, SUM(test_answers.value) nilai FROM test_answers GROUP BY test_answers.test_id) ta
        ON ta.test_id = t.test_id
        SET t.result = ta.nilai');

        $refresh = DB::update('UPDATE tests t
        INNER JOIN (SELECT test_answers.test_id, COUNT(test_answers.test_answer_id) total_soal FROM test_answers WHERE test_answers.correct = 0 GROUP BY test_answers.test_id) tn
        ON tn.test_id = t.test_id
        SET t.null_answer = tn.total_soal');

        $refresh = DB::update('UPDATE tests t
        INNER JOIN (SELECT test_answers.test_id, COUNT(test_answers.test_answer_id) total_soal FROM test_answers WHERE test_answers.correct = 1 GROUP BY test_answers.test_id) tt
        ON tt.test_id = t.test_id
        SET t.true_answer = tt.total_soal');

        $refresh = DB::update('UPDATE tests t
        INNER JOIN (SELECT test_answers.test_id, COUNT(test_answers.test_answer_id) total_soal FROM test_answers WHERE test_answers.correct = 2 GROUP BY test_answers.test_id) tf
        ON tf.test_id = t.test_id
        SET t.false_answer = tf.total_soal');

        return back();
    }

}
