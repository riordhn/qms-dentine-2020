<?php

namespace App\Http\Controllers\Organizer;

use App\Models\Account;
use App\Models\Event;
use App\Models\EventFee;
use App\Models\Participant;
use App\Models\ParticipantMember;
use App\Models\Regional;
use App\Models\Voucher;
use App\Models\VoucherEvent;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use PDF;
use Yajra\Datatables\Datatables;

class VoucherController extends BaseController
{
    public function indexList(Request $request)
    {
        return view('organizer/voucher');
    }

    public function indexNew(Request $request)
    {
        $events = Event::get();
        $regionals = Regional::get();
        return view('organizer/new-voucher', compact('events', 'regionals'));
    }

    public function indexInvoice(Request $request, $voucher_id = 0)
    {
        $voucher = Voucher::select(
            'vouchers.voucher_id',
            'vouchers.account_id',
            'vouchers.code',
            'is_paid',
            'vouchers.created_at',
            'vouchers.updated_at',
            'accounts.name',
            'accounts.country_id',
            'cities.area_id',
            'phone',
            'organization',
            'address',
            'vouchers.addon_status',
            'voucher_events.event_id'
        )
            ->leftJoin('accounts', 'accounts.account_id', '=', 'vouchers.account_id')
            ->leftJoin('cities', 'accounts.city_id', '=', 'cities.city_id')
            ->leftJoin('voucher_events', 'voucher_events.voucher_id', '=', 'vouchers.voucher_id')
            ->where('vouchers.voucher_id', $voucher_id)
            ->orderBy('vouchers.voucher_id', 'desc')
            ->first();

        // $text_1 = substr($voucher->code, 0, -8);
        // $text_2 = substr(str_replace($text_1, '', $voucher->code), 0, -4);
        // $text_3 = str_replace($text_1 . $text_2, '', $voucher->code);
        // $no_invoice = $text_1 . '-' . $text_2 . '-' . $text_3;
        $no_invoice = $voucher->code;

        $image = file_get_contents(public_path('media/invoice/header-footer.png'));
        $imagedata = base64_encode($image);
        $header_footer_img = 'data:image/png;base64, '.$imagedata;

        if($voucher->event_id == 1){
            if($voucher->is_paid == 10){
                $date_period = $voucher->updated_at->format('Y-m-d');
            }else{
                $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            }
            $event_fee = EventFee::where('event_id', $voucher->event_id)->isInThePeriod($date_period)->where('area_id', $voucher->area_id)->first();
            $image = file_get_contents(public_path('media/logo/logo-dentine-olympiad.png'));
        }else if($voucher->event_id == 2){
            if($voucher->country_id == 77){
                if($voucher->is_paid == 10){
                    $date_period = $voucher->updated_at->format('Y-m-d');
                }else{
                    $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
                }
                $event_fee = EventFee::where('event_id', $voucher->event_id)->whereNull('area_id')->isInThePeriod($date_period)->first();
            }else{
                if($voucher->is_paid == 10){
                    $date_period = $voucher->updated_at->format('Y-m-d');
                }else{
                    $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
                }
                $event_fee = EventFee::where('event_id', $voucher->event_id)->where('area_id', 'INTERNATIONAL')->isInThePeriod($date_period)->first();
            }
            $image = file_get_contents(public_path('media/logo/logo-dentine-essay.png'));
        }else if($voucher->event_id == 3){
            if($voucher->country_id == 77){
                if($voucher->is_paid == 10){
                    $date_period = $voucher->updated_at->format('Y-m-d');
                }else{
                    $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
                }
                $event_fee = EventFee::where('event_id', $voucher->event_id)->whereNull('area_id')->isInThePeriod($date_period)->first();
            }else{
                if($voucher->is_paid == 10){
                    $date_period = $voucher->updated_at->format('Y-m-d');
                }else{
                    $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
                }
                $event_fee = EventFee::where('event_id', $voucher->event_id)->where('area_id', 'INTERNATIONAL')->isInThePeriod($date_period)->first();
            }
            $image = file_get_contents(public_path('media/logo/logo-dentine-poster.png'));
        }else if($voucher->event_id == 4){
            if($voucher->is_paid == 10){
                $date_period = $voucher->updated_at->format('Y-m-d');
            }else{
                $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            }
            $event_fee = EventFee::where('event_id', $voucher->event_id)->isInThePeriod($date_period)->first();
            $image = file_get_contents(public_path('media/logo/logo-stourine.png'));
        }else if($voucher->event_id == 5){
            if($voucher->is_paid == 10){
                $date_period = $voucher->updated_at->format('Y-m-d');
            }else{
                $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            }
            $event_fee = EventFee::where('event_id', $voucher->event_id)->isInThePeriod($date_period)->first();
            $image = file_get_contents(public_path('media/logo/logo-debate.png'));
        }else if($voucher->event_id == 6){
            if($voucher->is_paid == 10){
                $date_period = $voucher->updated_at->format('Y-m-d');
            }else{
                $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            }
            $event_fee = EventFee::where('event_id', $voucher->event_id)->isInThePeriod($date_period)->first();
            $image = file_get_contents(public_path('media/logo/logo-stourine.png'));
        }else if($voucher->event_id == 7){
            if($voucher->is_paid == 10){
                $date_period = $voucher->updated_at->format('Y-m-d');
            }else{
                $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            }
            $event_fee = EventFee::where('event_id', $voucher->event_id)->isInThePeriod($date_period)->first();
            $image = file_get_contents(public_path('media/logo/logo-stourine.png'));
        }else if($voucher->event_id == 8){
            if($voucher->is_paid == 10){
                $date_period = $voucher->updated_at->format('Y-m-d');
            }else{
                $date_period = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            }
            $event_fee = EventFee::where('event_id', $voucher->event_id)->isInThePeriod($date_period)->first();
            $image = file_get_contents(public_path('media/logo/logo-stourine.png'));
        }
        $imagedata = base64_encode($image);
        $logo_img = 'data:image/png;base64, '.$imagedata;

        $html = view('invoice', compact('voucher', 'no_invoice', 'logo_img', 'header_footer_img', 'event_fee'))->render();
        return $html;

        $pdf = PDF::loadHTML($html)->setPaper('a4', 'portrait');
        return $pdf->stream('invoice.pdf');
    }

    public function commonList(Request $request)
    {
        $events = Event::get();
        $list_data = Voucher::select(
            'vouchers.voucher_id',
            'code',
            'accounts.account_id',
            'is_paid',
            'used_at',
            'referral',
            'name',
            'email',
            'image_id',
            'image_addon_id',
            'addon_status'
        )
            ->leftJoin('accounts', 'accounts.account_id', '=', 'vouchers.account_id')
            ->with('voucher_events')
            ->with('image', 'image_addon')
            ->orderby('vouchers.is_paid', 'asc')
            ->orderby('vouchers.image_id', 'desc');
        // ->get();

        $role = get_admin_role();
        $is_admin_register = get_admin_role() == 'register01' || get_admin_role() == 'register02';
        $is_admin_dentine = get_admin_role() == 'dentine';
        $is_admin_essay_poster = get_admin_role() == 'essayposter';

        if ($role != 'superadmin') {
            if ($is_admin_dentine) {
                $list_data = $list_data->whereHas('voucher_events', function ($q) {
                    $q->whereIn('event_id', [1]);
                });
            } else if ($is_admin_essay_poster) {
                $list_data = $list_data->whereHas('voucher_events', function ($q) {
                    $q->whereIn('event_id', [2, 3]);
                });
            }
        }

        return Datatables::of($list_data)
            ->addColumn('invoice', function ($item) {
                $text_1 = substr($item->code, 0, -8);
                $text_2 = substr(str_replace($text_1, '', $item->code), 0, -4);
                $text_3 = str_replace($text_1 . $text_2, '', $item->code);
                return $text_1 . '-' . $text_2 . '-' . $text_3;
            })
            ->addColumn('events', function ($item) use ($events) {
                if (!empty($item->voucher_events)) {
                    $text = '';
                    foreach ($item->voucher_events as $voucher_event) {
                        $event = $events->where('event_id', $voucher_event->event_id)->first();
                        $text .= $event->name . ', ';
                    }

                    $text = substr($text, 0, -2);
                    return $text;
                } else {
                    return '';
                }
            })
            ->addColumn('payment', function ($item) {
                if (!empty($item->image)) {
                    return url('uploads/' . $item->image->path);
                } else {
                    return null;
                }
            })
            ->addColumn('tweetbond', function ($item) {
                if (!empty($item->image_addon)) {
                    return url('uploads/' . $item->image_addon->path);
                } else {
                    return null;
                }
            })
            ->editColumn('is_paid', function ($item) {
                if ($item->is_paid == 10) {
                    return 'YES';
                } else {
                    return 'NO';
                }
            })
            ->addColumn('used_at', function ($item) {
                if (!empty($item->used_at)) {
                    return date_format(date_create($item->used_at), 'd M Y H:i') . ' WIB';
                } else {
                    return '';
                }
            })
            ->addColumn('used_by', function ($item) {
                if (!empty($item->account_id)) {
                    return $item->name;
                } else {
                    return '';
                }
            })
            ->addColumn('action', function ($item) {
                $data = array();
                $data['id'] = $item->voucher_id;
                if (!empty($item->voucher_events)) {
                    $data['voucher_events'] = $item->voucher_events->pluck(['event_id'])->all();
                } else {
                    $data['voucher_events'] = null;
                }

                if ($item->is_paid == 10) {
                    $data['status'] = 0;
                } else {
                    $data['status'] = 1;
                }

                return $data;
            })
            ->make(true);
    }

    public function actionGenerate(Request $request)
    {
        $input = (object) $request->input();

        DB::beginTransaction();

        try {
            $event = Event::find($input->event);
            $prefix = $event->code;

            $voucher = Voucher::where('code', 'LIKE', $prefix . '%')->orderBy('code', 'desc')->first();

            if ($voucher) {
                $number_code = (int) substr(str_replace($prefix, '', $voucher->code), 0, 4);
                $number_code++;

                if ($number_code > 999) {
                    $code = $prefix . $number_code . $this->RandomString();
                } else if ($number_code > 99) {
                    $code = $prefix . '0' . $number_code . $this->RandomString();
                } else if ($number_code > 9) {
                    $code = $prefix . '00' . $number_code . $this->RandomString();
                } else {
                    $code = $prefix . '000' . $number_code . $this->RandomString();
                }
            } else {
                $code = $prefix . '0001' . $this->RandomString();
            }

            $new_voucher = new Voucher;
            $new_voucher->code = $code;
            $new_voucher->save();

            $voucher_event = new VoucherEvent;
            $voucher_event->voucher_id = $new_voucher->voucher_id;
            $voucher_event->event_id = $event->event_id;
            $voucher_event->save();

            $event->participate = $event->participate + 1;
            $event->save();

            DB::commit();

            return back()->with('toast', 'Voucher generate successfully');
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('toast', (env('APP_DEBUG', 'true') == 'true') ? $e->getMessage() : 'Operation error');
        }
    }

    public function RandomString()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 4; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }

    public function actionPay(Request $request)
    {
        $input = (object) $request->input();
        if ($voucher = Voucher::where(['voucher_id' => $input->voucher, 'is_paid' => 1])->first()) {
            if($input->type == 'addon'){
                if(!empty($voucher->image_addon_id) && $voucher->addon_status == 0){
                    $voucher->addon_status = 1;
                }
            }

            if($input->type == 'pay'){
                $voucher->is_paid = 10;
            }
            $voucher->save();
        }

        return response()->json([
            'status' => 200,
            'message' => 'Payment successfully',
        ]);
    }

    public function actionDelete(Request $request)
    {
        $input = (object) $request->input();
        if ($voucher = Voucher::where(['voucher_id' => $input->voucher])->first()) {
            if (!empty($voucher->account_id)) {
                if ($account = Account::find($voucher->account_id)) {
                    $account->delete();
                }

                foreach (Participant::where('account_id', $voucher->account_id)->get() as $participant) {
                    foreach (ParticipantMember::where('participant_id', $participant->participant_id)->get() as $participant_member) {
                        $participant_member->delete();
                    }
                    $participant->delete();
                }
            }
            $voucher->delete();
        }

        return response()->json([
            'status' => 200,
            'message' => 'Delete successfully',
        ]);
    }

}
