<?php

namespace App\Http\Controllers\Organizer;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Yajra\Datatables\Datatables;
use App\Models\QuestionCategory;

use Auth;
use DB;
use Validator;

class QuestionCategoryController extends BaseController{
    public function indexList(Request $request){
        return view('organizer/question-category');
    }

    public function indexManage(Request $request, $question_category_id = 0){
        $item = QuestionCategory::find($question_category_id);
        return view('organizer/question-category-manage', compact('item'));
    }

    public function commonList(Request $request){
        $list_data = QuestionCategory::selectRaw('question_category_id,
                                                    name,
                                                    true_value,
                                                    false_value,
                                                    null_value');

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->question_category_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'true_value' => 'required',
            'false_value' => 'required',
            'null_value' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();

        if($question_category = QuestionCategory::find($input->question_category_id)){
            $question_category->name = $input->name;
            $question_category->true_value = $input->true_value;
            $question_category->false_value = $input->false_value;
            $question_category->null_value = $input->null_value;
            $question_category->save();

            return back()->with('toast', 'Your changed save successfully');
        }else{
            $question_category = new QuestionCategory;
            $question_category->name = $input->name;
            $question_category->true_value = $input->true_value;
            $question_category->false_value = $input->false_value;
            $question_category->null_value = $input->null_value;
            $question_category->save();

            return back()->with('toast', 'Save item successfully');
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($question_category = QuestionCategory::find($input->question_category_id)){
            $question_category->delete();

            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully'
            ]);
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'Error'
            ]);
        }
        
    }
}