<?php

namespace App\Http\Controllers\Organizer;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Yajra\Datatables\Datatables;
use App\Models\Question;
use App\Models\QuestionPackage;
use App\Models\QuestionPackageDetail;

use Auth;
use DB;
use Validator;

class QuestionPackageController extends BaseController{
    public function indexList(Request $request){
        return view('organizer/question-package');
    }

    public function indexDetail(Request $request, $question_package_id = 0){
        if($question_package = QuestionPackage::find($question_package_id)){
            return view('organizer/question-package-detail', compact('question_package'));
        }else{
            return abort();
        }
    }

    public function indexManage(Request $request, $question_package_id = 0){
        $item = QuestionPackage::find($question_package_id);
        return view('organizer/question-package-manage', compact('item'));
    }

    public function commonList(Request $request){
        $list_data = QuestionPackage::selectRaw('question_packages.question_package_id, title, count(question_package_details.question_package_id) total')
                                        ->leftJoin('question_package_details', 'question_package_details.question_package_id', '=', 'question_packages.question_package_id')
                                        ->groupBy('question_packages.question_package_id', 'title')
                                        ->orderBy('title');

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->question_package_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function detailList(Request $request, $question_package_id = 0, $tipe){
        $question_package_details = QuestionPackageDetail::where('question_package_id', $question_package_id)->get();
        $list_question_selected = $question_package_details->pluck('question_id');
        if($tipe == 1){
            $list_data = Question::select('question_id', 'name', 'content')
                                    ->join('question_categories', 'question_categories.question_category_id', '=', 'questions.question_category_id')
                                    ->whereNotIn('question_id', $list_question_selected)
                                    ->orderBy('question_id', 'asc');
        }else{
            $list_data = Question::select('question_id', 'name', 'content')
                                    ->join('question_categories', 'question_categories.question_category_id', '=', 'questions.question_category_id')
                                    ->whereIn('question_id', $list_question_selected)
                                    ->orderBy('question_id', 'asc');
        }

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->question_id
                    );
                    return $data;
                })
                ->rawColumns(['content'])
                ->make(true);
    }

    public function actionSave(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();

        if($question_package = QuestionPackage::find($input->question_package_id)){
            $question_package->title = $input->title;
            $question_package->save();

            return back()->with('toast', 'Your changed save successfully');
        }else{
            $question_package = new QuestionPackage;
            $question_package->title = $input->title;
            $question_package->save();

            return back()->with('toast', 'Save item successfully');
        }
    }

    public function actionDelete(Request $request){
        $input = (object) $request->input();
        if($question_package = QuestionPackage::find($input->question_package_id)){
            $question_package->delete();

            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully'
            ]);
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'Error'
            ]);
        }    
    }

    public function actionDetailAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'question_package_id' => 'required',
            'question_id' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();
        if($question_package_detail = QuestionPackageDetail::where(['question_package_id' => $input->question_package_id, 'question_id' => $input->question_id])->first()){
            return response()->json([
                'status' => 500,
                'message' => 'Error'
            ]);
        }else{
            $question_package_detail = new QuestionPackageDetail;
            $question_package_detail->question_package_id = $input->question_package_id;
            $question_package_detail->question_id = $input->question_id;
            $question_package_detail->save();

            return response()->json([
                'status' => 200,
                'message' => 'Add successfully'
            ]);
        }
    }

    public function actionDetailDelete(Request $request){
        $validator = Validator::make($request->all(), [
            'question_package_id' => 'required',
            'question_id' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();
        if($question_package_detail = QuestionPackageDetail::where(['question_package_id' => $input->question_package_id, 'question_id' => $input->question_id])->first()){
            $question_package_detail->delete();
            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully'
            ]);
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'Error'
            ]);
        }
    }
}