<?php

namespace App\Http\Controllers\Organizer;

use App\Models\FileUpload;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Yajra\Datatables\Datatables;

class FileUploadController extends BaseController
{
    public function indexList(Request $request)
    {
        return view('organizer/fileupload');
    }

    public function commonList(Request $request)
    {
        $list_data = FileUpload::selectRaw('
                            file_uploads.file_upload_id,
                            accounts.name,
                            accounts.email,
                            accounts.phone,
                            accounts.organization,
                            participants.account_id,
                            participants.event_id,
                            events.name AS event,
                            file_uploads.fileurl_1,
                            file_uploads.fileurl_2,
                            file_uploads.created_at
                            ')
            ->join('participants', 'file_uploads.participant_id', '=', 'participants.participant_id')
            ->join('events', 'participants.event_id', '=', 'events.event_id')
            ->join('accounts', 'participants.account_id', '=', 'accounts.account_id')
            ->orderby('file_uploads.created_at', 'desc')
            ->get();

        return Datatables::of($list_data)
            ->addColumn('submit_at', function ($item) {
                return date_format(date_create($item->created_at), 'd M Y H:i') . ' WIB';
            })
            ->addColumn('action', function ($item) {
                $data = array(
                    'id' => $item->file_upload_id,
                    'account' => $item->account_id,
                    'event' => $item->event_id,
                    'file_1' => !empty($item->fileurl_1) ? url($item->fileurl_1) : '',
                    'file_2' => !empty($item->fileurl_2) ? url($item->fileurl_2) : '',
                );

                return $data;
            })
            ->make(true);
    }

    public function actionDelete(Request $request)
    {
        $input = (object) $request->input();
        if ($item = FileUpload::find($input->id)) {
            $participant = Participant::find($item->participant_id);
            $participant->is_done = 1;
            $participant->save();

            $item->delete();

            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully',
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Error',
            ]);
        }
    }

}
