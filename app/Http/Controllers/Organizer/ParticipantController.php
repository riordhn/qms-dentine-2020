<?php

namespace App\Http\Controllers\Organizer;

use App\Exports\ViewToXlsx;
use App\Models\Account;
use App\Models\Event;
use App\Models\FileUpload;
use App\Models\Image;
use App\Models\Participant;
use App\Models\ParticipantMember;
use App\Models\Regional;
use App\Models\Voucher;
use App\Models\VoucherEvent;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Yajra\Datatables\Datatables;

class ParticipantController extends BaseController
{
    public function indexList(Request $request)
    {
        $regionals = Regional::orderBy('name')->get();
        return view('organizer/participant', compact('regionals'));
    }

    public function indexManage(Request $request, $id = 0)
    {
        $item = ParticipantMember::find($id);
        return view('organizer/manage-participant-member', compact('item'));
    }

    public function indexDetail(Request $request, $account_id = 0, $event_id = 0)
    {
        $participant = Participant::where(['account_id' => $account_id, 'event_id' => $event_id])->first();
        $participant_members = ParticipantMember::where('participant_id', $participant->participant_id)->get();
        return view('organizer/participant-member', compact('participant', 'participant_members'));
    }

    public function indexPassword(Request $request, $account_id = 0)
    {
        $participant = Account::where(['account_id' => $account_id])->first();
        return view('organizer/reset-password-participant', compact('participant'));
    }

    public function indexAccountDownloadExcel(Request $request)
    {
        $list_data = Account::with('voucher', 'voucher.voucher_events', 'voucher.voucher_events.event')->where('type', 1);
        $role = get_admin_role();

        $is_admin_register = get_admin_role() == 'register01' || get_admin_role() == 'register02';
        $is_admin_dentine = get_admin_role() == 'dentine';
        $is_admin_essay_poster = get_admin_role() == 'essayposter';

        if ($role != 'superadmin') {
            if ($is_admin_dentine) {
                $voucher_event = VoucherEvent::whereIn('event_id', [1])->get();
            } else if ($is_admin_essay_poster) {
                $voucher_event = VoucherEvent::whereIn('event_id', [2, 3])->get();
            }

            $list_data = $list_data->whereIn('voucher_id', $voucher_event->pluck('voucher_id'));
        }

        $list_data = $list_data->get();

        $filename = 'List of Account Participant DENTINE 2024-2025';

        return Excel::download(new ViewToXlsx('prints.participant-account-excel', ['list_data' => $list_data]), $filename . '.xlsx');
    }

    public function indexMemberDownloadExcel(Request $request)
    {
        $list_data = Participant::with('member', 'event', 'voucher', 'account', 'account.province', 'account.city');
        $role = get_admin_role();

        $is_admin_register = get_admin_role() == 'register01' || get_admin_role() == 'register02';
        $is_admin_dentine = get_admin_role() == 'dentine';
        $is_admin_essay_poster = get_admin_role() == 'essayposter';

        if ($role != 'superadmin') {
            if ($is_admin_dentine) {
                $list_data = $list_data->whereIn('event_id', [1]);
            } else if ($is_admin_essay_poster) {
                $list_data = $list_data->whereIn('event_id', [2, 3]);
            }
        }

        $list_data = $list_data->get();

        $filename = 'List of Team Member DENTINE 2023';

        return Excel::download(new ViewToXlsx('prints.participant-member-excel', ['list_data' => $list_data]), $filename . '.xlsx');
    }

    public function commonList(Request $request)
    {
        $list_data = Account::with('country', 'participant', 'voucher', 'province', 'city', 'voucher.voucher_events', 'voucher.voucher_events.event', 'rayon')->where('type', 1);
        $role = get_admin_role();
        $is_admin_register = get_admin_role() == 'register01' || get_admin_role() == 'register02';
        $is_admin_dentine = get_admin_role() == 'dentine';
        $is_admin_essay_poster = get_admin_role() == 'essayposter';

        if ($role != 'superadmin') {
            if ($is_admin_dentine) {
                $list_data = $list_data->whereHas('voucher.voucher_events', function ($q) {
                    $q->whereIn('event_id', [1]);
                });
            } else if ($is_admin_essay_poster) {
                $list_data = $list_data->whereHas('voucher.voucher_events', function ($q) {
                    $q->whereIn('event_id', [2, 3]);
                });
            }
        }

        return Datatables::of($list_data)
            ->addColumn('event_name', function ($item) {
                return $item->voucher->voucher_events->first()->event->name;
            })
            ->addColumn('is_paid', function ($item) {
                if ($item->voucher->is_paid == 10) {
                    return 'YES';
                } else {
                    return 'NO';
                }
            })
            ->addColumn('regist_from', function ($item) {
                return date_format(date_create($item->created_at), 'd M Y H:i') . ' WIB';
            })
            ->addColumn('action', function ($item) {
                if (!empty($item->participant->first())) {
                    $event = $item->participant->first()->event_id;
                } else {
                    $event = 0;
                }
                $data = array(
                    'id' => $item->account_id,
                    'event' => $event,
                    'status' => $item->voucher->is_paid,
                );
                return $data;
            })
            ->make(true);
    }

    public function participateList(Request $request)
    {
        $input = (object) $request->input();
        $validator = Validator::make($request->all(), [
            'account' => 'required',
        ]);
        $list_data = Participant::selectRaw('participants.event_id as id, CONCAT(\'Lomba \', events.name) as name')
            ->join('events', 'events.event_id', '=', 'participants.event_id')
            ->where('account_id', $input->account)
            ->get();

        return response()->json([
            'status' => 200,
            'list_data' => $list_data,
        ]);
    }

    public function actionResetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'participant' => 'required',
            'new_password' => 'required',
            'new_confirm_password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();
        if ($input->new_password == $input->new_confirm_password) {
            if ($account = Account::find($input->participant)) {
                $account->password = Hash::make($input->new_password);
                $account->save();
                return back()->with('toast', 'Reset password successfully');
            } else {
                return back()->with('toast', 'Opps, something error');
            }
        } else {
            return back()->with('toast', 'Re-type your new password again');
        }
    }

    public function actionDelete(Request $request)
    {
        $input = (object) $request->input();

        DB::beginTransaction();

        try {
            $item = Account::find($input->id);
            $voucher = Voucher::find($item->voucher_id);

            foreach (VoucherEvent::where('voucher_id', $voucher->voucher_id)->get() as $voucher_event) {
                $voucher_event->delete();
            }

            foreach (Participant::where('account_id', $voucher->account_id)->get() as $participant) {
                foreach (ParticipantMember::where('participant_id', $participant->participant_id)->get() as $participant_member) {
                    $participant_member->delete();
                }
                $participant->delete();
            }

            $voucher->delete();
            $item->delete();
            DB::commit();

            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully',
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 500,
                'message' => 'Error',
            ]);
        }
    }

    public function actionSaveMember(Request $request)
    {
        $input = (object) $request->input();
        if ($item = ParticipantMember::find($input->id)) {
            if ($request->has('file')) {
                $ext = $request->file('file')->getClientOriginalExtension();
                $filename = rand(100, 999) . "." . $ext;
                $request->file('file')->move('uploads/member-photos', $filename);

                $image = new Image;
                $image->name = $filename;
                $image->type = $ext;
                $image->path = "member-photos/" . $filename;
                $image->save();

                $item->photo = $image->path;
            }

            $item->name = strtoupper($input->name);
            $item->nim = $input->nim;
            $item->organization = strtoupper($input->organization);
            $item->generation = $input->generation;
            $item->birthday = $input->birthday;
            $item->phone = $input->phone;
            $item->social_media = $input->social_media;

            $item->save();

            return back()->with('toast', 'Save data successfully');
        } else {
            return back()->with('toast', 'Opps, failed to update data');
        }
    }


    public function actionDownloadFileZip(Request $request)
    {
        set_time_limit(-1);

        $list_data = FileUpload::where('fileurl_1', 'LIKE', 'uploads/Dentine Essay/%')
                            ->get();

        $zip_file = 'uploads/zip/FILE_ESSAY_ALL_PARTICIPANT.zip';

        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        foreach($list_data as $item){
            $data_file = $item->fileurl_1;
            $ext = pathinfo($data_file, PATHINFO_EXTENSION);

            $zip->addFile(public_path($data_file), $data_file);
        }
    
        $zip->close();
        return response()->download($zip_file);
    }

    public function actionDownloadFileZip2(Request $request)
    {
        set_time_limit(-1);

        $list_data = FileUpload::where('fileurl_1', 'LIKE', 'uploads/Dentine Poster/%')
                            ->get();

        $zip_file = 'uploads/zip/FILE_POSTER_ALL_PARTICIPANT.zip';

        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        foreach($list_data as $item){
            $data_file = $item->fileurl_1;
            $ext = pathinfo($data_file, PATHINFO_EXTENSION);

            $zip->addFile(public_path($data_file), $data_file);
        }
    
        $zip->close();
        return response()->download($zip_file);
    }
}
