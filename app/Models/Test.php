<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test
 */
class Test extends Model
{
    use SoftDeletes;
    
    protected $table = 'tests';

    protected $primaryKey = 'test_id';

	public $timestamps = true;

    protected $fillable = [
        'account_id',
        'question_package_id',
        'result',
        'start_at',
        'end_at'
    ];

    protected $guarded = [];

}