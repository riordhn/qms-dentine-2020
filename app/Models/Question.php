<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Question
 */
class Question extends Model
{
    use SoftDeletes;
    
    protected $table = 'questions';

    protected $primaryKey = 'question_id';

	public $timestamps = true;

    protected $fillable = [
        'question_category_id',
        'true_answer_id',
        'content',
        'text'
    ];

    protected $guarded = [];

    public function type_to_text(){
        if($this->question_type == 1){
            return 'Multiple Choice';
        }else{
            return 'Short Answer';
        }
    }
}