<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Regional
 */
class Regional extends Model
{
    use SoftDeletes;
    
    protected $table = 'regionals';

    protected $primaryKey = 'regional_id';

	public $timestamps = true;

    protected $fillable = [
        'code',
        'name',
        'limit_participant',
        'participate',
        'note'
    ];

    protected $guarded = [];

}