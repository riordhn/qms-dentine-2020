<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class QuestionPackageDetail
 */
class QuestionPackageDetail extends Model
{   
    protected $table = 'question_package_details';

    protected $primaryKey = 'question_package_detail_id';

    public $timestamps = false;

    protected $fillable = [
        'question_package_id',
        'question_id'
    ];

    protected $guarded = [];

    public function question(){
        return $this->belongsTo('App\Models\Question', 'question_id');
    }
}