<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Participant
 */
class Participant extends Model
{
    use SoftDeletes;

    protected $table = 'participants';

    protected $primaryKey = 'participant_id';

    public $timestamps = false;

    protected $fillable = [
        'event_id',
        'account_id',
        'name',
        'is_done'
    ];

    protected $guarded = [];

    public function event(){
        return $this->belongsTo('App\Models\Event', 'event_id');
    }

    public function account(){
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    public function voucher(){
        return $this->hasOne('App\Models\Voucher', 'account_id', 'account_id');
    }

    public function member(){
        return $this->hasMany('App\Models\ParticipantMember', 'participant_id');
    }

}