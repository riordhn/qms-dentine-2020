<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class City
 */
class City extends Model
{
    use SoftDeletes;
    
    protected $table = 'cities';

    protected $primaryKey = 'city_id';

	public $timestamps = true;

    protected $fillable = [
        'province_id',
        'name',
    ];

    protected $guarded = [];

}