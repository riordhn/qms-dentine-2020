<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ParticipantMember
 */
class ParticipantMember extends Model
{
    use SoftDeletes;
    
    protected $table = 'participant_members';

    protected $primaryKey = 'participant_member_id';

    public $timestamps = false;

    protected $fillable = [
        'participant_id',
        'name',
        'photo',
        'organization',
        'organization_status',
        'generation',
        'birthday',
        'email',
        'phone',
        'participate_as',
        'note'
    ];

    protected $guarded = [];

    public function event(){
        return $this->belongsTo('App\Models\Event', 'event_id');
    }

    public function participant(){
        return $this->belongsTo('App\Models\Participant', 'participant_id');
    }

}