<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QuestionOption
 */
class QuestionOption extends Model
{
    use SoftDeletes;
    
    protected $table = 'question_options';

    protected $primaryKey = 'question_option_id';

	public $timestamps = true;

    protected $fillable = [
        'question_id',
        'content',
        'text',
        'correct'
    ];

    protected $guarded = [];

}