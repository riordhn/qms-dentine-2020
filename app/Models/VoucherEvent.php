<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VoucherEvent
 */
class VoucherEvent extends Model
{
    protected $table = 'voucher_events';

    protected $primaryKey = 'voucher_event_id';

    public $timestamps = false;

    protected $fillable = [
        'voucher_id',
        'event_id'
    ];

    protected $guarded = [];

    public function event(){
        return $this->belongsTo('App\Models\Event', 'event_id');
    }

}