<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QuestionPackage
 */
class QuestionPackage extends Model
{
    use SoftDeletes;
    
    protected $table = 'question_packages';

    protected $primaryKey = 'question_package_id';

	public $timestamps = true;

    protected $fillable = [
        'title'
    ];

    protected $guarded = [];

}