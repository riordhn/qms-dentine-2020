<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TestAnswer
 */
class TestAnswer extends Model
{
    use SoftDeletes;
    
    protected $table = 'test_answers';

    protected $primaryKey = 'test_answer_id';

	public $timestamps = true;

    protected $fillable = [
        'account_id',
        'test_id',
        'number',
        'question_id',
        'question_option_id',
        'correct',
        'value'
    ];

    protected $guarded = [];

    public function question_option(){
        return $this->belongsTo('App\Models\QuestionOption', 'question_option_id');
    }

    public function question(){
        return $this->belongsTo('App\Models\Question', 'question_id');
    }

}