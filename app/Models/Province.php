<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Province
 */
class Province extends Model
{
    use SoftDeletes;
    
    protected $table = 'provinces';

    protected $primaryKey = 'province_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    protected $guarded = [];

}