<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EventTime
 */
class EventTime extends Model
{   
    protected $table = 'event_times';

    protected $primaryKey = 'event_time_id';

	public $timestamps = true;
    
    protected $guarded = [];

}