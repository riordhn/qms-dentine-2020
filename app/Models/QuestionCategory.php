<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QuestionCategory
 */
class QuestionCategory extends Model
{
    use SoftDeletes;
    
    protected $table = 'question_categories';

    protected $primaryKey = 'question_category_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
        'true_value',
        'false_value',
        'null_value'
    ];

    protected $guarded = [];

}