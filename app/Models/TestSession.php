<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TestSession
 */
class TestSession extends Model
{
    
    protected $table = 'test_sessions';

    protected $primaryKey = 'test_session_id';

	public $timestamps = false;

    protected $guarded = [];

    public function event_time(){
        return $this->belongsTo('App\Models\EventTime', 'event_time_id');
    }

}