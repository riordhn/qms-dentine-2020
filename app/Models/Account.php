<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Account
 */
class Account extends Authenticatable
{
    use SoftDeletes;
    
    protected $table = 'accounts';

    protected $primaryKey = 'account_id';

	public $timestamps = true;

    protected $fillable = [
        'voucher_id',
        'name',
        'type',
        'email',
        'phone',
        'organization',
        'province_id',
        'city_id',
        'address',
        'status',
        'auth_token',
        'password'
    ];

    protected $guarded = [];

    protected $hidden = ['password', 'remember_token'];

    public function country(){
        return $this->belongsTo('App\Models\Country', 'country_id')->withDefault([
            'name' => ''
        ]);
    }

    public function province(){
        return $this->belongsTo('App\Models\Province', 'province_id')->withDefault([
            'name' => ''
        ]);
    }

    public function city(){
        return $this->belongsTo('App\Models\City', 'city_id')->withDefault([
            'name' => ''
        ]);
    }

    public function voucher(){
        return $this->belongsTo('App\Models\Voucher', 'voucher_id');
    }

    public function rayon(){
        return $this->belongsTo('App\Models\Rayon', 'rayon_id')->withDefault([
            'name' => ''
        ]);
    }

    public function participant(){
        return $this->hasMany('App\Models\Participant', 'account_id');
    }

}