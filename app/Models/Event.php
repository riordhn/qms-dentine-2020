<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Event
 */
class Event extends Model
{
    use SoftDeletes;
    
    protected $table = 'events';

    protected $primaryKey = 'event_id';

	public $timestamps = true;

    protected $fillable = [
        'code',
        'name',
        'registration_fee',
        'person_in_event',
        'limit_participant',
        'participate'
    ];

    protected $guarded = [];

    public function account(){
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

}