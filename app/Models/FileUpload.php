<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FileUpload
 */
class FileUpload extends Model
{
    use SoftDeletes;

    protected $table = 'file_uploads';

    protected $primaryKey = 'file_upload_id';

    public $timestamps = true;

    protected $fillable = [
        'participant_id',
        'filename',
        'fileurl'
    ];

    protected $guarded = [];

}