<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 */
class Country extends Model
{
    use SoftDeletes;
    
    protected $table = 'countries';

    protected $primaryKey = 'country_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    protected $guarded = [];

}