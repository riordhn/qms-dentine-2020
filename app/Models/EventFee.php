<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EventFee
 */
class EventFee extends Model
{
    use SoftDeletes;
    
    protected $table = 'event_fees';

    protected $primaryKey = 'event_fee_id';

	public $timestamps = true;

    protected $fillable = [
        'event_id',
        'area_id',
        'title',
        'start_date',
        'end_date',
        'registration_fee',
    ];

    protected $guarded = [];

    public function event(){
        return $this->belongsTo('App\Models\Event', 'event_id');
    }

    public function scopeIsInThePeriod($query, $the_date){
        return $query->where('start_date', '<=', $the_date)->where('end_date', '>=', $the_date);
    }

}