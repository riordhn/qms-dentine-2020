<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Voucher
 */
class Voucher extends Model
{
    use SoftDeletes;
    
    protected $table = 'vouchers';

    protected $primaryKey = 'voucher_id';

	public $timestamps = true;

    protected $fillable = [
        'regional_id',
        'code',
        'account_id',
        'image_id',
        'image_addon_id',
        'is_paid',
        'used_at'
    ];

    protected $guarded = [];

    public function account(){
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    public function regional(){
        return $this->belongsTo('App\Models\Regional', 'regional_id');
    }

    public function image(){
        return $this->belongsTo('App\Models\Image', 'image_id');
    }

    public function image_addon(){
        return $this->belongsTo('App\Models\Image', 'image_addon_id', 'image_id');
    }

    public function voucher_events(){
        return $this->hasMany('App\Models\VoucherEvent', 'voucher_id');
    }

}