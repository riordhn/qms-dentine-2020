<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rayon
 */
class Rayon extends Model
{
    use SoftDeletes;
    
    protected $table = 'rayons';

    protected $primaryKey = 'rayon_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    protected $guarded = [];

}