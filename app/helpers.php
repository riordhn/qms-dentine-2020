<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

if (!function_exists('get_admin_role')) {
    /**
     * get auth data from session
     * @return String
     */
    function get_admin_role()
    {
        $account = Auth::user();
        $emails = explode('@', $account->email);
        if (strpos($emails[0], '.') !== false) {
            return explode('.', $emails[0])[1];
        } else {
            return 'superadmin';
        }
    }
}

if (!function_exists('success_response')) {
    /**
     * return success response of web operation
     * @param any $message ,
     * @param int $status_code,
     * @return json
     */
    function success_response($message, $payload_data = [], $status_code = 200)
    {
        $return_array = [
            'status' => 'Success',
            'status_code' => $status_code,
            'message' => $message,
        ];

        if (!empty($payload_data)) {
            $return_array['data'] = $payload_data;
        }

        return response()->json($return_array);
    }
}

if (!function_exists('error_response')) {
    /**
     * return error response of web operation
     * @param any $message ,
     * @param int $status_code,
     * @return json
     */
    function error_response($message, $payload_data = [], $status_code = 300)
    {
        if ($message instanceof Exception) {
            $message = isDebugMode() ? $message->getMessage() : 'Failed code ' . $message->getLine();
        }

        $return_array = [
            'status' => 'Failed',
            'status_code' => $status_code,
            'message' => $message,
        ];

        if (!empty($payload_data)) {
            $return_array['data'] = $payload_data;
        }

        return response()->json($return_array);
    }
}

if (!function_exists('isDebugMode')) {
    /**
     * check is debug mode of the system active
     * @return boolean
     */
    function isDebugMode()
    {
        return env('APP_DEBUG', 'true') == 'true';
    }
}
