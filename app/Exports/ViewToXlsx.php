<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ViewToXlsx implements FromView
{
    protected $view_pathname;
    protected $array_data;

    public function __construct($view_pathname, $array_data)
    {
        $this->view_pathname = $view_pathname;
        $this->array_data = $array_data;
    }

    public function view(): View
    {
        return view($this->view_pathname, $this->array_data);
    }
}
