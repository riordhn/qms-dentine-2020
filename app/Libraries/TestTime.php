<?php

namespace App\Libraries;

use App\Models\Account;
use App\Models\TestSession;
use App\Models\VoucherEvent;

use Carbon\Carbon;

class TestTime{

    // private static $startDate = '2021-12-25 16:00';
    // private static $endDate = '2021-12-25 18:00';

    public static function getDate($account, $name){
        if($test_session = TestSession::with('event_time')->where('account_id', $account->account_id)->first()){
            if($name == 'START'){
                return $test_session->event_time->start_date;
            }else if($name == 'END'){
                return $test_session->event_time->end_date;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }

    public static function checkRunning($test){
        $account = Account::find($test->account_id);
        if($test_session = TestSession::with('event_time')->where('account_id', $account->account_id)->first()){
            // Mulai ujian dan sampai kapan
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->start_date);
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->end_date);
            // END
            $now = Carbon::now('Asia/Jakarta');
            if($voucher_event = VoucherEvent::where(['voucher_id' => $account->voucher_id, 'event_id' => 1])->first()){
                // if($account->voucher->regional_id == 20){
                    if(strtotime($start_date) < strtotime($now)){
                        if(strtotime($end_date) > strtotime($now) && strtotime($test->end_at) > strtotime($now)){
                            return (object) ['status' => true, 'code' => 200, 'message' => 'Masih dalam pengerjaan test'];
                        }else{
                            return (object) ['status' => false, 'code' => 300, 'message' => 'Test sudah selesai'];
                        }
                    }
                // }
            }
        }


        return (object) ['status' => false, 'code' => 500, 'message' => 'Belum waktunya test'];
    }

    public static function checkTime($account){
        if($test_session = TestSession::with('event_time')->where('account_id', $account->account_id)->first()){
            // Mulai ujian dan sampai kapan
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->start_date);
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->end_date);
            // END
            $now = Carbon::now('Asia/Jakarta');
            if($voucher_event = VoucherEvent::where(['voucher_id' => $account->voucher_id, 'event_id' => 1])->first()){
                // if($account->voucher->regional_id == 20){
                    if(strtotime($start_date) < strtotime($now)){
                        if(strtotime($end_date) > strtotime($now)){
                            return (object) ['status' => true, 'code' => 200, 'message' => 'Masih dalam pengerjaan test'];
                        }else{
                            return (object) ['status' => false, 'code' => 300, 'message' => 'Test sudah selesai' ];
                        }
                    }
                // }
            }
        }

        return (object) ['status' => false, 'code' => 500, 'message' => 'Belum waktunya test'];

    }

    public static function checkTimeTestResult($account){
        if($test_session = TestSession::with('event_time')->where('account_id', $account->account_id)->first()){
            // Waktu menampilkan ujian
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $test_session->event_time->start_date);
            // END
            $now = Carbon::now('Asia/Jakarta');
            if(strtotime($start_date) < strtotime($now)){
                return (object) ['status' => true, 'code' => 200, 'message' => 'OK'];
            }
        }

        return (object) ['status' => false, 'code' => 300, 'message' => 'Belum waktunya'];

    }

}