<?php

namespace App\Services\Midtrans;

use Midtrans\Snap;

class CreateTransactionService extends Midtrans
{
    protected $order;
    protected $items;
    protected $customer;

    public function __construct($order, $items, $customer)
    {
        parent::__construct();

        $this->order = $order;
        $this->items = $items;
        $this->customer = $customer;
    }

    public function create()
    {
        $item_details = [];
        foreach($this->items as $item){
            $item_details[] = [
                'id' => $item->id,
                'price' => $item->price,
                'quantity' => $item->quantity,
                'name' => $item->name,
            ];
        }
        $params = [
            'transaction_details' => [
                'order_id' => $this->order->order_id,
                'gross_amount' => $this->order->gross_amount,
            ],
            'item_details' => $item_details,
            'customer_details' => [
                'first_name' => $this->customer->first_name,
                'email' => $this->customer->email,
                'phone' => $this->customer->phone,
            ]
        ];

        $transaction = Snap::createTransaction($params);

        return $transaction;
    }
}