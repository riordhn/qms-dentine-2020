<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DENTINE 2025 FKG UNAIR</title>

    <!-- Google Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"> -->

    <!-- Bootstrap Core Css -->
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <!-- <style>
        @page {
            margin:0px;
        }
    </style> -->
</head>
<body style="background:white;">
    <img src="{{ $header_footer_img }}" style="display: block; position: absolute">
    <div class="container">
        <div class="row" style="margin-top: 120px;">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <img class="img" style="height: 128px;" src="{{ $logo_img }}"/>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right">
                <h2 style="margin: 0;">REGIST NO: {{$no_invoice}}</h2>
                @if($voucher->is_paid == 1)
                <h3 style="color: red">STATUS: UNPAID</h3>
                @elseif($voucher->is_paid == 10)
                <h3 style="color: green">STATUS: PAID</h3>
                @endif
                @if(!empty($event_fee->area_id))
                <h3>{{$event_fee->area_id}}</h3>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p>
                    <strong>Faculty of Dental Medicine:</strong><br>
                    Airlangga University<br>
                    Jl. Dr. Ir. H. Soekarno, Mulyorejo,<br>
                    Surabaya 60115
                </p>
            </div>
            @if(!empty($voucher->account_id))
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
                <p>
                    <strong>Participant:</strong><br>
                    {{$voucher->name}}<br>
                    {{$voucher->organization}}<br>
                    {{$voucher->phone}}<br>
                </p>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p>
                    <strong>PAYMENT:</strong><br>
                    @if(in_array($voucher->event_id, [1,4,5,6,7,8]))
                    Bank Mandiri 1420020692975 a.n. MARETHA PUTRI SURYA MAHENDRA
                    @else
                    Bank Mandiri 1420020913363 a.n ARDELIA SABRINA RAFTIANI
                    @endif
                </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
                <p>
                    <strong>Invoice Date:</strong><br>
                    {{date_format(date_create($voucher->created_at), 'd M Y')}}<br><br>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Summary</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <td><strong>Detail</strong></td>
                                        <td class="text-center"><strong>Price</strong></td>
                                        <td class="text-center"><strong>Quantity</strong></td>
                                        <td class="text-right"><strong>Subtotal</strong></td>
                                    </tr>
                                </thead>
                                @php
                                    $registration_fee = $event_fee->registration_fee;
                                    $discount_status = $voucher->addon_status;
                                    $discount = 0;

                                    if($voucher->addon_status == 1 && $voucher->event_id == 1) $discount -= 15000;
                                    if($voucher->addon_status == 1 && $voucher->event_id == 2) $discount -= 5000;
                                    if($voucher->addon_status == 1 && $voucher->event_id == 3) $discount -= 5000;
                                    if($voucher->addon_status == 1 && $voucher->event_id == 5) $discount -= 5000;
                                @endphp
                                <tbody>
                                    <tr>
                                        <td>Registration fee {{$event_fee->title}} {{(!empty($event_fee->area_id)? '('.$event_fee->area_id.')' : '')}}</td>
                                        <td class="text-center">{{number_format($registration_fee)}}</td>
                                        <td class="text-center">1</td>
                                        <td class="text-right">{{number_format($registration_fee)}}</td>
                                    </tr>
                                    @if($voucher->country_id == 77)
                                    <tr>
                                        <td>Discount</td>
                                        <td class="text-center">{{number_format($discount)}}</td>
                                        <td class="text-center">1</td>
                                        <td class="text-right">{{number_format($discount)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Unique Number</td>
                                        <td class="text-center">{{number_format($voucher->event_id)}}</td>
                                        <td class="text-center">1</td>
                                        <td class="text-right">{{number_format($voucher->event_id)}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan=3>Total</td>
                                        @if($voucher->country_id == 77)
                                        <td class="text-right">IDR {{number_format($registration_fee + $discount + $voucher->event_id)}}</td>
                                        @else
                                        <td class="text-right">USD ${{number_format($registration_fee)}}</td>
                                        @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <blockquote>
                <footer>
                    Note
                    <ol class="font-italic">
                        <li>Preliminary round location will be informed via gmail</li>
                        <li>Contact Person +6285156533082 (admin dentine) </li>
                    </ol>
                    </footer>
                </blockquote>
            </div>
        </div>
    </div>
</body>

<script>
    window.print();
</script>
</html>
