<strong>REGISTRATION WAS SUCCESSFUL</strong><br>
This is an automated message. Please do not reply<br>
<br>
Hi, {{ $account->name }}<br>
Thank you for register to website Dentine FKG Unair 2025.<br>
You are registered in Event {{ $event_name }} as
<strong>Email/Username:</strong> {{ $account->email }}<br>
<strong>Contact/Phone:</strong> {{ $account->phone }}<br>
<strong>Name:</strong> {{ $account->name }}<br>
<strong>From:</strong> {{ $account->organization }}<br>
<br>
You can Login with email dan password that you create before, with this link below<br>
<a href="https://peserta.dentinefkgunair.com/">THIS LINK</a><br>
<br>
REGARDS<br>
DENTINE FKG UNAIR 2025 OFFICIAL