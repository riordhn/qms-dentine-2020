@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('participant/partials/topbar')
    @include('participant/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                @if(!empty($participant) && $participant->is_pass == 1)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-pink">
                            Congratulations on entering the semifinals! For certificates you will be given a hard copy during the semifinals in Surabaya
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h4><b>The following are terms and conditions, as well as important information regarding the {{$event->name}}</b></h4>
                        </div>
                        <div class="body">
                        @php
                            $filename = str_replace(' ', '-', strtolower($event->name));
                        @endphp
                            @include('participant/partials/event-info/'.$filename)
                            <!-- <span class="col-pink">Make sure that you fill in correctly</span>
                            <p>Kind regards</p>
                            <p>DENTINE 2025 Admin</p>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-univ.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-faculty.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo/main-logo.png')}}"/> -->
                            @if(empty($participant))
                            <form class="form-validation" method="POST" action="{{url('event/'.$event->event_id.'/participate')}}">
                            {{csrf_field()}}
                            <p class="col-pink">Click the button below to fill in the team data</p>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">COMPLETE DATA CONTENT OF MEMBERS</button>
                                </div>
                            </div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
                @if(!empty($participant))
                @php
                    $participant_members = \App\Models\ParticipantMember::where('participant_id', $participant->participant_id)->orderBy('participant_member_id', 'asc')->get();
                    $voucher = \App\Models\Voucher::where('account_id', $account->account_id)->first();
                @endphp
                @if($event->event_id == 1)
                    @php
                        $time_status = \App\Libraries\TestTime::checkTime($account);
                    @endphp
                @endif
                @if($participant_members->first())
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-teal">
                            <h4><b>Team Member Data</b></h4>
                        </div>
                        <div class="body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Participate As</th>
                                        <th>Age</th>
                                        <th>School</th>
                                        <th>Student</th>
                                        <th>Phone</th>
                                        <th>LINE ID</th>
                                        <th>Student Card</th>
                                        <!-- <th>Edit</th> -->
                                        <th>Certificate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($participant_members as $i => $participant_member)
                                    <tr>
                                        <th scope="row">{{$i+1}}</th>
                                        <td>{{$participant_member->name}}</td>
                                        <td>{{$participant_member->participate_as}}</td>
                                        <td>{{\Carbon\Carbon::make($participant_member->birthday)->age}} years old</td>
                                        <td>{{$participant_member->organization}}</td>
                                        <td>{{$participant_member->generation}}</td>
                                        <td>{{$participant_member->phone}}</td>
                                        <td>{{$participant_member->social_media}}</td>
                                        <td><a href="{{url('uploads/'.$participant_member->photo)}}" target="_blank">Show</a></td>
                                        <!-- <td><a href="{{url('member/edit/'.$participant_member->participant_member_id)}}">Edit</a></td> -->
                                        <td>
                                            <a href="{{url('certificate/'.$participant_member->participant_member_id)}}" target="_blank">Download for Participant</a>
                                            @if(Auth::user()->is_winner == 1)
                                            <br/>
                                            <a href="{{url('certificate/next/'.$participant_member->participant_member_id)}}" target="_blank">Download for Pass the Exam</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <a href="{{url('event/'.$event->event_id.'/card')}}" target="_blank" class="btn bg-pink btn-block btn-lg waves-effect">Download Bukti Tanda Pendaftaran</a>
                                </div>
                            </div> -->
                            @if($participant_members->count() < $event->person_in_event)
                            <!-- <a class="btn bg-teal waves-effect" href="{{url('event/'.$event->event_id.'/1')}}">
                                <span>Add Other Member</span>
                            </a> -->
                            @endif
                        </div>
                    </div>
                </div>
                    @if($event->event_id == 2)
                        @if($participant->is_done == 1)
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h4><b>Upload your work here, participant of {{$event->name}}</b></h4>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <form action="{{url('event/essay/upload')}}" method="post" enctype="multipart/form-data">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <p>You can upload your work by clicking the button below and selecting the file you uploaded</p>
                                                <p class="col-red">It's important to remember, you can only upload one file, make sure the file you upload is completely ready</p>
                                                {{csrf_field()}}
                                                <input name="participant" type="hidden" value="{{$participant->participant_id}}" />
                                                <h3>Click the button below.</h3>
                                                <em><strong>Make sure the final files before uploaded :)</strong></em>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <label>Results of your work</label>
                                                <div class="fallback">
                                                    <input name="file_1" type="file" multiple />
                                                </div>
                                            </div>
                                             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <p>You will also need to upload an originality statement file.</p>
                                                <p class="col-red">Templates can be downloaded below</p>
                                                @if($event->event_id == 2)
                                                <a href="https://docs.google.com/document/d/1oKCzycH17X9ih-g3sSbzDBq6dVjsFFvROkuiI_jBUp0/edit?usp=drivesdk">Download here</a>
                                                @elseif($event->event_id == 3)
                                                <a href="https://docs.google.com/document/d/1l7EyejC2cpCVZMUNoEPUuk3ST6IgCc-RyVujUgvLQpw/edit?usp=drivesdk">Download here</a>
                                                @endif
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <label>Your Statement File*</label>
                                                <div class="fallback">
                                                    <input name="file_2" type="file" multiple required="" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        @php
                            $file_upload = \App\Models\FileUpload::where('participant_id', $participant->participant_id)->first();
                        @endphp
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h4><b>Thank you for uploading your best results</b></h4>
                                    @if($file_upload)
                                    <a href="{{url($file_upload->fileurl_1)}}" target="_blank"><b>Check your uploaded file</b></a>
                                    <br/>
                                    
                                    <!-- <h6>*If you have a revision about the uploading file, please click button below</h6>
                                    <a href="{{url('event/file-upload/delete/'.$file_upload->file_upload_id)}}" class="btn bg-pink btn-block btn-lg waves-effect">Re-upload My File</a> -->
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                    @if($event->event_id == 3)
                        @if($participant->is_done == 1)
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h4><b>Upload your work here, participant of {{$event->name}}</b></h4>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <form action="{{url('event/essay/upload')}}" method="post" enctype="multipart/form-data">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <p>You can upload your work by clicking the button below and selecting the file you uploaded</p>
                                                <p class="col-red">It's important to remember, you can only upload one file, make sure the file you upload is completely ready</p>
                                                {{csrf_field()}}
                                                <input name="participant" type="hidden" value="{{$participant->participant_id}}" />
                                                <h3>Click the button below.</h3>
                                                <em><strong>Make sure the final files before uploaded :)</strong></em>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <label>Results of your work</label>
                                                <div class="fallback">
                                                    <input name="file_1" type="file" multiple />
                                                </div>
                                            </div>
                                             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <p>You will also need to upload an originality statement file.</p>
                                                <p class="col-red">Templates can be downloaded below</p>
                                                @if($event->event_id == 2)
                                                <a href="https://docs.google.com/document/d/1oKCzycH17X9ih-g3sSbzDBq6dVjsFFvROkuiI_jBUp0/edit?usp=drivesdk">Download here</a>
                                                @elseif($event->event_id == 3)
                                                <a href="https://docs.google.com/document/d/1l7EyejC2cpCVZMUNoEPUuk3ST6IgCc-RyVujUgvLQpw/edit?usp=drivesdk">Download here</a>
                                                @endif
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <label>Your Statement File*</label>
                                                <div class="fallback">
                                                    <input name="file_2" type="file" multiple required="" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        @php
                            $file_upload = \App\Models\FileUpload::where('participant_id', $participant->participant_id)->first();
                        @endphp
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h4><b>Thank you for uploading your best results</b></h4>
                                    @if($file_upload)
                                    <a href="{{url($file_upload->fileurl_1)}}" target="_blank"><b>Check your uploaded file</b></a>
                                    <br/>
                                    
                                    <!-- <h6>*If you have a revision about the uploading file, please click button below</h6>
                                    <a href="{{url('event/file-upload/delete/'.$file_upload->file_upload_id)}}" class="btn bg-pink btn-block btn-lg waves-effect">Re-upload My File</a> -->
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif

                    @if($event->event_id == 5)
                        @if($participant->is_done == 1)
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h4><b>Upload your work here, participant of {{$event->name}}</b></h4>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <form action="{{url('event/essay/upload')}}" method="post" enctype="multipart/form-data">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <p>You can upload your work by clicking the button below and selecting the file you uploaded</p>
                                                <p class="col-red">It's important to remember, you can only upload one file, make sure the file you upload is completely ready</p>
                                                {{csrf_field()}}
                                                <input name="participant" type="hidden" value="{{$participant->participant_id}}" />
                                                <h3>Click the button below.</h3>
                                                <em><strong>Make sure the final files before uploaded :)</strong></em>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <label>Results of your work</label>
                                                <div class="fallback">
                                                    <input name="file_1" type="file" multiple />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <p>You will also need to upload an originality statement file.</p>
                                                <p class="col-red">Templates can be downloaded below</p>
                                                <a href="https://docs.google.com/document/d/10cZqDJ8Ykyn2T_5uLlQB-Xd3itgJy-Dws4HxYvl-1ds/edit?usp=drivesdk">Download here</a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                                <label>Your Statement File*</label>
                                                <div class="fallback">
                                                    <input name="file_2" type="file" multiple required="" />
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">Submit</button>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        @php
                            $file_upload = \App\Models\FileUpload::where('participant_id', $participant->participant_id)->first();
                        @endphp
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h4><b>Thank you for uploading your best results</b></h4>
                                    @if($file_upload)
                                    <a href="{{url($file_upload->fileurl_1)}}" target="_blank"><b>Check your uploaded file</b></a>
                                    <br/>
                                    
                                    <!-- <h6>*If you have a revision about the uploading file, please click button below</h6>
                                    <a href="{{url('event/file-upload/delete/'.$file_upload->file_upload_id)}}" class="btn bg-pink btn-block btn-lg waves-effect">Re-upload My File</a> -->
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                @else
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-teal">
                            <h4><b>Fill Team Member Data</b></h4>
                        </div>
                        <div class="body">
                            <a class="btn bg-teal waves-effect" href="{{url('event/'.$event->event_id.'/1')}}">
                                <span>Add Team Member</span>
                            </a>
                            <!-- <p>How many is your team member?</p>
                            @for($i=1; $i<=$event->person_in_event; $i++)
                            <a class="btn bg-teal waves-effect" href="{{url('event/'.$event->event_id.'/'.$i)}}">
                                <span>{{$i}}</span>
                            </a>
                            @endfor -->
                            <!-- <small class="col-pink">Please choose carefully.</small> -->
                        </div>
                    </div>
                </div>
                @endif
                @endif
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        time: false
    });
</script>
@endsection
