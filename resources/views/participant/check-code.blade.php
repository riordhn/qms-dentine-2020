@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="login-page" style="background-color: #ffe0a7;">
    <div class="login-box">
        <div class="logo align-center">
            <img class="m-b-15" src="{{asset('media/logo/main-logo.png')}}" width="192"/>
            <small class="col-black">Ajang kompetisi pelajar SMA/Sederajat yang diadakan oleh Badan Eksekutif Mahasiswa Fakultas Kedokteran Gigi Universitas Airlangga yang akan dilaksanakan secara nasional.</small>
        </div>
        <div class="card">
            <div class="body">
                <form class="form-validation" method="POST" action="{{url('register')}}">
                    {{csrf_field()}}
                    <div class="msg">Masukkan kode vouchermu di sini</div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">extension</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="code" required="" minlength="4" maxlength="20" aria-required="true" aria-invalid="true">
                                    <label class="form-label">Kode voucher</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">CHECK CODE</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-9">
                            Sudah punya akun?
                        </div>
                        <div class="col-xs-3 align-right">
                            <a href="{{url('/')}}">Login</a>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-9">
                            Atau belum punya kode untuk daftar?
                        </div>
                        <div class="col-xs-3 align-right">
                            <a href="https://adsmfkgunair.com/#contact">Hubungi kami</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
