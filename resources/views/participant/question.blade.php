@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('participant/partials/topbar')
    @include('participant/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DENTINE ONLINE</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            Member List
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <ol>
                                    @foreach ($participant->member as $participant_member)
                                        @if($loop->first)
                                        <img src="{{url('uploads/'.$participant_member->profile)}}" style="width: 150px"/>
                                        @endif
                                        <li>{{$participant_member->name}} ({{$participant_member->participate_as}})</li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-blue">
                            Rules
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    Notes:
                                    Waktu pengerjaan 60 menit,
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                (Question Number {{$test_answer->number}}) Asnwer the question below
                            </h2>
                        </div>
                        <div class="body">
                            <div style="background:white; border: 1px solid #ccc; border-radius: 4px; display: block; padding: 9.5px; margin-bottom: 10px; overflow: auto;">
                                {!!$question->content!!}
                            </div>
                            <div class="row clearfix">
                                <form id="question-form" class="form-validation" method="POST" action="{{url('test/answer')}}">
                                    <input type="hidden" name="question" value="{{$question->question_id}}">
                                    {{csrf_field()}}
                                    @if($question->question_type == 1)
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="demo-radio-button">
                                        @foreach($question_options as $no_option => $question_option)
                                        @if(empty($test_answer->question_option_id))
                                            <input name="question_option" type="radio" id="radio_{{$no_option}}" value="{{$question_option->question_option_id}}">
                                            <label for="radio_{{$no_option}}"><pre>{!!$question_option->content!!}</pre></label>
                                        @else
                                            @if($test_answer->question_option_id == $question_option->question_option_id)
                                            <input name="question_option" type="radio" checked="" id="radio_{{$no_option}}" value="{{$question_option->question_option_id}}">
                                            <label for="radio_{{$no_option}}"><pre>{!!$question_option->content!!}</pre></label>
                                            @else
                                            <input name="question_option" type="radio" id="radio_{{$no_option}}" value="{{$question_option->question_option_id}}">
                                            <label for="radio_{{$no_option}}"><pre>{!!$question_option->content!!}</pre></label>
                                            @endif
                                        @endif
                                            <hr style="border-top: 0;">
                                        @endforeach
                                        </div>
                                    </div>
                                    @else
                                    <br>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Fill in the answers in the column below</label>
                                        <textarea style="background:white; border: 1px solid #ccc; border-radius: 4px; display: block; padding: 9.5px; margin-bottom: 10px; width: 100%;" name="answer">{!!$test_answer->content_text!!}</textarea>
                                    </div>
                                    @endif
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button class="btn btn-block bg-green waves-effect" type="submit">Save & next</button>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button class="btn btn-block bg-pink waves-effect" type="button" onclick="deleteAnswerAction()">Leave the answer blank</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-brown">
                            <h2>
                            <i class="material-icons">access_alarm</i>
                                <span id="timeleft">Time Remaining: -</span>
                            </h2>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                List of categories
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    @foreach($question_categories as $question_category)
                                    <button type="button" class="btn btn-block {{$active_category->question_category_id == $question_category->question_category_id? 'bg-amber' : 'bg-grey'}} waves-effect waves-float">
                                        {{ $question_category->name }} ({{ $question_category->minutes_total }} minutes)
                                    </button>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                List of questions
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    @foreach($test_answers as $no_test_answer => $other_test)
                                    @if($test_answer->number == ($other_test->number))
                                    <a type="button" href="{{url('test/question/'.$other_test->question_id)}}" class="btn bg-amber btn-circle waves-effect waves-circle waves-float">
                                    @else
                                        @if($other_test->question->question_type == 1)
                                            @if(empty($other_test->question_option_id))
                                            <a type="button" href="{{url('test/question/'.$other_test->question_id)}}" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">
                                            @else
                                            <a type="button" href="{{url('test/question/'.$other_test->question_id)}}" class="btn bg-green btn-circle waves-effect waves-circle waves-float">
                                            @endif
                                        @else
                                            @if(empty($other_test->content_text))
                                            <a type="button" href="{{url('test/question/'.$other_test->question_id)}}" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">
                                            @else
                                            <a type="button" href="{{url('test/question/'.$other_test->question_id)}}" class="btn bg-green btn-circle waves-effect waves-circle waves-float">
                                            @endif
                                        @endif
                                    @endif
                                        {{$no_test_answer+1}}
                                        <!-- {{$other_test->number}} -->
                                    </a>
                                    @endforeach
                                    
                                    @if($question->question_category_id == 5)
                                    <button type="button" href="#" onclick="endAction()" class="btn btn-block bg-cyan waves-effect" style="margin-top:2rem;">
                                        My work is done, close the page
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    var distance = {{$timeup_left}};
    var x = setInterval(function () {
        var hours = Math.floor((distance % (1 * 60 * 60 * 24)) / (1 * 60 * 60));
        var minutes = Math.floor((distance % (1 * 60 * 60)) / (1 * 60));
        var seconds = Math.floor((distance % (1 * 60)) / 1);

        document.getElementById("timeleft").innerHTML = "Time Remaining: " + hours + "h " +
            minutes + "m " + seconds + "s ";

        if (distance <= 0) {
            clearInterval(x);
            location.reload();
        }else{
            distance--;
        }
    }, 1000);

    function endAction(){
        vex.dialog.confirm({
            message: "Are you sure you've finished your work?",
            callback: function (value) {
                if(value){
                    var url = '{{url('test/end')}}';
                    window.location = url; 
                }
            }
        })
    }

    function deleteAnswerAction(){
        $('input[name=question_option]').prop('checked', false);
        $('#question-form').submit();
    }

    function actionFlags(){
        if (localStorage.getItem('blok_user_next')) {
            // alert('Kamu melakukan pelanggaran dengan membuka tab lain di browser. Hati-hati agar akun tidak terlock');
            // window.location.href = `{{url('logout')}}`;
            // alert('Sorry your account is violation more than 3x opening the tab');
            actionPostFlags();
        } else {
            if (localStorage.getItem('count_blok_user_next')) {
                localStorage.setItem('count_blok_user_next', parseInt(localStorage.getItem('count_blok_user_next')) + 1);
            } else {
                localStorage.setItem('count_blok_user_next', 1);
            }

            actionPostFlags();
            let sisa = 3 - localStorage.getItem('count_blok_user_next');
            alert(
                `You commit a violation by opening another tab in the browser, ${sisa} more chance for the account`
            );

            if (localStorage.getItem('count_blok_user_next') == 3) {
                alert('Sorry your account is violation more than 3x opening the tab');
                localStorage.setItem('blok_user_next', true);
            }
        }
    }

    function actionPostFlags(){
        $.ajax({
            type: "POST",
            url: '{{url('test/flags')}}',
            success: function (data) {
                console.log('...');
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
</script>

<script>
    document.addEventListener("visibilitychange", () => {
        if (document.visibilityState === 'visible') {
            actionFlags();
        }
    });
</script>
@endsection