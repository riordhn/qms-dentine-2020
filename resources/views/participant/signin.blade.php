@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="login-page" style="background-image: url('media/bg-login-02.png'); background-size: cover;">
    <div class="login-box">
        <div class="card" style="border-radius: 25px; height:90px;margin-bottom: 16px;background:#F7DB70;padding-top:6px;">
            <center>
                <img class="img" style="height: 75px;" src="{{asset('media/logo/logo-logo-2.png')}}"/>
            </center>
        </div>
        <form class="form-validation" method="POST" action="{{url('signin')}}">
            <div class="card" style="border-top-left-radius: 25px;border-top-right-radius: 25px; margin-bottom: 16px;background:#ffffff80;">
                <div class="body">
                    <div class="logo align-center">
                        <img class="m-b-15" src="{{asset('media/logo/main-logo.png')}}" width="175"/>
                        <!-- <small class="col-black">Ajang kompetisi pelajar SMA/Sederajat yang diadakan oleh Badan Eksekutif Mahasiswa Fakultas Kedokteran Gigi Universitas Airlangga yang akan dilaksanakan secara nasional.</small> -->
                    </div>
                    {{csrf_field()}}
                    <div class="msg"><b>Sign in to access your account</b></div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" style="background: transparent;" name="email" required="" aria-required="true" aria-invalid="true">
                                    <label class="form-label" style="color:black;">Email</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" class="form-control" style="background: transparent;" name="password" required="" aria-required="true">
                                    <label class="form-label" style="color:black;">Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="{{url('register')}}">Register</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button style="border-bottom-left-radius: 25px;border-bottom-right-radius: 25px;background-color: #F7DB70;height:50px;" class="btn btn-block waves-effect" type="submit">LOG IN</button>
                </div>
            </div>
        </form>
    </div>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
