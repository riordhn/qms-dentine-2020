<center>
    <img style="max-width: 225px;" src="{{url('media/logo/main-logo.png')}}">
</center>
<br>
<!-- <p>KETENTUAN LOMBA</p>
<ul>
    <li>Ketentuan Umum
        <ol>
            <li>DESCOM 2021 merupakan lomba esai yang bersifat terbuka, tidak mewakili rayon-rayon tertentu dan tidak ada batasan mengenai jumlah tim yang ikut dalam satu sekolah.</li>
            <li>Peserta merupakan siswa SMA/SMK/MA atau sederajat di Indonesia.</li>
            <li>Satu tim terdiri dari 1 – 3 siswa/i dan yang berasal dari sekolah yang sama.</li>
            <li>Tiap tim mengirimkan satu naskah esai sesuai dengan ketentuan yang telah diatur dalam petunjuk teknis.</li>
            <li>Satu tim diperbolehkan mengirim lebih dari satu naskah. Untuk mengirimkan naskah lain, peserta diharuskan mendaftar dengan nomor pendaftaran yang berbeda.</li>
            <li>Peserta wajib mengikuti ketentuan-ketentuan yang telah diatur oleh panitia.</li>
        </ol>
    </li>
    <li>Syarat Pengumpulan Naskah
        <ol>
            <li>Naskah belum pernah diterbitkan di media apapun dan tidak sedang diikutkan dalam kegiatan serupa.</li>
            <li>Esai harus asli, bukan terjemahan atau saduran.</li>
            <li>Keputusan dewan juri bersifat mutlak, mengikat, dan tidak bisa diganggu-gugat.</li>
            <li>Naskah yang masuk menjadi hak panitia dan tidak dikembalikan.</li>
            <li>Panitia memiliki hak dan wewenang untuk mempublikasikan naskah esai dengan tetap mencantumkan nama penulis.</li>
            <li>Judul esai bebas sesuai dengan topik yang dipilih, ekspresif, dan menggambarkan isi dari esai tersebut dengan tetap menggunakan bahasa Indonesia yang baik dan benar.</li>
            <li>Esai tergolong jenis ilmiah populer yang bersifat asli (bukan jiplakan) sesuai dengan topik dan bisa juga disertai gagasan atau ide kreatif tentang topik yang dipilih penulis.</li>
        </ol>
    </li>
    <li>Ketentuan Format Esai
        <ol>
            <li>Judul diketik dengan huruf kapital.</li>
            <li>Di bawah judul disertakan logo, nama penulis, dan instansi/sekolah.</li>
            <li>Esai diketik pada kertas berukuran A4 dengan tulisan Times New Roman 12 pt, spasi single.</li>
            <li>Batas pengetikan : kiri 4 cm, kanan 3 cm, atas 4 cm, dan bawah 3 cm.</li>
            <li>Naskah esai diketik minimal 3 lembar dan maksimal 10 lembar dihitung mulai dari naskah esai (cover, daftar isi, dan daftar riwayat hidup tidak dihitung).</li>
            <li>Melampirkan daftar riwayat hidup.</li>
            <li>Esai di upload melalui website http://dentinefkgunair.com , dalam bentuk file PDF (sudah dijadikan satu dengan daftar riwayat hidup).</li>
            <li>Esai yang sudah di upload tidak dapat diubah atau direvisi kembali.</li>
            <li>Kriteria Penilaian
                <ol>
                    <li>Orisinalitas ide</li>
                    <li>Kesesuaian dengan tema</li>
                    <li>Inovasi dan kreativitas</li>
                    <li>Sistematika penulisan</li>
                </ol>
            </li>
            <li>Ketajaman analisa</li>
            <li>Kemungkinan solusi yang ditawarkan</li>
            <li>Gaya penulisan (tata bahasa dan penggunaan EYD)</li>
        </ol>
    </li>
</ul>
<p>JUKLAK</p>
<ul>
    <li>JUKLAK untuk Olimpiade DESCOM dapat didownload pada di bawah ini
        <ol>
            <li><a target="_blank" href="{{url('downloads/JUKLAK DESCOM 2021.pdf')}}">JUKLAK DESCOM 2021</a></li>
        </ol>
    </li>
</ul> -->
