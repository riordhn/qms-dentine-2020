<center>
    <img style="max-width: 225px;" src="{{url('media/logo/main-logo.png')}}">
</center>
<br>
<!-- <ul>
    <li>Ketentuan Peserta:
        <ol>
            <li>Peserta adalah siswa aktif di tingkat SMA/SMK sederajat se-Indonesia.</li>
            <li>Peserta terdiri atas 1 orang.</li>
            <li>Peserta membuat akun untuk melakukan pendaftaran online melalui situs resmi DENTINE 2025 : http://dentinefkgunair.com, melengkapi persyaratan, dan melakukan konfirmasi (online).</li>
            <li>Peserta mengisi formulir pendaftaran yang dapat diunduh melalui website http://dentinefkgunair.com.</li>
            <li>Peserta melakukan pembayaran biaya pendaftaran sebesar :
                <br>• Gelombang I
                    <br>Tanggal : 24 Oktober – 5 Desember 2020
                    <br>Biaya  : Rp 50.000,-/karya.
                <br>• Gelombang II
                    <br>Tanggal : 5 Desember 2020 – 27 Januari 2021
                    <br>Biaya      : Rp 60.000,-/karya.
                </li>
            <li>Setiap sekolah boleh mengirimkan lebih dari 1 peserta.</li>
            <li>Setiap siswa boleh mengirimkan lebih dari 1 karya, dan perhitungan pendaftaran per karya.</li>
            <li>Setiap peserta akan mendapatkan e-sertifikat sebagai peserta.</li>
            <li>Pengumpulan karya maksimal pada tanggal 31 Januari 2021 pukul 23.59 WIB.</li>
            <li>Karya yang telah dikumpulkan tidak dapat direvisi atau diganti.</li>
            <li>Keputusan juri adalah mutlak dan tidak dapat diganggu gugat.</li>
            <li>Peserta bersedia mengikuti rangkaian acara dan prosedur kompetisi.</li>
        </ol>
    </li>
    <li>Persyaratan Karya:
        <ol>
            <li>Jenis poster yang diperlombakan adalah Poster Infografik.</li>
            <li>Karya desain tidak mengandung unsur pornografi, SARA, dan hal-hal yang bertentangan dengan norma yang berlaku.</li>
            <li>Poster diberikan judul sesuai dengan tema.</li>
            <li>Tema poster infografik babak penyisihan “The Variety of Teeth and Their Functions”.</li>
            <li>Peserta diharuskan mengirim 1 karya desain yang berukuran maksimal 10 MB dalam format JPG/JPEG/PNG minimal 300 dpi dengan ukuran A3 menggunakan aplikasi umum (Corel Draw, Adobe Photoshop, atau aplikasi lainnya) ke website http://dentinefkgunair.com disertai judul dan deskripsi poster.</li>
            <li>Peserta mengirimkan karya desain dengan format nama file poster "No. Peserta_POSTER_Nama Lengkap_Asal Sekolah"</li>
            <li>Wajib menyertakan tagline “D-START 2021 FKG UNIVERSITAS AIRLANGGA” dengan penempatan pada kanan bawah karya poster dan watermark NAMA PESERTA di kiri bawah poster.</li>
            <li>Desain yang dibuat adalah hasil karya orisinil, tidak mengambil dari karya orang lain baik sebagian maupun keseluruhan dan belum pernah dipublikasikan sebelumnya dan tidak sedang diikutkan dalam perlombaan apapun selain D-START 2021.</li>
            <li>Apabila di kemudian hari terbukti karya tersebut tidak orisinil atau karya orang lain, panitia berhak mencabut hak peserta tersebut dari keikutsertaannya dalam lomba ini, termasuk menganulir kemenangannya (sertifikat dan uang).</li>
            <li>Tidak diperkenankan untuk mempromosikan produk atau label tertentu di dalam karya yang diproduksi.</li>
            <li>Pemilihan warna dan gaya desain tidak dibatasi.</li>
            <li>Ilustrasi harus komunikatif dan edukatif dalam menyampaikan pesan.</li>
            <li>Karya desain yang telah diterima akan diseleksi oleh dewan juri dan akan ditentukan 5 grand finalis yang berhak mengikuti babak selanjutnya.</li>
            <li>Karya finalis akan menjadi hak milik Panitia Pelaksana dan dapat diperbanyak serta dipublikasikan untuk kepentingan edukasi tanpa perlu mendapat izin dari pembuat karya, serta tanpa kompensasi materi/uang, tetapi hak cipta tetap milik pembuat karya.</li>
            <li>Pada babak final, tema dan ketentuan desain akan ditentukan kemudian oleh panitia pada tanggal 7 Februari 2021.</li>
            <li>Setiap peserta yang lolos babak final diwajibkan mengirimkan softcopy desain poster dengan tema baru yang akan ditentukan panitia dan dikumpulkan maksimal tanggal 13 Februari 2021 pukul 23.59 WIB ke website http://dentinefkgunair.com</li>
            <li>Karya yang telah dikumpulkan tidak dapat direvisi atau diganti.</li>
            <li>Hasil karya 5 grand finalis ART babak final akan diupload di media sosial DENTINE 2025 (Instagram: @dentinefkgunair) dan juga dipamerkan bersama dengan penyisihan dan semifinal DENTINE 2025 dan dilakukan voting penentuan juara favorit.</li>
            <li>Aspek yang dinilai:
                <ul>
                    <li>Kesesuaian dengan tema</li>
                    <li>Inovasi dan kreativitas</li>
                    <li>Orisinalitas karya</li>
                    <li>Estetika desain</li>
                    <li>Lingkup kedalaman eksplorasi tema dan komunikatif dalam penyampaian pesan</li>
                    <li>Jumlah “likes” yang dihimpun pada instagram @dentinefkgunair sebagai juara favorit</li>
                </ul>
            </li>
            <li>Bagi peserta yang didapati melakukan kecurangan dalam bentuk apapun (menggunakan aplikasi likers, dll) akan didiskualifikasi dari perlombaan.</li>
        </ol>
    </li>
    <li>Jadwal penerimaan dan penjurian (Timeline):
        <ul>
            <li>Pendaftaran dimulai tanggal 24 Oktober 2020 - tanggal yang ditentukan pukul 23.59 WIB melalui pendaftaran online.</li>
            <li>Pengumpulan karya dimulai tanggal 24 Oktober 2020 - 31 Januari 2021 pukul 23.59 WIB melalui website dentine http://dentinefkgunair.com. Jika lewat dari tanggal tersebut, peserta dianggap mengundurkan diri dan uang pendaftaran tidak dapat dikembalikan.</li>
            <li>Pengumuman seleksi penyisihan akan diumumkan pada tanggal 7 Februari 2021 di website http://dentinefkgunair.com dan di media sosial DENTINE 2025 (Instagram: @dentinefkgunair)</li>
            <li>Pengumpulan karya untuk babak final dimulai tanggal 7 Februari 2021 - 13 Februari 2021 pukul 23.59 WIB melalui website dentine http://dentinefkgunair.com. Jika lewat dari tanggal tersebut, peserta dianggap mengundurkan diri dan uang pendaftaran tidak dapat dikembalikan.</li>
            <li>Karya yang telah dikumpulkan tidak dapat direvisi atau diganti.</li>
            <li>Untuk penentuan juara 1 dan juara 2 mutlak melalui keputusan dewan juri.</li>
            <li>Untuk penentuan juara favorit menggunakan mekanisme voting melalui instagram @dentinefkgunair dimulai tanggal 14 Februari 2021 - 19 Februari 2021 pukul 23.59</li>
            <li>Pemenang lomba akan diumumkan pada 21 Februari 2021 dan akan ditentukan juara 1, juara 2, dan juara favorit.</li>
        </ul>
    </li>
    <li>JUKLAK<br>
        JUKLAK untuk Olimpiade DSTART dapat didownload pada di bawah ini
        <ol>
            <li><a target="_blank" href="{{url('downloads/JUKLAK DSTART 2021.pdf')}}">JUKLAK DSTART 2021</a></li>
        </ol>
    </li>
</ul> -->
