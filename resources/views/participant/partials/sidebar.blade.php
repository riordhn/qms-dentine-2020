@php
    $account = Auth::user();
    $events = \App\Models\Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
    $voucher = \App\Models\Voucher::find($account->voucher_id);
@endphp
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
            <img src="https://ui-avatars.com/api/?background=001935&color=fff&name={{$account->name}}" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:black;">{{$account->name}}</div>
                <div class="email" style="color:black;">{{$account->email}}</div>
                <!-- <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{url('profile')}}"><i class="material-icons">person</i>Profile</a></li>
                        <li><a href="{{url('change-password')}}"><i class="material-icons">lock</i>Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{url('logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div> -->
            </div>
        </div>
        <!-- #User Info -->

        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="{{url('welcome')}}">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                @if($voucher->is_paid == 10)
                <!-- <li>
                    <a href="javascript:void(0);" class="menu-toggle toggled">
                        <i class="material-icons">format_list_numbered</i>
                        <span>Main Events</span>
                    </a>
                    <ul class="ml-menu"> -->
                        @foreach($events as $event)
                            @if(!in_array($event->event_id, [6,7,8]))
                            <li><a href="{{url('event/'.$event->event_id)}}"><i class="material-icons">account_circle</i><span>Event {{$event->name}}</span></a></li>
                            @endif
                            @if($event->event_id == 1)
                                @php
                                    $check_time = \App\Libraries\TestTime::checkTime($account);
                                    $participant = \App\Models\Participant::where('account_id', $account->account_id)->first();
                                @endphp
                                @if($check_time->status && $participant)
                                <li style="background-color: #4CAF50 !important; color: white;"><a href="{{url('test/rule')}}"><i class="material-icons">check</i><span><strong>Lets do the Exam</strong></span></a></li>
                                @endif
                                @if($check_time->code == 300)
                                <li style="background-color: #4CAF50 !important; color: white;"><a href="{{url('test/result')}}"><i class="material-icons">check</i><span><strong>Exam Summary</strong></span></a></li>
                                @endif
                            <!-- <li><a href="https://drive.google.com/drive/folders/1Tbaa-QzCEYF52OryNjPynI2gWIUWjKXp"><i class="material-icons">description</i><span>Guideline</span></a></li> -->
                            <!-- <li><a href="#"><i class="material-icons">description</i><span>Card Registration</span></a></li> -->
                            <!-- <li><a href="#"><i class="material-icons">description</i><span>E-Certificate</span></a></li> -->
                            @endif
                            @if($event->event_id == 2)
                            <!-- <li><a href="{{url('downloads/Contoh Karya Descom 2025.pdf')}}"><i class="material-icons">description</i><span>Sample Work</span></a></li> -->
                            <li><a href="https://drive.google.com/drive/folders/1FRnQZ2fA1oJiCOeiaQeAjv_y3zqk2RiT"><i class="material-icons">description</i><span>Guideline</span></a></li>
                            <li><a href="{{url('event/'.$event->event_id)}}"><i class="material-icons">description</i><span>Upload Work</span></a></li>
                            @endif
                            @if($event->event_id == 3)
                            <!-- <li><a href="#"><i class="material-icons">description</i><span>Contoh Karya</span></a></li> -->
                            <li><a href="https://drive.google.com/drive/folders/1mHeYcknL1fr7HK1sVX_Ee1HVyb2fCRVt" href="#"><i class="material-icons">description</i><span>Guideline</span></a></li>
                            <li><a href="{{url('event/'.$event->event_id)}}"><i class="material-icons">description</i><span>Upload Work</span></a></li>
                            @endif
                        @endforeach
                    <!-- </ul>
                </li> -->
                @endif
                <li><a href="{{url('change-password')}}"><i class="material-icons">lock</i><span>Change Password</span></a></li>
                <li><a href="{{url('logout')}}"><i class="material-icons">input</i><span>Sign Out</span></a></li>
            </ul>
        </div>
        <!-- #Menu -->

        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                Copyright &copy;2015 - 2025
            </div>
            <div class="version">
                Made with <span style="color: #e25555;">&hearts;</span> by @riordhn
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
