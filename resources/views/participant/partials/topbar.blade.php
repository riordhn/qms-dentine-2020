@php
    $account = Auth::user();
    $events = \App\Models\Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
@endphp
<nav class="navbar bg-cyan">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand">
                @foreach ($events as $event)
                    @if($event->event_id == 1 || $event->event_id == 6 || $event->event_id == 7 || $event->event_id == 8)
                    <p>Dentine Olympiad 2025</p>
                    @elseif($event->event_id == 2)
                    <p>Dentine Essay 2025</p>
                    @elseif($event->event_id == 3)
                    <p>Dentine Poster 2025</p>
                    @elseif($event->event_id == 4)
                    <p>Dentine Stourine 2025</p>
                    @elseif($event->event_id == 5)
                    <p>Dentine Debate 2025</p>
                    @else
                    @endif
                    @php
                        break;
                    @endphp
                @endforeach
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <!-- <ul class="nav navbar-nav navbar-right"> -->
                <!-- Call Search -->
                <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
                <!-- #END# Call Search -->
            <!-- </ul> -->
        </div>
    </div>
</nav>
