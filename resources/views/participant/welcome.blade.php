
@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('participant/partials/topbar')
    @include('participant/partials/sidebar')
    @php
        $account = Auth::user();
        $events = \App\Models\Event::join('voucher_events', 'events.event_id', '=', 'voucher_events.event_id')->where('voucher_id', $account->voucher_id)->get();
        $voucher = \App\Models\Voucher::find($account->voucher_id);
    @endphp
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h4><b>Hello {{$account->name}}, welcome to the DENTINE 2025 participant page</b></h4>
                        </div>
                        <div class="body">
                            <ol>
                                <li>Thank you for registering for the <b>{{$events[0]->name}}</b>.</li>
                                <li>Please save your email account and password to make it easier for you to log in again.</li>
                                <li>To find out info related to the competition, you can click the icon at the top left and click the competition you are following.</li>
                                <li>Keep your spirits up and have a nice day. :)</li>
                            </ol>
                            <p>Kind regards</p>
                            @foreach ($events as $event)
                                @if($event->event_id == 1)
                                <p>Dentine Olympiad 2025 Admin</p>
                                @elseif($event->event_id == 2)
                                <p>Dentine Essay 2025 Admin</p>
                                @elseif($event->event_id == 3)
                                <p>Dentine Poster 2025 Admin</p>
                                @elseif($event->event_id == 4)
                                <p>Dentine Stourine 2025 Admin</p>
                                @elseif($event->event_id == 5)
                                <p>Dentine Debate 2025 Admin</p>
                                @else
                                @endif
                            @endforeach
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-univ.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-faculty.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-bem.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-dentine.png')}}"/>
                            @foreach ($events as $event)
                                @if($event->event_id == 1)
                                <img class="img" style="height: 128px;" src="{{asset('media/logo/logo-dentine-olympiad.png')}}"/>
                                @elseif($event->event_id == 2)
                                <img class="img" style="height: 128px;" src="{{asset('media/logo/logo-dentine-essay.png')}}"/>
                                @elseif($event->event_id == 3)
                                <img class="img" style="height: 128px;" src="{{asset('media/logo/logo-dentine-poster.png')}}"/>
                                @elseif($event->event_id == 4)
                                <img class="img" style="height: 128px;" src="{{asset('media/logo/logo-stourine.png')}}"/>
                                @elseif($event->event_id == 5)
                                <img class="img" style="height: 128px;" src="{{asset('media/logo/logo-debate.png')}}"/>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                @if($voucher->is_paid == 1)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="card">
                        <div class="header bg-red">
                            <h1>Attention</h4><br>
                            @if(!empty($voucher->image_id))
                            <b>You have uploaded proof of transfer. Please wait 2x24 hours max for us to confirm your payment.</b>
                            @else
                            <b>You haven't made a payment for the competition.</b>
                            @endif
                        </div>
                        <div class="body">
                            With registration fees:
                            @if($events[0]->event_id == 1)
                                <h3><b>DENTINE OLIMPYIAD</b></h3>
                                <b>ONLINE</b><br/>
                                - Early Bird: 125.000<br/>
                                - Gelombang 1: 150.000<br/>
                                - Gelombang 2: 160.000<br/>
                                <br/>
                                <b>OFFLINE </b><br/>
                                - Early Bird: 150.000<br/>
                                - Gelombang 1: 165.000<br/>
                                - Gelombang 2: 180.000<br/>
                            @endif
                            @if($events[0]->event_id == 2)
                                <h3><b>DENTINE ESSAY</b></h3>
                                <b>DOMESTIC</b><br/>
                                - Early Bird: 90.000<br/>
                                - Gelombang 1: 120.000<br/>
                                - Gelombang 2: 135.000<br/>

                                <b>INTERNATIONAL</b><br/>
                                - First Phase: USD $20<br/>
                            @endif
                            @if($events[0]->event_id == 3)
                                <h3><b>DENTINE POSTER</b></h3>
                                <b>DOMESTIC</b><br/>
                                - Early Bird: 75.000<br/>
                                - Gelombang 1: 85.000<br/>
                                - Gelombang 2: 90.000<br/>

                                <b>INTERNATIONAL</b><br/>
                                - First Phase: USD $15<br/>
                            @endif
                            @if($events[0]->event_id == 4)
                                <h3><b>DENTINE STROURINE</b></h3>
                                - Open Regist : 115.000<br/>
                            @endif
                            @if($events[0]->event_id == 5)
                                <h3><b>DENTINE DEBATE</b></h3>
                                - Early Bird: 100.000<br/>
                                - Gelombang 1: 120.000<br/>
                                - Gelombang 2: 150.000<br/>
                            @endif
                            @if($events[0]->event_id == 6)
                                <h4><b>Package 1:<br/> Stourine (1 Person)<br/>+ Dentine Scientific Olympiad Offline (1 Team)</b></h4>
                                - Package 1: 250.000<br/>
                            @endif
                            @if($events[0]->event_id == 7)
                                <h4><b>Package 2:<br/> Stourine (2 Person)<br/>+ Dentine Scientific Olympiad Offline (1 Team)</b></h4>
                                - Package 2: 340.000<br/>
                            @endif
                            @if($events[0]->event_id == 8)
                                <h4><b>Package 3:<br/> Stourine (3 Person)<br/>+ Dentine Scientific Olympiad Offline (1 Team)</b></h4>
                                - Package 3: 420.000<br/>
                            @endif
                            <br>
                            Clicking the button below to check invoice and submit payment
                            <!-- <a class="btn btn-block bg-pink waves-effect" href="{{url('payment-test')}}" id="pay-button">Payment</a> -->
                            <a class="btn bg-pink waves-effect" target="_blank" href="{{url('invoice/get/'.$voucher->voucher_id)}}">Download Invoice</a>
                            &nbsp;&nbsp;&nbsp;
                            <!-- <a class="btn bg-teal waves-effect" href="{{url('payment-confirmation')}}">Submit Payment</a> -->
                        </div>
                    </div>
                </div>
                @elseif($voucher->is_paid == 10)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="card">
                        <div class="header bg-green">
                            <h1>Thank you for making payment. Click the button below for download paid invoice</h4><br>
                            <a class="btn bg-cyan waves-effect" target="_blank" href="{{url('invoice/get/'.$voucher->voucher_id)}}">Download Paid Invoice</a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
