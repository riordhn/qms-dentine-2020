@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('participant/partials/topbar')
    @include('participant/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Payment Confirmation</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                Check Your Biodata Below
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" disabled="" value="{{$account->name}}">
                                            <label class="form-label">Leader Name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="email" class="form-control" disabled="" value="{{$account->email}}">
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" disabled="" value="{{$account->phone}}">
                                            <label class="form-label">Phone</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <span class="col-pink">Make sure that you fill in correctly</span>
                                    <p>Kind regards</p>
                                    <p>DENTINE 2025 Admin</p>
                                    <img class="img" style="height: 128px;" src="{{asset('media/logo-univ.png')}}"/>
                                    <img class="img" style="height: 128px;" src="{{asset('media/logo-faculty.png')}}"/>
                                    <img class="img" style="height: 128px;" src="{{asset('media/logo/main-logo.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                Upload your proof of transfer
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>Check invoice by click link below</p>
                                    <a class="btn bg-pink waves-effect" target="_blank" href="{{url('invoice/get/'.$voucher->voucher_id)}}">Download Invoice</a>
                                    @if(!in_array($voucher_event->event_id, [6,7,8]))
                                    &nbsp;
                                    &nbsp;
                                    @if(!empty($voucher->image_addon))
                                    @if($voucher->addon_status == 1)
                                    <span style="color:green;">Discount applied</span>
                                    @else
                                    <span style="color:red;">Please waiting us to applied your discount. (<a target="_blank" href="{{url('uploads/'.$voucher->image_addon->path)}}">File</a>)</span>
                                    @endif
                                    @else
                                    <a data-toggle="modal" data-target="#upload-modal">Get More Discount</a>
                                    @endif
                                    @endif
                                </div>
                                @if(!empty($voucher->image))
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>This is your proof of transfer</p>
                                    <a target="_blank" href="{{url('uploads/'.$voucher->image->path)}}">Check File/Download</a>
                                </div>
                                @endif
                                @if(empty($voucher->image))
                                <form action="{{url('payment-confirmation')}}" method="post" enctype="multipart/form-data">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>Please upload your proof of transfer by clicking the button below (Upload results will appear above)</p>
                                        {{csrf_field()}}
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                        <div class="fallback">
                                            <input name="file" type="file" required="" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>Input REFERRAL CODE below (Optional)</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                                        <div class="fallback">
                                            <input name="referral" type="text" />
                                        </div>
                                    </div>
                                    
                                    <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <button type="submit" class="btn bg-teal btn-block btn-lg waves-effect">Submit</button>
                                    </div> -->
                                </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

<!-- Modal -->
<div class="modal fade" id="upload-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{url('payment-confirmation')}}" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Upload Proof of discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ol>
                        <li>
                            Please upload Twibbon if you select event DENTINE OLYMPIAD and upload proof you shared event DENTINE POSTER, ESSAY, DEBATE
                        </li>
                        <li>
                            Please download template for twibbon in link here (<a href="https://drive.google.com/drive/folders/1ScE-EVkKVpzW7TXpuIWJ0KOXGhM_vknQ" target="_blank">Download</a>)
                        </li>
                        <li>
                            Discount will applied if your uploaded proof is verified<br/>(IDR 15.000 for DENTINE OLYMPIAD & IDR 5.000 for DENTINE POSTER/ESSAY/DEBATE)
                        </li>
                    </ol>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p><b>Proof of twibbon with your photo or proof of share the event</b></p>
                    </div>
                    {{csrf_field()}}
                    <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">
                        <div class="fallback">
                            <input name="file_tweet" type="file" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<!-- Javascript -->
@endsection
