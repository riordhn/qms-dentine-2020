
@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<style type="text/css">
    h1 {
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: bold;
        text-decoration: none;
        font-size: 13pt;
    }

    .body p {
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
        margin: 0pt;
    }

    .a {
        color: black;
        font-family: "Times New Roman", serif;
        font-style: italic;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    .s2 {
        color: black;
        font-family: Calibri, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    li {
        display: block;
    }

    #l1 {
        padding-left: 0pt;
        counter-reset: c1 1;
    }

    #l1>li>*:first-child:before {
        counter-increment: c1;
        content: counter(c1, decimal)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l1>li:first-child>*:first-child:before {
        counter-increment: c1 0;
    }

    #l2 {
        padding-left: 0pt;
    }

    #l2>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l3 {
        padding-left: 0pt;
    }

    #l3>li>*:first-child:before {
        content: "- ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l4 {
        padding-left: 0pt;
    }

    #l4>li>*:first-child:before {
        content: "- ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l5 {
        padding-left: 0pt;
    }

    #l5>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l6 {
        padding-left: 0pt;
        counter-reset: d1 1;
    }

    #l6>li>*:first-child:before {
        counter-increment: d1;
        content: "("counter(d1, upper-latin)") ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l6>li:first-child>*:first-child:before {
        counter-increment: d1 0;
    }

    #l7 {
        padding-left: 0pt;
        counter-reset: e1 1;
    }

    #l7>li>*:first-child:before {
        counter-increment: e1;
        content: "("counter(e1, upper-latin)") ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 11pt;
    }

    #l7>li:first-child>*:first-child:before {
        counter-increment: e1 0;
    }

    #l8 {
        padding-left: 0pt;
    }

    #l8>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l9 {
        padding-left: 0pt;
    }

    #l9>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    li {
        display: block;
    }

    #l10 {
        padding-left: 0pt;
        counter-reset: f1 1;
    }

    #l10>li>*:first-child:before {
        counter-increment: f1;
        content: counter(f1, decimal)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l10>li:first-child>*:first-child:before {
        counter-increment: f1 0;
    }

    li {
        display: block;
    }

    #l11 {
        padding-left: 0pt;
        counter-reset: g1 1;
    }

    #l11>li>*:first-child:before {
        counter-increment: g1;
        content: counter(g1, decimal)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l11>li:first-child>*:first-child:before {
        counter-increment: g1 0;
    }

    #l12 {
        padding-left: 0pt;
        counter-reset: g2 1;
    }

    #l12>li>*:first-child:before {
        counter-increment: g2;
        content: counter(g2, lower-latin)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l12>li:first-child>*:first-child:before {
        counter-increment: g2 0;
    }

    #l13 {
        padding-left: 0pt;
    }

    #l13>li>*:first-child:before {
        content: "- ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l14 {
        padding-left: 0pt;
    }

    #l14>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l15 {
        padding-left: 0pt;
    }

    #l15>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l16 {
        padding-left: 0pt;
    }

    #l16>li>*:first-child:before {
        content: "- ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l17 {
        padding-left: 0pt;
    }

    #l17>li>*:first-child:before {
        content: "● ";
        color: black;
        font-family: Arial, sans-serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l18 {
        padding-left: 0pt;
        counter-reset: g2 1;
    }

    #l18>li>*:first-child:before {
        counter-increment: g2;
        content: counter(g2, lower-latin)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l18>li:first-child>*:first-child:before {
        counter-increment: g2 0;
    }

    #l19 {
        padding-left: 0pt;
        counter-reset: g2 1;
    }

    #l19>li>*:first-child:before {
        counter-increment: g2;
        content: counter(g2, lower-latin)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l19>li:first-child>*:first-child:before {
        counter-increment: g2 0;
    }

    #l20 {
        padding-left: 0pt;
        counter-reset: g2 1;
    }

    #l20>li>*:first-child:before {
        counter-increment: g2;
        content: counter(g2, lower-latin)". ";
        color: black;
        font-family: "Times New Roman", serif;
        font-style: normal;
        font-weight: normal;
        text-decoration: none;
        font-size: 12pt;
    }

    #l20>li:first-child>*:first-child:before {
        counter-increment: g2 0;
    }
</style>
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('participant/partials/topbar')
    @include('participant/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">

                        <h1 style="padding-top: 3pt;padding-left: 171pt;text-indent: -133pt;line-height: 150%;text-align: center;">TATA TERTIB &amp; PETUNJUK PELAKSANAAN BABAK PENYISIHAN ONLINE DENTINE 2025</h1>
                        <p style="padding-top: 7pt;text-indent: 0pt;text-align: left;"><br /></p>
                        <ol id="l1">
                            <li data-list-text="1.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Babak penyisihan
                                    online dilaksanakan pada hari Sabtu, 8 Februari 2025 secara serentak dan peserta bisa mulai <i>log in
                                    </i>ke <a href="http://www.dentinefkgunair.com/" class="a" target="_blank">website
                                    </a><i>www.dentinefkgunair.com </i>pada pukul 09.30 WIB, 10.30 WITA, dan 11.30 WIT untuk melakukan
                                    registrasi.</p>
                            </li>
                            <li data-list-text="2.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Peserta diharuskan
                                    untuk sudah log in ke website dan log in ke akun masing-masing peserta di laptop maksimal 30 menit
                                    sebelum lomba dimulai.</p>
                            </li>
                            <li data-list-text="3.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Log in dengan
                                    username dan password yang telah dikonfirmasi oleh panitia melalui SMS atau chat dari WA/Line</p>
                            </li>
                            <li data-list-text="4.">
                                <p style="padding-left: 23pt;text-indent: -18pt;text-align: justify;">Setiap akun yang diberikan kepada
                                    peserta hanya dapat diakses oleh satu perangkat saja.</p>
                            </li>
                            <li data-list-text="5.">
                                <p style="padding-top: 6pt;padding-left: 23pt;text-indent: -18pt;text-align: justify;">Babak penyisihan
                                    terdiri dari 80 soal. Dengan rincian sebagai berikut.</p>
                                <ul id="l2">
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Babak Penyisihan
                                            terdiri dari 80 soal. Matematika dan Penalaran Umum : 25 Soal</p>
                                        <ul id="l3">
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">Kimia :
                                                    15 Soal</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">Fisika :
                                                    15 Soal</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">Biologi
                                                    : 15 Soal</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">
                                                    Kedokteran Gigi Dasar : 10 Soal</p>
                                            </li>
                                        </ul>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: left;">
                                            Soal disajikan per subtest, apabila waktu dari per subtest telah habis maka tidak dapat kembali
                                            ke subtest sebelumnya.</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-left: 45pt;text-indent: -18pt;text-align: left;">Satu soal terdiri dari 45 detik,
                                            dengan rincian waktu per subtes sebagai berikut</p>
                                        <ul id="l4">
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">
                                                    Matematika dan Penalaran Umum : 25 Soal x 45 detik = 18,75 menit</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">Kimia :
                                                    15 Soal x 45 detik = 11,25 menit</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">Fisika :
                                                    15 Soal x 45 detik = 11,25 menit</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: left;">Biologi
                                                    : 15 Soal x 45 detik = 11,25 menit</p>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 80pt;text-indent: -17pt;text-align: justify;">
                                                    Kedokteran Gigi Dasar : 10 Soal x 45 detik = 7,5 menit</p>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li data-list-text="6.">
                                <p style="padding-top: 6pt;padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                    Pengerjaan soal penyisihan online dimulai serentak pada pukul 09.30 WIB HINGGA 10.30 WIB dan berlangsung
                                    selama 60 MENIT. Untuk wilayah Indonesia bagian tengah, pengerjaan soal dimulai pukul 10.30 WITA hingga
                                    11.30 WITA. Untuk wilayah Indonesia bagian timur, pengerjaan soal dimulai pukul 11.30 WIT hingga 12.30
                                    WIT.</p>
                            </li>
                            <li data-list-text="7.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Setiap peserta WAJIB
                                    menyimpan jawaban di setiap nomor. Jawaban yang telah disimpan dapat diubah menjadi jawaban lain, dan
                                    dapat dihapus.</p>
                            </li>
                            <li data-list-text="8.">
                                <p style="padding-top: 3pt;padding-left: 23pt;text-indent: -18pt;text-align: justify;">Bila waktu sudah
                                    mencapai 60 menit, jawaban yang telah tersimpan akan otomatis tersubmit.</p>
                            </li>
                            <li data-list-text="9.">
                                <p style="padding-top: 6pt;padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                    Peserta DILARANG menggunakan alat penghitung, alat komunikasi (HP, dll) dan berkomunikasi dengan tim
                                    yang lain, jika peserta melanggar akan diberi peringatan hingga diskualifikasi dalam olimpiade DENTINE
                                    2025.</p>
                            </li>
                            <li data-list-text="10.">
                                <p style="padding-left: 23pt;text-indent: -18pt;text-align: justify;">Tipe soal berbentuk multiple choice
                                    sesuai dengan petunjuk soal sebagai berikut.</p>
                                <ul id="l5">
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Tipe 1: MCQ</p>
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: 0pt;line-height: 150%;text-align: left;">
                                            Pilih satu jawaban paling tepat diantara lima pilihan yaitu: (A), (B), (C), (D), atau (E)</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-left: 45pt;text-indent: -18pt;text-align: left;">Tipe 2: (1),(2),(3),(4).</p>
                                        <p style="padding-top: 6pt;padding-left: 44pt;text-indent: 0pt;text-align: left;">Tentukan pilihan
                                            yang paling tepat diantara beberapa pilihan</p>
                                        <ol id="l6">
                                            <li data-list-text="(A)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -19pt;text-align: left;">1,2,3
                                                </p>
                                            </li>
                                            <li data-list-text="(B)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -18pt;text-align: left;">1 dan 3
                                                </p>
                                            </li>
                                            <li data-list-text="(C)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -18pt;text-align: left;">2 dan 4
                                                </p>
                                            </li>
                                            <li data-list-text="(D)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -19pt;text-align: left;">4 saja
                                                </p>
                                            </li>
                                            <li data-list-text="(E)">
                                                <p style="padding-top: 6pt;padding-left: 62pt;text-indent: -18pt;text-align: left;">1,2,3,4
                                                </p>
                                            </li>
                                        </ol>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Tipe 3: sebab
                                            akibat</p>
                                        <ol id="l7">
                                            <li data-list-text="(A)">
                                                <p style="padding-top: 6pt;padding-left: 61pt;text-indent: -16pt;text-align: left;">
                                                    Pernyataan 1 dan 2 benar, saling berhubungan</p>
                                            </li>
                                            <li data-list-text="(B)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -18pt;text-align: left;">
                                                    Pernyataan 1 dan 2 benar, tidak ada hubungan</p>
                                            </li>
                                            <li data-list-text="(C)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -18pt;text-align: left;">
                                                    Pernyataan 1 benar dan pernyataan 2 salah</p>
                                            </li>
                                            <li data-list-text="(D)">
                                                <p style="padding-top: 6pt;padding-left: 64pt;text-indent: -19pt;text-align: left;">
                                                    Pernyataan 1 salah dan pernyataan 2 benar</p>
                                            </li>
                                            <li data-list-text="(E)">
                                                <p style="padding-top: 6pt;padding-left: 63pt;text-indent: -18pt;text-align: left;">
                                                    Pernyataan 1 dan 2 salah</p>
                                            </li>
                                        </ol>
                                    </li>
                                </ul>
                            </li>
                            <li data-list-text="11.">
                                <p style="padding-top: 6pt;padding-left: 23pt;text-indent: -17pt;text-align: justify;">Sistem penilaian :
                                </p>
                                <ul id="l8">
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Benar = +4</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Salah = -1</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Kosong = 0</p>
                                    </li>
                                </ul>
                            </li>
                            <li data-list-text="12.">
                                <p style="padding-top: 6pt;padding-left: 23pt;text-indent: -18pt;text-align: justify;">Jumlah peserta yang
                                    lolos ke babak semifinal sebanyak 50 tim yang diambil dari</p>
                                <ul id="l9">
                                    <li data-list-text="●">
                                        <p
                                            style="padding-top: 7pt;padding-left: 45pt;text-indent: -18pt;line-height: 149%;text-align: justify;">
                                            28 tim terbaik yaitu juara 1 dan 2 wilayah rayon offline dari 14 wilayah <span class="s2">offline
                                            </span>(Surabaya, Lamongan, Gresik, Sidoarjo, Madura, Kediri, Malang,
                                            Jombang, Probolinggo, Madiun, Tulungagung, Bondowoso, Ponorogo, Bojonegoro)</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-left: 45pt;text-indent: -18pt;text-align: justify;">3 tim dari juara 3, juara
                                            harapan 1 dan 2 rayon Surabaya.</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-top: 7pt;padding-left: 45pt;text-indent: -18pt;text-align: justify;">Top 9 tim
                                            terbaik secara akumulatif nasional pada penyisihan <span class="s2">offline</span>.</p>
                                    </li>
                                    <li data-list-text="●">
                                        <p style="padding-top: 3pt;padding-left: 45pt;text-indent: -18pt;text-align: left;">Top 10 tim
                                            terbaik secara akumulatif nasional pada penyisihan <span class="s2">online</span>.</p>
                                    </li>
                                </ul>
                            </li>
                            <li data-list-text="13.">
                                <p style="padding-top: 7pt;padding-left: 23pt;text-indent: -18pt;line-height: 152%;text-align: left;">50
                                    besar semifinalis akan diumumkan paling lambat (tanggal menyusul) pukul 23.59 WIB melalui <span
                                        class="s2">website </span>resmi dan media sosial dentine lainnya.</p>
                            </li>
                            <li data-list-text="14.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 13pt;text-align: left;">Keputusan panitia TIDAK
                                    dapat diganggu gugat.</p>
                            </li>
                        </ol>
                        <h1 style="padding-top: 3pt;padding-left: 178pt;text-indent: -114pt;line-height: 150%;text-align: center;">TATA TERTIB &amp; KETENTUAN DEVICE BABAK PENYISIHAN ONLINE DENTINE 2025</h1>
                        <p style="padding-top: 7pt;text-indent: 0pt;text-align: left;"><br /></p>
                        <ol id="l10">
                            <li data-list-text="1.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Setiap tim wajib
                                    berkumpul di satu tempat yang kondusif dan memastikan koneksi internet lancar dengan tetap melaksanakan
                                    protokol kesehatan.</p>
                            </li>
                            <li data-list-text="2.">
                                <p style="padding-left: 23pt;text-indent: -18pt;text-align: justify;">Setiap akun yang digunakan untuk login
                                    hanya bisa diakses melalui 1 device.</p>
                            </li>
                            <li data-list-text="3.">
                                <p style="padding-top: 6pt;padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                    Peserta wajib menyiapkan maksimal 2 device yang berupa 1 laptop &amp; 1 handphone. Laptop digunakan
                                    untuk login di official website DENTINE 2025 dan handphone digunakan untuk melakukan kontak pengaduan.
                                </p>
                            </li>
                            <li data-list-text="4.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Pastikan peserta
                                    untuk mulai mengerjakan soal sesuai dengan waktu yang diberikan, keterlambatan proses pengerjaan tidak
                                    akan diberikan perpanjangan waktu dan di luar tanggung jawab panitia. Toleransi keterlambatan pengerjaan
                                    soal 10 menit.</p>
                            </li>
                            <li data-list-text="5.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Pengawasan akan
                                    dilakukan melalui <i>website</i>, pastikan peserta tidak membuka <i>tab </i>atau halaman lain selain
                                    halaman DENTINE 2025 selama proses pengerjaan soal berlangsung. Akan terdapat peringatan pada laman
                                    pengerjaan soal bila peserta terdeteksi membuka <i>tab </i>maupun <i>website </i>lain.</p>
                            </li>
                            <li data-list-text="6.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Peserta dilarang
                                    menggunakan alat komunikasi lain selain yang tertera di atas, alat bantu hitung, serta berkomunikasi
                                    dengan tim lain selama pelaksanaan DENTINE 2025.</p>
                            </li>
                            <li data-list-text="7.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Panitia berhak
                                    memberi peringatan bahkan mendiskualifikasi Peserta yang terbukti melakukan kecurangan dalam bentuk
                                    apapun.</p>
                            </li>
                            <li data-list-text="8.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Segala kendala
                                    berupa masalah koneksi, suasana tidak kondusif, maupun internal Peserta bukan merupakan tanggung jawab
                                    panitia.</p>
                            </li>
                        </ol>
                        <h1 style="padding-top: 3pt;padding-left: 230pt;text-indent: -219pt;line-height: 150%;text-align: left;">PELANGGARAN
                            &amp; KETENTUAN TROUBLE BABAK PENYISIHAN DENTINE 2025</h1>
                        <p style="padding-top: 7pt;text-indent: 0pt;text-align: left;"><br /></p>
                        <ol id="l11">
                            <li data-list-text="1.">
                                <p style="padding-left: 23pt;text-indent: -18pt;text-align: left;">PELANGGARAN</p>
                                <ol id="l12">
                                    <li data-list-text="a.">
                                        <p style="padding-top: 6pt;padding-left: 37pt;text-indent: -17pt;text-align: left;">Pelanggaran
                                            ringan</p>
                                        <ul id="l13">
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 51pt;text-indent: -17pt;text-align: left;">Kriteria
                                                    pelanggaran</p>
                                                <ul id="l14">
                                                    <li data-list-text="●">
                                                        <p
                                                            style="padding-top: 6pt;padding-left: 66pt;text-indent: -18pt;line-height: 150%;text-align: left;">
                                                            Terdapat peringatan peserta yang membuka halaman lain selama proses pengerjaan
                                                            soal (2 kali).</p>
                                                    </li>
                                                    <li data-list-text="●">
                                                        <p style="padding-left: 66pt;text-indent: -18pt;text-align: left;">Terlambat memulai
                                                            pengerjaan soal sesuai dengan waktu yang ditentukan.</p>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 51pt;text-indent: -17pt;text-align: left;">Sanksi
                                                </p>
                                                <ul id="l15">
                                                    <li data-list-text="●">
                                                        <p style="padding-top: 6pt;padding-left: 66pt;text-indent: -18pt;text-align: left;">
                                                            Terdapat peringatan dari laman pengerjaan sebanyak 2 kali</p>
                                                    </li>
                                                    <li data-list-text="●">
                                                        <p style="padding-top: 6pt;padding-left: 66pt;text-indent: -18pt;text-align: left;">
                                                            Pengurangan waktu</p>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li data-list-text="b.">
                                        <p style="padding-top: 6pt;padding-left: 37pt;text-indent: -18pt;text-align: left;">Pelanggaran
                                            berat</p>
                                        <ul id="l16">
                                            <li data-list-text="-">
                                                <p style="padding-top: 6pt;padding-left: 51pt;text-indent: -17pt;text-align: left;">Kriteria
                                                    pelanggaran</p>
                                                <ul id="l17">
                                                    <li data-list-text="●">
                                                        <p style="padding-top: 6pt;padding-left: 66pt;text-indent: -18pt;text-align: left;">
                                                            Menggunakan handphone atau kalkulator sebagai alat bantu</p>
                                                    </li>
                                                    <li data-list-text="●">
                                                        <p style="padding-top: 6pt;padding-left: 66pt;text-indent: -18pt;text-align: left;">
                                                            Kerjasama dalam bentuk apapun baik dengan tim lain maupun guru pembimbing.</p>
                                                    </li>
                                                    <li data-list-text="●">
                                                        <p
                                                            style="padding-top: 6pt;padding-left: 66pt;text-indent: -18pt;line-height: 150%;text-align: left;">
                                                            Terdapat lebih dari dua kali peringatan peserta yang membuka halaman lain selama
                                                            proses pengerjaan soal</p>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li data-list-text="-">
                                                <p style="padding-left: 51pt;text-indent: -17pt;text-align: left;">Sanksi : langsung
                                                    diskualifikasi tanpa pemberitahuan</p>
                                                <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                            </li>
                                        </ul>
                                    </li>
                                </ol>
                            </li>
                            <li data-list-text="2.">
                                <p style="padding-left: 26pt;text-indent: -21pt;text-align: left;">KETENTUAN APABILA SOAL BERMASALAH ATAU
                                    KURANG LENGKAP</p>
                                <ol id="l18">
                                    <li data-list-text="a.">
                                        <p style="padding-top: 6pt;padding-left: 44pt;text-indent: -17pt;text-align: justify;">Peserta
                                            melapor kendala ke CP berikut : (Rizqi / 081257704384).</p>
                                    </li>
                                    <li data-list-text="b.">
                                        <p
                                            style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                            Peserta harus mengirimkan bukti (foto/screenshot soal yang bermasalah dengan memberikan
                                            keterangan). Peserta diperbolehkan menggunakan handphone untuk kepentingan tersebut.</p>
                                    </li>
                                    <li data-list-text="c.">
                                        <p style="padding-left: 44pt;text-indent: -17pt;text-align: justify;">Panitia akan memproses soal
                                            yang bermasalah dan akan di infokan secepatnya.</p>
                                        <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                    </li>
                                </ol>
                            </li>
                            <li data-list-text="3.">
                                <p style="padding-left: 23pt;text-indent: -18pt;text-align: left;">KETENTUAN APABILA WEBSITE ERROR</p>
                                <ol id="l19">
                                    <li data-list-text="a.">
                                        <p style="padding-top: 6pt;padding-left: 44pt;text-indent: -17pt;text-align: justify;">Peserta
                                            melapor kendala ke CP berikut : (Rizqi / 081257704384).</p>
                                    </li>
                                    <li data-list-text="b.">
                                        <p
                                            style="padding-top: 3pt;padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                            Peserta harus mengirimkan bukti (foto/screenshot website yang error dengan menampilkan waktu dan
                                            tanggalnya). Peserta diperbolehkan menggunakan handphone untuk kepentingan tersebut.</p>
                                    </li>
                                    <li data-list-text="c.">
                                        <p style="padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Apabila
                                            website sudah bisa diakses, maka pengawas memberitahukan melalui CP yang dihubungi</p>
                                    </li>
                                    <li data-list-text="d.">
                                        <p style="padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: justify;">Apabila
                                            error lebih dari 10 menit, peserta akan dikirim link untuk join ke grup khusus error untuk
                                            penjadwalan ulang.</p>
                                        <p style="padding-top: 6pt;text-indent: 0pt;text-align: left;"><br /></p>
                                    </li>
                                </ol>
                            </li>
                            <li data-list-text="4.">
                                <p style="padding-left: 23pt;text-indent: -18pt;line-height: 150%;text-align: left;">KETENTUAN APABILA
                                    BERMASALAH DENGAN DEVICE PENGERJAAN SOAL (LAPTOP, KOMPUTER)</p>
                                <ol id="l20">
                                    <li data-list-text="a.">
                                        <p style="padding-left: 44pt;text-indent: -17pt;text-align: justify;">Peserta melapor kendala ke CP
                                            berikut : (Rizqi / 081257704384).</p>
                                    </li>
                                    <li data-list-text="b.">
                                        <p
                                            style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                            Peserta harus mengirimkan bukti (foto/screenshot device pengerjaan soal yang error dengan
                                            menampilkan waktu dan tanggalnya). Peserta diperbolehkan menggunakan handphone untuk kepentingan
                                            tersebut.</p>
                                    </li>
                                    <li data-list-text="c.">
                                        <p style="padding-left: 44pt;text-indent: -17pt;text-align: justify;">Setelah dikonfirmasi, peserta
                                            diperkenankan memperbaiki atau mengganti device.</p>
                                    </li>
                                    <li data-list-text="d.">
                                        <p
                                            style="padding-top: 6pt;padding-left: 45pt;text-indent: -18pt;line-height: 150%;text-align: justify;">
                                            Tidak ada penambahan waktu pengerjaan karena merupakan kesalahan internal peserta dan bukan
                                            merupakan tanggung jawab panitia seperti yang telah disebutkan di atas.</p>
                                    </li>
                                </ol>
                            </li>
                        </ol>
                        <a href="{{url('test')}}" class="btn btn-block btn-lg bg-green waves-effect">
                        Kerjakan Ujian Sekarang
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
