@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('participant/partials/topbar')
    @include('participant/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h4><b>The following are terms and conditions, as well as important information regarding the {{$event->name}}</b></h4>
                        </div>
                        <div class="body">
                        @php
                            $filename = str_replace(' ', '-', strtolower($event->name));
                        @endphp
                            @include('participant/partials/event-info/'.$filename)
                            <span class="col-pink">Make sure that you fill in correctly</span>
                            <p>Kind regards</p>
                            <p>DENTINE 2025 Admin</p>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-univ.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo-faculty.png')}}"/>
                            <img class="img" style="height: 128px;" src="{{asset('media/logo/main-logo.png')}}"/>
                            @if(empty($participant))
                            <form class="form-validation" method="POST" action="{{url('event/'.$event->event_id.'/participate')}}">
                            {{csrf_field()}}
                            <p class="col-pink">Click the button below to fill in the team data</p>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">COMPLETE DATA CONTENT OF MEMBERS</button>
                                </div>
                            </div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-teal">
                            <h4><b>Team Member Data</b></h4>
                        </div>
                    </div>
                </div>
                <form class="form-validation" method="POST" action="{{url('event/'.$event->event_id.'/regist')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @for($i=0; $i<$participant_number; $i++)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="header bg-pink">
                                <h4><b>Data {{$participate_as[$i]}}</b></h4>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="name[]" required="" aria-required="true" aria-invalid="true" oninput="this.value = this.value.toUpperCase()">
                                                <label class="form-label">Full name</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment_ind</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="participate_as[]" readonly="" required="" aria-required="true" aria-invalid="true" value="{{$participate_as[$i]}}">
                                                <label class="form-label">Participate As</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="organization[]" required="" aria-required="true" aria-invalid="true" value="{{$account->organization}}" oninput="this.value = this.value.toUpperCase()">
                                                <label class="form-label">School</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">format_list_numbered</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <select class="form-control show-tick" name="generation[]" required="">
                                                <option value="">Select Student</option>
                                                <option value="X">Class X Senior High School</option>
                                                <option value="XI">Class XI Senior High School</option>
                                                <option value="XII">Class XII Senior High School</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">bookmark</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="status[]" required="">
                                                <option value="STUDENT">STUDENT</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">extension</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="nim[]" required="" aria-required="true" aria-invalid="true" oninput="this.value = this.value.toUpperCase()">
                                                <label class="form-label">Student Number</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">perm_contact_calendar</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control datepicker" name="birthday[]" required="" aria-required="true" aria-invalid="true" value="1990-01-01">
                                                <label class="form-label">Date of birth</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="phone[]" required="" aria-required="true" aria-invalid="true">
                                                <label class="form-label">Phone</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">whatshot</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="social_media[]" aria-required="true" aria-invalid="true">
                                                <label class="form-label">LINE ID</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <label class="form-label">Student Card (Image)</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="fallback">
                                            <input name="file[]" type="file" required="" multiple />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <label class="form-label">3x4 Formal Photo (Image)</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="fallback">
                                            <input name="profile[]" type="file" required="" multiple />
                                        </div>
                                    </div>
                                </div>
                                <div class="msg"><span class="col-pink font-italic">*Make sure your data input is correct</span></div>
                            </div>
                        </div>
                    </div>
                    @endfor
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">SAVE ALL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        time: false
    });
</script>
@endsection
