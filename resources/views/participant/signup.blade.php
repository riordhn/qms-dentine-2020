@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="login-page" style="background-image: url('media/bg-login-02.png'); background-size: cover;">
    <div class="login-box">
        <div class="logo align-center">
            <img class="m-b-15" src="{{asset('media/logo/main-logo.png')}}" width="350"/>
        </div>
        <div class="card" style="border-radius: 25px;">
            <div class="body">
                <form class="form-validation" method="POST" action="{{url('signup')}}">
                    {{csrf_field()}}
                    <div class="msg"><b>Fill in the complete Team Leader data below</b><br><span class="col-pink">Make sure that you fill in correctly</span></div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="name" required="" aria-required="true" aria-invalid="true" oninput="this.value = this.value.toUpperCase()">
                                    <label class="form-label">Leader Name</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">domain</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="organization" required="" aria-required="true" aria-invalid="true" oninput="this.value = this.value.toUpperCase()">
                                    <label class="form-label">School</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="margin:0">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">extension</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="margin:0">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" name="event_id" required="" onchange="changeEventAction(this)">
                                        <option value="" disabled selected>Select Event</option>
                                        @foreach($events as $event)
                                        <option value="{{$event->event_id}}">{{$event->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="is_other_country" class="row clearfix" style="display: none;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0">
                            <label>Choose you come from</label>
                            <div class="form-group">
                                <div class="demo-radio-button">
                                    <input name="is_other_country" type="radio" checked="" id="radio_0" value="0" onchange="changeOtherAction()">
                                    <label for="radio_0">I am from Indonesia</label>
                                    <input name="is_other_country" type="radio" id="radio_1" value="1" onchange="changeOtherAction()">
                                    <label for="radio_1">I am from outside Indonesia</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="country_id" class="row clearfix" style="display: none;">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">nature</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick is-select2" name="country_id" style="width: 100%;">
                                        <option value="">From Country</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="province_id" class="row clearfix" style="display: none;">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">nature</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick is-select2" name="province_id" style="width: 100%;">
                                        <option value="">From Province</option>
                                        @foreach($provinces as $province)
                                        <option value="{{$province->province_id}}">{{$province->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="city_id" class="row clearfix" style="display: none;">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">nature</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick is-select" name="city_id" style="width: 100%;">
                                        <option value="">From Region/City</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="rayon_id" class="row clearfix" style="display: none;">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">nature</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick is-select" name="rayon_id" style="width: 100%;">
                                        <option value="">Choose Rayon</option>
                                        @foreach($rayons as $rayon)
                                        <option value="{{$rayon->rayon_id}}">{{$rayon->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="phone" required="" aria-required="true" aria-invalid="true">
                                    <label class="form-label">Phone</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mail</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" name="email" required="" aria-required="true" aria-invalid="true">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="password" type="password" class="form-control" minlength="3" name="password" required="" aria-required="true">
                                    <label class="form-label">Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="retype-password" required="" aria-required="true" equalto="#password">
                                    <label class="form-label">Re-type password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-cyan waves-effect" type="submit">REGISTER</button>
                        </div>
                    </div>
                    <!-- <div class="row m-t-15 m-b--20">
                        <div class="col-xs-9">
                            Already have an account?
                        </div>
                        <div class="col-xs-3 align-right">
                            <a href="{{url('/')}}">Login</a>
                        </div>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    initAutoSelect2();

    $('select[name=province_id]').on('change', function(e){
        var province_id = e.target.value;
        $.ajax({
            type: 'GET',
            url: '{{url('change-province')}}',
            data: {
                province_id: province_id
            },
            success: function (data) {
                $('select[name=city_id]').empty();

                $('select[name=city_id]').append($('<option>')
                    .attr('value', '')
                    .text('Select Region/City')
                );

                $.each(data, function(index, obj){
                    $('select[name=city_id]').append($("<option>")
                        .attr("value", obj.city_id)
                        .text(obj.name)
                    );
                })

                $('select[name=city_id]').select2();
            },
        });
    });

    function changeEventAction(el){
        // 1 dentine, 2 essay, 3 poster, 4 stourine, 5 debate
        if($(el).val() == 1 || $(el).val() == 6 || $(el).val() == 7 || $(el).val() == 8){
            $('#is_other_country').hide();
            $('#country_id').hide();

            $('#province_id').show();
            $('select[name=province_id]').attr('required', true);
            $('#city_id').show();
            $('select[name=city_id]').attr('required', true);
            $('#rayon_id').show();
            $('select[name=rayon_id]').attr('required', true);
        }else if($(el).val() == 2 || $(el).val() == 3){
            $('#is_other_country').show();
            $('#country_id').hide();

            $('#province_id').hide();
            $('select[name=province_id]').attr('required', false);
            $('#city_id').hide();
            $('select[name=city_id]').attr('required', false);
            $('#rayon_id').hide();
            $('select[name=rayon_id]').attr('required', false);

            changeOtherAction();
        }else if($(el).val() == 5){
            $('#is_other_country').hide();
            $('#country_id').hide();

            $('#province_id').show();
            $('select[name=province_id]').attr('required', true);
            $('#city_id').show();
            $('select[name=city_id]').attr('required', true);
            $('#rayon_id').hide();
            $('select[name=rayon_id]').attr('required', false);
        }else{
            $('#is_other_country').hide();
            $('#country_id').hide();
            $('#province_id').hide();
            $('select[name=province_id]').attr('required', false);
            $('#city_id').hide();
            $('select[name=city_id]').attr('required', false);
            $('#rayon_id').hide();
            $('select[name=rayon_id]').attr('required', false);
        }
    }

    function changeOtherAction(){
        if($('input[name=is_other_country]:checked').val() == 0){ // ID
            $('#country_id').hide();
            $('#province_id').show();
            $('#city_id').show();
        }else{
            $('#country_id').show();
            $('#province_id').hide();
            $('#city_id').hide();
        }
    }
</script>
@endsection
