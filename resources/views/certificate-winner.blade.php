<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DENTINE PASS THE EXAM 2025 FKG UNAIR</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <style>
        .container {
            position: relative;
            text-align: center;
            color: black;
        }

        .name {
            width: 100%;
            top: 33.5%;
            left: 0;
            display: block;
            text-align: center;
            /* transform: translate(-50%, -50%); */
            position: absolute;
            font-family: 'Roboto', serif;
            font-size: 32px;
        }
    </style>
</head>
<body style="background:white;">
    <!-- <div class="container"> -->
        @if($participant_member->participant->event_id == 1)
        <img src="{{asset('media/certificate/olympiad-winner.png')}}" style="width: 95%; margin-left: 2%; position: absolute;">
        @elseif($participant_member->participant->event_id == 5)
        <img src="{{asset('media/certificate/debate-winner.png')}}" style="width: 95%; margin-left: 2%; position: absolute;">
        @endif
        <div class="name">{{strtoupper($participant_member->name)}}</div>
    <!-- </div> -->
</body>
</html>