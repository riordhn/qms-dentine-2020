<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DENTINE 2025 FKG UNAIR</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{asset('css/style.css')}}?v=3" rel="stylesheet">
    <style>
        .table tbody tr td, .table tbody tr th{
            border:0; !important;
        }

        .table tbody tr th{
            width:35%;
        }

        p.separator{
            height: 1px;
            width: 100%;
            border: 0;
            margin-top: 12px;
            border-top: 12px solid #0b406e;
            padding-bottom: 4px;
            border-bottom: 18px solid #c72222;
        }
    </style>
</head>
<body style="background:white;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table style="width:100%;">
                    <tbody>
                        <tr>
                            <td style="width:25%;"><img src="{{asset('media/logo/main-logo.png')}}" width="176"></td>
                            <td><p class="separator"> </p></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">
                <h4>BUKTI TANDA PENDAFTARAN PESERTA DENTINE 2025 FKG UNAIR</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>NO PESERTA</th>
                            <td>: {{$voucher->code}}</td>
                        </tr>
                        @foreach($participant_members as $i => $participant_member)
                        @if($i == 0)
                        <tr>
                            <th>NAMA KETUA</th>
                            <td>: {{$participant_member->name}} | Kelas: {{$participant_member->organization_status}}</td>
                        </tr>
                        @elseif($i == 1)
                        <tr>
                            <th>NAMA ANGGOTA</th>
                            <td>: ({{$i}}) {{$participant_member->name}} | Kelas: {{$participant_member->organization_status}}</td>
                        </tr>
                        @else
                        <tr>
                            <th></th>
                            <td>: ({{$i}}) {{$participant_member->name}} | Kelas: {{$participant_member->organization_status}}</td>
                        </tr>
                        @endif
                        @endforeach
                        @if(!empty($regional))
                        <tr>
                            <th>RAYON</th>
                            <td>: {{$regional->name}}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>UNIVERSITAS</th>
                            <td>: {{$participant_members->first()->organization}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
                    Diharapkan peserta membawa bukti tanda pendaftaran peserta DENTINE 2025 FKG UNAIR ini saat berlangsungnya babak penyisihan untuk menentukan ruangan ujian di rayon masing-masing.
                    <br>
                    Semoga sukses!
                </p>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <tbody>
                    <tr>
                        @foreach($participant_members as $i => $participant_member)
                        <td style="text-align:center;">
                            <img class="thumbnail" style="width:144px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALkAAAERCAMAAAAQS/ZZAAAAGFBMVEX+/v7///+TkpKVlJWZl5h8enuDgYJ3dXZjL6NLAAABL0lEQVR4nO3OgQnDMBAAMSd2mv03bukQBw/SBFqfsyc6z7qvmfZvvia6bvOaec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3zHvmPfOeec+8Z94z75n3/vOZ9nrOnui8X22+DCzG/6bBAAAAAElFTkSuQmCC">
                            <br>
                            <p>{{$participant_member->name}}<br>{{$participant_member->participate_as}}</p>
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                <table style="width:100%;">
                    <tbody>
                        <tr>
                            <td><p class="separator"> </p></td>
                            <td style="width:25%;"><img src="{{asset('media/logo/main-logo.png')}}" width="176"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
