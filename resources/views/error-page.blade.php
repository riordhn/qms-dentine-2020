<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="token" content="{{csrf_token()}}">
        <title>DENTINE 2025 FKG UNAIR</title>
        <!-- Favicon-->
        <!-- <link rel="icon" href="{{asset('media/favico-144.png')}}" type="image/x-icon"> -->

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />

        <!-- Toast -->
        <link rel="stylesheet" href="{{asset('plugins/vex-4.0.1/dist/css/vex.css')}}" >
        <link rel="stylesheet" href="{{asset('plugins/vex-4.0.1/dist/css/vex-theme-flat-attack.css')}}" >

        <!-- Bootstrap Select Css -->
        <link href="{{asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="{{asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />

        <!-- JQuery DataTable Css -->
        <link href="{{asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

        <!-- Dropzone Css -->
        <link href="{{asset('plugins/dropzone/dropzone.css')}}" rel="stylesheet">

        <!-- Custom Css -->
        <link href="{{asset('css/style.css')}}?v=3" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="{{asset('css/themes/all-themes.css')}}" rel="stylesheet" />
    </head>

    <body>
        @if(session()->has('err'))
            {{session('err')}}
        @endif
        <div class="_2">Click button below to login again or please continue in other device</div>
        <a class="btn" href="{{url('/')}}">Back To Login Page</a>
    </body>

</html>
