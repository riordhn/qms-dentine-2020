@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="true" aria-controls="collapseExample">
                            <h2>
                                Filter Table
                            </h2>
                        </div>
                        <div class="body collapse in" id="collapseExample" aria-expanded="true">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <b>Paid status</b>
                                    <select class="form-control show-tick" onchange="selectSearch(this, 6)">
                                        <option value="">Select Paid/No</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Account
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="primary_table" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Leader Name</th>
                                            <th>Email</th>
                                            <th>Events</th>
                                            <th>Registration Code</th>
                                            <th>Referral</th>
                                            <th>Paid (Y/N)</th>
                                            <th>Invoice</th>
                                            <th>Twibbon</th>
                                            <th>Bukti Transfer</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Leader Name</th>
                                            <th>Email</th>
                                            <th>Events</th>
                                            <th>Registration Code</th>
                                            <th>Referral</th>
                                            <th>Paid (Y/N)</th>
                                            <th>Invoice</th>
                                            <th>Twibbon</th>
                                            <th>Bukti Transfer</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    var dtButtonConfig = {
        buttons: [{
            extend: "pageLength",
        }, {
            extend: "print",
            footer: true,
            text: "PDF",
            orientation: "landscape",
            exportOptions: {
                columns: ":visible",
                stripHtml: false
            },
            customize: function (e) {
                $(e.document.body).css("font-size", "10pt"), $(e.document.body).find("table").addClass("compact").css("font-size", "inherit")
            }
        }, {
            extend: "excelHtml5",
            footer: true,
            exportOptions: {
                columns: ":visible"
            }
        }],
        dom: {
            button: {
                className: "btn"
            }
        }
    };
    var dtLengButton=[[ 10, 25, 50, 100, -1 ], [ '10', '25', '50', '100', 'All' ]];
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });
    // $(function(){
        var primary_table = $('#primary_table').DataTable({
            processing: true,
            // serverSide: true,
            dom: 'Bfrtip',
            lengthMenu: dtLengButton,
            buttons: dtButtonConfig,
            ajax: {
                url: '{{url('organizer/voucher/table')}}',
                type: 'POST'
            },
            columns: [
                { data: 'index_table', defaultContent: '', searchable: false, orderable: false },
                { data: 'name', name: 'accounts.name' },
                { data: 'email', name: 'email' },
                { data: 'events', name: 'events', orderable: false},
                { data: 'code', name: 'code', orderable: false},
                { data: 'referral', name: 'referral', orderable: false},
                { data: 'is_paid', name: 'is_paid', orderable: false,
                    render: function(data) {
                        if(data == 'YES'){
                            return '<span class="badge bg-green">'+ data +'</spam>';
                        }else{
                            return '<span class="badge bg-pink">'+ data +'</span>';
                        }
                    }
                },
                { data: 'action', name: 'action', searchable: false, orderable: false,
                    render: function(data) {
                        return `<a target="_blank" href="{{url('invoice/get')}}/${data.id}">
                                    Download
                                </a>`;
                    }
                },
                { data: 'tweetbond', name: 'tweetbond', searchable: false, orderable: false,
                    render: function(data, type, row) {
                        var html =  '';
                        if(data == null){
                        }else{
                            html += '<a class="btn bg-pink btn-circle waves-effect waves-circle waves-float" target="_blank" href="' +data+ '">'+
                                '    <i class="material-icons">image</i>'+
                                '</a>';

                            if(row.addon_status == 0){
                                html +=`<button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-id="${row.action.id}" onclick="actionPayVoucher(this, 'addon')">
                                    <i class="material-icons">check</i>
                                </button>`;
                            }
                        }

                        return html;
                    }
                },
                { data: 'payment', name: 'payment', searchable: false, orderable: false,
                    render: function(data, type, row) {
                        var html =  '';
                        if(data == null){
                        }else{
                            html += '<a class="btn bg-red btn-circle waves-effect waves-circle waves-float" target="_blank" href="' +data+ '">'+
                                '    <i class="material-icons">print</i>'+
                                '</a>';

                            if(row.action.status == 1){
                                html +=`<button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-id="${row.action.id}" onclick="actionPayVoucher(this, 'pay')">
                                    <i class="material-icons">check</i>
                                </button>`;
                            }
                        }

                        return html;
                    }
                },
                // { data: 'action', name: 'action', searchable: false, orderable: false,
                //     render: function(data) {
                //         var html =  '';
                //         if(data.status == 1){
                //             html +='<button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionPayVoucher(this)">'+
                //             '    <i class="material-icons">check</i>'+
                //             '</button>';
                //         }

                //         @if(Auth::user()->account_id == 1)
                //         // html +='<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionDelete(this)">'+
                //         //     '    <i class="material-icons">delete</i>'+
                //         //     '</button>';
                //         @endif
                //         return  html;
                //     }
                // }
            ],
            initComplete: function () {
                this.api().columns('.search-filter').every(function () {
                    var column = this;
                    var input = document.createElement('input');
                    $(input).appendTo($(column.header()))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false).draw();
                    });
                });

                this.api().columns('.select-filter').every(function () {
                    var column = this;
                    var select = document.createElement("select");
                    var option = document.createElement("option");
                    option.value = "";
                    option.text = "";
                    option.selected = true;
                    select.appendChild(option);
                    column.data().unique().sort().each( function ( d, j ) {
                        var option = document.createElement("option");
                        option.value = d;
                        option.text = d;
                        select.appendChild(option);
                    } );
                    $(select).appendTo($(column.header()))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false).draw();
                    });
                });
            }
        });

        primary_table.on( 'draw', function () {
            primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                var start = this.page.info().page * this.page.info().length;
                cell.innerHTML = start + i + 1;
                primary_table.cell(cell).invalidate('dom');
            } );
        } ).draw();
    // });

    function actionPayVoucher(element, type){
        var item = $(element);
        item.prop('disabled', true);
        var url = '{{url('organizer/voucher/pay')}}';
        vex.dialog.confirm({
            message: 'Are you sure to confirm this?',
            callback: function (value) {
                if(value){
                    $.ajax({
                        type: "POST",
                        url: url,
                        data:{
                            voucher: item.attr('data-id'),
                            type: type
                        },
                        success: function (data) {
                            primary_table.ajax.reload(null, false);
                            // location.reload();
                        },
                        error: function (xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }else{
                    item.prop('disabled', false);
                }
            }
        })
    }

    function actionDelete(element){
        var item = $(element);
        item.prop('disabled', true);
        var url = '{{url('organizer/voucher/delete')}}';
        vex.dialog.confirm({
            message: 'Are you sure to delete this?',
            callback: function (value) {
                if(value){
                    $.ajax({
                        type: "POST",
                        url: url,
                        data:{
                            voucher: item.attr('data-id')
                        },
                        success: function (data) {
                            location.reload();
                        },
                        error: function (xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }else{
                    item.prop('disabled', false);
                }
            }
        })
    }

    function selectSearch(el, column){
        var search = $.fn.dataTable.util.escapeRegex($(el).val());
        primary_table.columns( column ).search(search ? search : '', true, false).draw();
    }
</script>
@endsection
