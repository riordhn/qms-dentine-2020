@php
    $account = (object) json_decode(session('user'));
@endphp
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="https://ui-avatars.com/api/?background=001935&color=fff&name={{$account->name}}" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$account->name}}</div>
                <div class="email">{{$account->email}}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{url('organizer/profile')}}"><i class="material-icons">person</i>Profile</a></li>
                        <li><a href="{{url('organizer/change-password')}}"><i class="material-icons">lock</i>Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{url('logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="{{url('organizer/welcome')}}">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                @php
                    $is_superadmin = get_admin_role() == 'superadmin';
                    $is_admin_register = get_admin_role() == 'register01' || get_admin_role() == 'register02';
                    $is_admin_dentine = get_admin_role() == 'dentine';
                    $is_admin_essay_poster = get_admin_role() == 'essayposter';
                @endphp
                @if($is_admin_register || $is_superadmin)
                <li>
                    <a href="{{url('organizer/participant')}}">
                        <i class="material-icons">perm_identity</i>
                        <span>Participant</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('organizer/voucher')}}">
                        <i class="material-icons">card_membership</i>
                        <span>Payment Confirmation</span>
                    </a>
                </li>
                @endif
                @if($is_admin_essay_poster || $is_superadmin)
                <li>
                    <a href="{{url('organizer/fileupload')}}">
                        <i class="material-icons">insert_drive_file</i>
                        <span>Essay, Poster, & Debate Upload</span>
                    </a>
                </li>
                @endif
                @if($is_admin_dentine || $is_superadmin)
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">query_builder</i>
                        <span>Dentine Online Test</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{url('organizer/question/category')}}">
                                <span>Category</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('organizer/question')}}">
                                <span>Question</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('organizer/question/package')}}">
                                <span>Package</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('organizer/test')}}">
                                <span>Test Result</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                Copyright &copy;2015 - 2025
            </div>
            <div class="version">
                Made with <span style="color: #e25555;">&hearts;</span> by @riordhn
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
