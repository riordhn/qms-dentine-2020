@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="true" aria-controls="collapseExample">
                            <h2>
                                Filter Table
                            </h2>
                        </div>
                        <div class="body collapse in" id="collapseExample" aria-expanded="true">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <b>Event</b>
                                    <select class="form-control show-tick" onchange="selectSearch(this, 1)">
                                        <option value="">Select Event</option>
                                        <option>Dentine Scientific Essay</option>
                                        <option>Dentine Scientific Poster</option>
                                        <option>Dentine Scientific Debate</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Upload File
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="primary_table" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Event</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>From</th>
                                            <th>Submit At</th>
                                            <th>Download Hasil</th>
                                            <th>Download Statement</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Event</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>From</th>
                                            <th>Submit At</th>
                                            <th>Download Hasil</th>
                                            <th>Download Statement</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
// $(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });
    var participant_detail_url = '{{url('organizer/participant/detail')}}';
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('organizer/fileupload/table')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'event', name: 'event', searchable: true, orderable: false },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone', searchable: false, orderable: false },
            { data: 'organization', name: 'organization' },
            { data: 'submit_at', name: 'submit_at' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    if(data.file_1 == ''){
                        return '';
                    }else{
                        return '<a href="'+ data.file_1 +'" download class="btn bg-cyan btn-circle waves-effect waves-circle waves-float">'+
                                '    <i class="material-icons">insert_drive_file</i>'+
                                '</a>';
                    }
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    if(data.file_2 == ''){
                        return '';
                    }else{
                        return '<a href="'+ data.file_2 +'" download class="btn bg-cyan btn-circle waves-effect waves-circle waves-float">'+
                                '    <i class="material-icons">insert_drive_file</i>'+
                                '</a>';
                    }
                }
            },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a href="'+ participant_detail_url +'/'+ data.account +'/'+ data.event +'" target="_blank" type="button" class="btn bg-indigo btn-circle waves-effect waves-circle waves-float">'+
                            '    <i class="material-icons">remove_red_eye</i>'+
                            '</a>'+
                            '<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionDelete(this)">'+
                            '    <i class="material-icons">delete</i>'+
                            '</button>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();


    function selectSearch(el, column){
        var search = $.fn.dataTable.util.escapeRegex($(el).val());
        primary_table.columns( column ).search(search ? search : '', true, false).draw();
    }
// });

function actionDelete(element){
    var item = $(element);
    item.prop('disabled', true);
    var url = '{{url('organizer/fileupload/delete')}}';
    vex.dialog.confirm({
        message: 'Are you sure to delete this item?',
        callback: function (value) {
            if(value){
                $.ajax({
                    type: "POST",
                    url: url,
                    data:{
                        id: item.attr('data-id')
                    },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }else{
                item.prop('disabled', false);
            }
        }
    })
}
</script>
@endsection
