@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DATA PESERTA</h2>
            </div>
            <div class="row clearfix">
                @php
                    $participant_members = \App\Models\ParticipantMember::where('participant_id', $participant->participant_id)->orderBy('participant_member_id', 'asc')->get();
                @endphp
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-teal">
                            <h4><b>Data Lengkap Anggota Tim</b></h4>
                        </div>
                        <div class="body">
                        @if(!empty($participant) && $participant_members->first())
                            @foreach($participant_members as $i => $participant_member)
                            <div class="media">
                                <div class="media-left">
                                    <a href="javascript:void(0);">
                                        <img class="media-object" src="{{url('uploads/'.$participant_member->photo)}}" width="64" height="64">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$participant_member->name}} ({{$participant_member->participate_as}})</h4>
                                    <p>
                                        School : {{$participant_member->organization}}<br>
                                        Student : {{$participant_member->generation}}<br>
                                        {{\Carbon\Carbon::make($participant_member->birthday)->age}} Tahun<br>
                                        {{$participant_member->email}} / {{$participant_member->phone}}
                                    </p>
                                </div>
                                <div class="media-right">
                                    <a class="btn bg-success waves-effect" target="_blank" href="{{url('organizer/participant/member/manage/'.$participant_member->participant_member_id)}}">Edit</a>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <p class="col-pink">Peserta ini belum mengisi data lengkap anggota timnya</p>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
