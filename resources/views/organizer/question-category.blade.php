@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Category
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="{{url('organizer/question/category/manage')}}">Adding new category</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="primary_table" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Benar</th>
                                            <th>Salah</th>
                                            <th>Kosong</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Benar</th>
                                            <th>Salah</th>
                                            <th>Kosong</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $(function(){
        var primary_table = $('#primary_table').DataTable({
            processing: true,
            // serverSide: true,
            ajax: {
                url: '{{url('organizer/question/category/table')}}',
                type: 'POST'
            },
            columns: [
                { data: null, searchable: false, orderable: false },
                { data: 'name', name: 'name'},
                { data: 'true_value', name: 'true_value', orderable: false, searchable: false },
                { data: 'false_value', name: 'false_value', orderable: false, searchable: false },
                { data: 'null_value', name: 'null_value', orderable: false, searchable: false },
                { data: 'action', name: 'action', searchable: false, orderable: false,
                    render: function(data) {
                        return '<a type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float" href="{{url('organizer/question/category/manage')}}/'+ data.id +'">'+
                            '    <i class="material-icons">mode_edit</i>'+
                            '</a>'+
                            '<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionDelete(this)">'+
                            '    <i class="material-icons">delete</i>'+
                            '</button>';
                    }
                }
            ]
        });

        primary_table.on( 'draw', function () {
            primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                var start = this.page.info().page * 10;
                cell.innerHTML = start + i + 1;
            } );
        } ).draw();
    });

    function actionDelete(element){
        var item = $(element);
        item.prop('disabled', true);
        var url = '{{url('organizer/question/category/delete')}}';
        vex.dialog.confirm({
            message: 'Are you sure to delete this item?',
            callback: function (value) {
                if(value){
                    $.ajax({
                        type: "POST",
                        url: url,
                        data:{
                            question_category_id: item.attr('data-id')
                        },
                        success: function (data) {
                            location.reload();
                        },
                        error: function (xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }else{
                    item.prop('disabled', false);
                }
            }
        })
    }
</script>
@endsection
