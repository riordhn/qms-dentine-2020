@extends('app')
@section('meta')
<!-- Meta -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.9.1/summernote-lite.min.css" integrity="sha512-ySljI0ZbsJxjJIpfsg+7ZJOyKzBduAajCJpc4mBiVpGDPln2jVQ0kwYu3e2QQM5fwxLp6VSVaJm8XCK9uWD4uA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>MANAGE</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                QUESTION
                            </h2>
                        </div>
                        <div class="body">
                            <form method="POST" action="{{url('organizer/question')}}">
                                {{csrf_field()}}
                                <h2 class="card-inside-title">Category</h2>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <select class="form-control show-tick" name="category" required="">
                                            @foreach($question_categories as $question_category)
                                            <option value="{{$question_category->question_category_id}}">{{$question_category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <h2 class="card-inside-title">Question</h2>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <textarea class="summernote-editor" required="" name="question">
                                            <p>Belum ada pertanyaan</p>
                                        </textarea>
                                    </div>
                                </div>
                                @for($i=0; $i < 5; $i++)
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <!-- <button class="btn bg-cyan waves-effect m-b-15 collapsed" type="button" data-toggle="collapse" data-target="#collapseExample-{{$i}}" aria-expanded="false" aria-controls="collapseExample-{{$i}}">
                                            Answer {{$i + 1}}
                                        </button> -->
                                        Answer {{$i + 1}}
                                        <div id="collapseExample-{{$i}}">
                                            <textarea class="summernote-editor" required="" name="answer[]">
                                                <p>-</p>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                @endfor
                                <h2 class="card-inside-title">True Answer</h2>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <select class="form-control show-tick" name="true_answer" required="">
                                            @for($i=0; $i < 5; $i++)
                                            <option value="{{$i}}">Answer {{$i + 1}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button class="btn btn-block bg-pink waves-effect" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.9.1/summernote-lite.min.js" integrity="sha512-sIOi8vwsJpzCHtHd06hxJa2umWfY1FfEEE0nGAaolGlR73EzNnQaWdijVyLueB0PSnIp8Mj2TDQLLHTiIUn1gw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $('.summernote-editor').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['height', ['height']],
            ['view', ['codeview']],
        ],
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], this);
            }
        }
    });

    function summernoteUploadsImage(image, notekey) {
        var data = new FormData();
        data.append("files[]", image);
        $.ajax({
            url: "{{url('organizer/img/upload')}}",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function(data) {
                var url = data.split('}]')[1];
                var image = $('<img>').attr('src', url);
                $(notekey).summernote("insertNode", image[0]);
                console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endsection
