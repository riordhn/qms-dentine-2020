@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>MANAGE TEAM MEMBER</h2>
            </div>
            <div class="row clearfix">
                <form class="form-validation" method="POST" action="{{url('organizer/participant/member/save')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" name="id" required="" value="{{$item->participant_member_id}}">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="card">
                            <div class="header bg-pink">
                                <h4><b>EDIT DATA</b></h4>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="name" required="" value="{{$item->name}}">
                                                <label class="form-label">Full name</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment_ind</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="participate_as" readonly="" value="{{$item->participate_as}}">
                                                <label class="form-label">Participate As</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="organization" required="" value="{{$item->organization}}">
                                                <label class="form-label">School</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">bookmark</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="status" required="">
                                                <option value="STUDENT">STUDENT</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">format_list_numbered</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="generation" required="">
                                                <option value="">Select High School Student</option>
                                                <option value="X" {{($item->generation == "X")? 'selected': ''}}>Class X Senior High School</option>
                                                <option value="XI" {{($item->generation == "XI")? 'selected': ''}}>Class XI Senior High School</option>
                                                <option value="XII" {{($item->generation == "XII")? 'selected': ''}}>Class XII Senior High School</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">extension</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="nim" required="" value="{{$item->nim}}">
                                                <label class="form-label">Student Number</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">perm_contact_calendar</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control datepicker" name="birthday" required="" value="{{$item->birthday}}">
                                                <label class="form-label">Date of birth</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="phone" required="" value="{{$item->phone}}">
                                                <label class="form-label">Phone</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">whatshot</i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="form-group form-float">
                                            <div class="form-line success">
                                                <input type="text" class="form-control" name="social_media" value="{{$item->social_media}}">
                                                <label class="form-label">LINE ID</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <label class="form-label">Student Card</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                        <div class="fallback">
                                            <input name="file" type="file" multiple />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <img class="img" style="width: 50%;" src="{{url('uploads/'.$item->photo)}}"/>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <button type="submit" class="btn bg-pink btn-block btn-lg waves-effect">SAVE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        time: false
    });
</script>
@endsection
