@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    @php
        $role = get_admin_role();
        $is_admin_register = get_admin_role() == 'register01' || get_admin_role() == 'register02';
        $is_admin_dentine = get_admin_role() == 'dentine';
        $is_admin_essay_poster = get_admin_role() == 'essayposter';

        if ($role != 'superadmin') {
            if ($is_admin_essay_poster) {
                $vouchers = \App\Models\Voucher::select('used_at')->join('voucher_events', 'voucher_events.voucher_id', '=', 'vouchers.voucher_id')
                                ->whereIn('event_id', [2, 3])
                                ->where('is_paid', 10)
                                ->get();
                $voucher_not_paids = \App\Models\Voucher::select('used_at')->join('voucher_events', 'voucher_events.voucher_id', '=', 'vouchers.voucher_id')
                                ->whereIn('event_id', [2, 3])
                                ->where('is_paid', 1)
                                ->get();
            } else if ($is_admin_dentine) {
                $event = \App\Models\Event::where('name', strtoupper($role))->first();
                $vouchers = \App\Models\Voucher::select('used_at')->join('voucher_events', 'voucher_events.voucher_id', '=', 'vouchers.voucher_id')
                                ->where('event_id', 1)
                                ->where('is_paid', 10)
                                ->get();
                $voucher_not_paids = \App\Models\Voucher::select('used_at')->join('voucher_events', 'voucher_events.voucher_id', '=', 'vouchers.voucher_id')
                                ->where('event_id', 1)
                                ->where('is_paid', 1)
                                ->get();
            }else{
                $vouchers = \App\Models\Voucher::select('used_at')->where('is_paid', 10)->get();
                $voucher_not_paids = \App\Models\Voucher::select('used_at')->where('is_paid', 1)->get();
            }
        }else{
            $vouchers = \App\Models\Voucher::select('used_at')->where('is_paid', 10)->get();
            $voucher_not_paids = \App\Models\Voucher::select('used_at')->where('is_paid', 1)->get();
        }
        $today = Carbon\Carbon::today('Asia/Jakarta');
        $yesterday = Carbon\Carbon::yesterday('Asia/Jakarta');
    @endphp
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{$today->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body bg-teal" style="background-color:#e28f27 !important;">
                            <div class="font-bold m-b--35">SUMMARY ACCOUNT</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    NOT PAID
                                    <span class="pull-right"><b>{{$voucher_not_paids->count()}}</b> <small>ACCOUNT</small></span>
                                </li>
                                <li>
                                    TOTAL PAID ACCOUNT
                                    <span class="pull-right"><b>{{$vouchers->count()}}</b> <small>ACCOUNT</small></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h1>12 AGT</h1> <h4><b>Pengumuman</b></h4>
                        </div>
                        <div class="body">
                            Quis pharetra a pharetra fames blandit. Risus faucibus velit Risus imperdiet mattis neque volutpat, etiam lacinia netus dictum magnis per facilisi sociosqu. Volutpat. Ridiculus nostra.
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
