@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DATA PESERTA</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h4><b>Data Lengkap Anggota Tim</b></h4>
                        </div>
                        <div class="body">
                        @if(!empty($participant))
                            @php
                                $participant_members = \App\Models\ParticipantMember::where('participant_id', $participant->participant_id)->orderBy('participant_member_id', 'asc')->get();
                            @endphp
                            @if($participant_members->first())
                                @foreach($participant_members as $i => $participant_member)
                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0);">
                                            <img class="media-object" src="http://placehold.it/64x64" width="64" height="64">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{$participant_member->name}} ({{$participant_member->participate_as}})</h4>
                                        <p>
                                            Universitas : {{$participant_member->organization}}<br>
                                            {{\Carbon\Carbon::make($participant_member->birthday)->age}} Tahun<br>
                                            {{$participant_member->email}} / {{$participant_member->phone}}
                                        </p>
                                    </div>
                                </div>
                                @endforeach
                            @else
                                <p class="col-pink">Peserta ini belum mengisi data lengkap anggota timnya</p>
                            @endif
                        @else
                            <p class="col-pink">Peserta ini belum mengisi data lengkap anggota timnya</p>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                Summary (Hanya berlaku untuk soal pilihan ganda)
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>Jumlah soal</th>
                                        <td>{{$test_answers->count()}}</td>
                                    </tr>
                                    <tr>
                                        <th>Soal terjawab</th>
                                        <td>{{$test_answers->where('correct', '<>', 0)->count()}}</td>
                                    </tr>
                                    <tr>
                                        <th>Soal tidak terjawab</th>
                                        <td>{{$test_answers->where('correct', '=', 0)->count()}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total nilai</th>
                                        <td>{{$test_answers->sum('value')}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="block-header">
                <h2>DETAIL</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                Hasil test mengerjakan {{$question_package->title}}
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nomor</th>
                                        <th>Status</th>
                                        <th>Nilai</th>
                                        <th>Jawaban Isian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($test_answers as $no_test_answer => $test_answer)
                                    <tr>
                                        <th>Soal {{$no_test_answer+1}}</th>
                                        @if($test_answer->question->question_type == 1)
                                            @if($test_answer->correct == 0)
                                            <td>Tidak dijawab</td>
                                            @elseif($test_answer->correct == 1)
                                            <td>Benar</td>
                                            @elseif($test_answer->correct == 2)
                                            <td>Salah</td>
                                            @endif
                                            <td>{{$test_answer->value}}</td>
                                        @else
                                        <td></td>
                                        <td></td>
                                        <td>{{$test_answer->content_text}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection
