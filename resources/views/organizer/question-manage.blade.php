@extends('app')
@section('meta')
<!-- Meta -->
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css">
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>MANAGE</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                MANAGE QUESTION
                            </h2>
                            <div class="header-dropdown m-r-15" style="top:12px">
                                <a class="btn bg-orange waves-effect" href="{{url('organizer/question/test/'.$item->question_id)}}" target="_blank">
                                    <i class="material-icons">open_in_new</i>
                                    <span>To Test Page</span>
                                </a>
                            </div>
                        </div>
                        <div class="body">
                            <form class="form-validation" method="POST" action="{{url('organizer/question')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="question_id" value="{{$item->question_id}}">
                                <h2 class="card-inside-title">Category</h2>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <select class="form-control show-tick" name="category" required="">
                                            @foreach($question_categories as $question_category)
                                            <option value="{{$question_category->question_category_id}}" @if($item->question_category_id == $question_category->question_category_id) selected="" @endif>{{$question_category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <h2 class="card-inside-title">Question</h2>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <textarea class="summernote-editor" name="question">
                                            {!!$item->content!!}
                                        </textarea>
                                    </div>
                                </div>
                                @if($item->question_type == 1)
                                @foreach($question_options as $i => $question_option)
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <!-- <button class="btn bg-cyan waves-effect m-b-15 collapsed" type="button" data-toggle="collapse" data-target="#collapseExample-{{$i}}" aria-expanded="false" aria-controls="collapseExample-{{$i}}">
                                            Answer {{$i + 1}}
                                        </button> -->
                                        Answer {{$i + 1}}
                                        <div id="collapseExample-{{$i}}">
                                            <input type="hidden" name="answer_id[]" value="{{$question_option->question_option_id}}">
                                            <textarea class="summernote-editor" name="answer[]">
                                                {!!$question_option->content!!}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <h2 class="card-inside-title">True Answer</h2>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <select class="form-control show-tick" name="true_answer" required="">
                                            @foreach($question_options as $i => $question_option)
                                            <option value="{{$i}}" @if($question_option->correct == 1) selected="" @endif>Answer {{$i + 1}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button class="btn btn-block bg-pink waves-effect" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
<script>
    $('.summernote-editor').summernote({
        placeholder: '',
        tabsize: 2,
        height: 250,
        callbacks: {
            onImageUpload: function(files) {
                summernoteUploadsImage(files[0], this);
            }
        }
    });

    function summernoteUploadsImage(image, notekey) {
        var data = new FormData();
        data.append("files[]", image);
        $.ajax({
            url: "{{url('organizer/img/upload')}}",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function(data) {
                var url = data.split('}]')[1];
                var image = $('<img>').attr('src', url);
                $(notekey).summernote("insertNode", image[0]);
                console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endsection
