@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Participant
                            </h2>
                            <div class="header-dropdown m-r-15" style="top:12px">
                                <a class="btn bg-teal waves-effect" onclick="downloadExcel('account')">
                                    <i class="material-icons">file_download</i>
                                    <span>Download Account</span>
                                </a>
                                <a class="btn bg-teal waves-effect" onclick="downloadExcel('member')">
                                    <i class="material-icons">file_download</i>
                                    <span>Download Team Member</span>
                                </a>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="primary_table" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Event</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>From</th>
                                            <th>Country</th>
                                            <th>City</th>
                                            <th>Rayon</th>
                                            <th>Paid</th>
                                            <th>Regist From</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Event</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>From</th>
                                            <th>Country</th>
                                            <th>City</th>
                                            <th>Rayon</th>
                                            <th>Paid</th>
                                            <th>Regist From</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
$(function () {
    var detail_url = '{{url('organizer/participant/detail')}}/';
    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('organizer/participant/table')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'name', name: 'name' },
            { data: 'event_name', searchable: false, orderable: false },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'organization', name: 'organization' },
            { data: 'country.name'},
            { data: 'city.name', 
                render: function(data, type, row){
                    return `${row.province.name} - ${data}`;
                }
            },
            { data: 'rayon.name' },
            {   data: 'is_paid', 
                searchable: false, 
                orderable: false,
                render: function(data) {
                        if(data == 'YES'){
                            return '<span class="badge bg-green">'+ data +'</spam>';
                        }else{
                            return '<span class="badge bg-pink">'+ data +'</span>';
                        }
                    }
            },
            { data: 'regist_from', name: 'regist_from' },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    var html = '';
                    if(data.event != 0){
                        html += '<div class="btn-group" data-load="false">'+
                                '    <a type="button" class="btn btn-info" href="'+ detail_url + data.id +'/'+ data.event+'">'+
                                '        Show Team'+
                                '    </a>'+
                                '</div>';
                    }
                    html +='<div class="btn-group" data-load="false">'+
                            '    <a type="button" class="btn btn-success" href="{{url('organizer/participant/reset-password')}}/'+ data.id +'">'+
                            '        Reset Password'+
                            '    </a>'+
                            '</div>';
                    // if(data.status == 1){
                        html += '<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionDelete(this)">'+
                            '    <i class="material-icons">delete</i>'+
                            '</button>';
                    // }

                    return html;
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();
});

function participateList(element){
    var item = $(element);
    if(item.attr('data-load') == 'false'){
        var url = '{{url('organizer/participant/participate/list')}}';
        var detail_url = '{{url('organizer/participant/detail')}}/';
        $.ajax({
            type: "POST",
            url: url,
            data:{
                account: item.attr('data-id')
            },
            success: function (data) {
                item.html('');
                var html = '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">'+
                            '    Detail <span class="caret"></span>'+
                            '</button>'+
                            '<ul class="dropdown-menu">';
                $.each(data.list_data, function(key, event){
                    html += '<li><a href="'+ detail_url + item.attr('data-id') +'/'+ event.id +'" class=" waves-effect waves-block">'+ event.name +'</a></li>';
                });

                html += '</ul>';
                item.html(html);
                item.attr('data-load', 'true')
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }else{
        return false;
    }
}

function actionDelete(element){
    var item = $(element);
    item.prop('disabled', true);
    var url = '{{url('organizer/participant/delete')}}';
    vex.dialog.confirm({
        message: 'Are you sure to delete this item?',
        callback: function (value) {
            if(value){
                $.ajax({
                    type: "POST",
                    url: url,
                    data:{
                        id: item.attr('data-id')
                    },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }else{
                item.prop('disabled', false);
            }
        }
    })
}

function downloadExcel(path_url){
    var url = "{{url('organizer/participant')}}/" + path_url + '/excel';
    var event = $('select[name=event]').val();

    window.open(url + '?event=' + event, '_blank');
}
</script>
@endsection
