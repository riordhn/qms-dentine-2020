@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-black">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>List {{$question_package->title}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                Pick Question
                            </h2>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#not" data-toggle="tab" class="col-pink">Not Selected Question</a></li>
                                <li role="presentation"><a href="#selected" data-toggle="tab" class="col-green">Selected Question</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="not">
                                    <b>List Question</b>
                                    <div class="table-responsive">
                                        <table id="primary_table" class="table table-bordered table-striped table-hover dataTable" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Category</th>
                                                    <th>Question</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Category</th>
                                                    <th>Question</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="selected">
                                    <b>List Question Selected</b>
                                    <div class="table-responsive">
                                        <table id="secondary_table" class="table table-bordered table-striped table-hover dataTable" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Category</th>
                                                    <th>Question</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Category</th>
                                                    <th>Question</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });

    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('organizer/question/package/detail/table/'.$question_package->question_package_id.'/1')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'name', name: 'name'},
            { data: 'content', name: 'text', orderable: false },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<button type="button" class="btn btn-info btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionAdd(this)">'+
                        '    <i class="material-icons">library_add</i>'+
                        '</button>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('organizer/question/package/detail/table/'.$question_package->question_package_id.'/2')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'name', name: 'name'},
            { data: 'content', name: 'text', orderable: false },
            { data: 'action', name: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<button type="button" class="btn btn-warning btn-circle waves-effect waves-circle waves-float" data-id="'+data.id+'" onclick="actionDelete(this)">'+
                        '    <i class="material-icons">delete</i>'+
                        '</button>';
                }
            }
        ]
    });

    secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function actionAdd(element){
        var item = $(element);
        item.prop('disabled', true);
        var url = '{{url('organizer/question/package/detail/add')}}';
        if(item.is(":disabled") ){
            $.ajax({
                type: "POST",
                url: url,
                data:{
                    question_package_id: {{$question_package->question_package_id}},
                    question_id: item.attr('data-id')
                },
                success: function (result) {
                    primary_table.ajax.reload(null, false);
                    secondary_table.ajax.reload(null, false);
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        }
    }

    function actionDelete(element){
        var item = $(element);
        item.prop('disabled', true);
        var url = '{{url('organizer/question/package/detail/delete')}}';
        if(item.is(":disabled") ){
            $.ajax({
                type: "POST",
                url: url,
                data:{
                    question_package_id: {{$question_package->question_package_id}},
                    question_id: item.attr('data-id')
                },
                success: function (result) {
                    primary_table.ajax.reload(null, false);
                    secondary_table.ajax.reload(null, false);
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        }
    }
</script>
@endsection
