@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <div class="overlay"></div>
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    @include('organizer/partials/topbar')
    @include('organizer/partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Result Participant
                            </h2>
                            <div class="header-dropdown m-r-15" style="top:12px">
                                <a class="btn bg-teal waves-effect" href="{{url('organizer/test/excel')}}">
                                    <i class="material-icons">file_download</i>
                                    <span>Download Excel</span>
                                </a>
                                <a class="btn bg-green waves-effect" href="{{url('organizer/test/refresh')}}">
                                    <i class="material-icons">refresh</i>
                                    <span>Refresh</span>
                                </a>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="primary_table" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Asal</th>
                                            <th>Nilai</th>
                                            <th>New Tab</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Asal</th>
                                            <th>Nilai</th>
                                            <th>New Tab</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<!-- TinyMCE -->
<script>
    $(function(){
        var primary_table = $('#primary_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{url('organizer/test/table')}}',
                type: 'POST'
            },
            columns: [
                { data: null, searchable: false, orderable: false },
                { data: 'name', name: 'name', orderable: false },
                { data: 'email', name: 'email', orderable: false },
                { data: 'organization', name: 'organization', orderable: false },
                { data: 'result', name: 'result', searchable: false },
                { data: 'flags', name: 'flags', searchable: false },
                { data: 'status', name: 'status', searchable: false, orderable: false },
                { data: 'action', name: 'action', searchable: false, orderable: false,
                    render: function(data) {
                        return '<a type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float" href="{{url('organizer/test/detail')}}/'+ data.id +'">'+
                            '    <i class="material-icons">remove_red_eye</i>'+
                            '</a>';
                    }
                }
            ]
        });

        primary_table.on( 'draw', function () {
            primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                var start = this.page.info().page * 10;
                cell.innerHTML = start + i + 1;
            } );
        } ).draw();
    });
</script>
@endsection
