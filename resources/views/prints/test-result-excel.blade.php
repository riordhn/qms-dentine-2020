<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Organization</th>
            <th>Paket</th>
            <th>Nilai</th>
            <th>Soal Benar</th>
            <th>Soal Salah</th>
            <th>Soal Kosong</th>
            <th>New Tab</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
    @foreach($list_data as $no => $data)
        <tr>
            <td>{{$no+1}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->organization}}</td>
            <td>{{$data->title}}</td>
            <td>{{$data->result}}</td>
            <td>{{$data->true_answer}}</td>
            <td>{{$data->false_answer}}</td>
            <td>{{$data->null_answer}}</td>
            <td>{{$data->flags}}</td>
            <td>{{$data->email}}</td>
            <td>{{$data->phone}}</td>
            <td>{{$data->address}}</td>
        </tr>
    @endforeach
    </tbody>
</table>