<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Event</th>
            <th>Leader Name</th>
            <th>School</th>
            <th>Email</th>
            <th>Phone</th>
        </tr>
    </thead>
    <tbody>
    @foreach($list_data as $no => $data)
        <tr>
            <td>{{$no+1}}</td>
            <td>{{$data->voucher->voucher_events->first()->event->name}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->organization}}</td>
            <td>{{$data->email}}</td>
            <td>{{$data->phone}}</td>
        </tr>
    @endforeach
    </tbody>
</table>