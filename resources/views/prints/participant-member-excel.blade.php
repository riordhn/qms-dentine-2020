<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Code</th>
            <th>Province</th>
            <th>City</th>
            <th>Leader Name</th>
            <th>Age</th>
            <th>School</th>
            <th>Student</th>
            <th>Student Number</th>
            <th>Phone</th>
            <th>LINE ID</th>
            <th>Member 1</th>
            <th>Age</th>
            <th>School</th>
            <th>Student</th>
            <th>Student Number</th>
            <th>Phone</th>
            <th>LINE ID</th>
            <th>Member 2</th>
            <th>Age</th>
            <th>School</th>
            <th>Student</th>
            <th>Student Number</th>
            <th>Phone</th>
            <th>LINE ID</th>
        </tr>
    </thead>
    <tbody>
    @foreach($list_data as $no => $participant)
        <tr>
            <td>{{$no+1}}</td>
            <td>{{$participant->voucher->code}}</td>
            <td>{{$participant->account->province->name}}</td>
            <td>{{$participant->account->city->name}}</td>
            @foreach($participant->member as $participant_member)
                <td>{{$participant_member->name}}</td>
                <td>{{\Carbon\Carbon::make($participant_member->birthday)->age}} years old</td>
                <td>{{$participant_member->organization}}</td>
                <td>{{$participant_member->generation}}</td>
                <td>{{$participant_member->nim}}</td>
                <td>{{$participant_member->phone}}</td>
                <td>{{$participant_member->social_media}}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
